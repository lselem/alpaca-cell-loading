# CLApp !

*Cell Loading App !*

## Français

Une documentation complète de l'application est disponible en français [ici](https://cell-loading-app.docs.cern.ch/fr).

Pour l'installation complète et le déploiement, voir cette [page](https://cell-loading-app.docs.cern.ch/fr/Installation/).

Un démonstrateur de cette application a été déployé sur CERN Openshift. Il est accessible [ici](https://demonstrator-clapp.app.cern.ch/).

## English

A complete documentation of the application is available in english [here](https://cell-loading-app.docs.cern.ch/en).

For the full installation and deploying, see this [page](https://cell-loading-app.docs.cern.ch/en/Installation/).

A demonstrator of this application has been deployed on CERN Openshift. It is available [here](https://demonstrator-clapp.app.cern.ch/).


