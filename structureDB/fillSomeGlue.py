# -*-coding:utf-8 -*-
import argparse
from access_db import *

#-- Get a connector object to the DB
parser = argparse.ArgumentParser()
parser.add_argument(
        "--db", type=str,
        help="The configuration file containing the DB access parameters",
        required=False,
    )
parser.add_argument(
        "--host", type=str,
        help="Address of the server where the YalDB volume is stored",
        required=False,
    )
parser.add_argument(
        "--port", type=str,
        help="Exposed port on the server where the YalDB volume is stored",
        required=False,
    )
args = parser.parse_args()

conn = connectionDB( args )
cursor = conn.cursor()

#-- SQL commands for the DB structure creation
fill_stycast = """
    INSERT INTO Stycast (
                refglue,
                opening_date,
                expiry_date,
                no_pot,
                no_cartridge
                )
         VALUES (%s,%s,%s,%s,%s);
"""
stycastValues = (
        ("O223069113","2023-05-19","2024-08-19","1","1"),
        ("O223069112","2023-05-20","2025-01-19","2","1"),
        ("O223069114","2023-10-20","2025-01-19","2","1"),
        )
fill_catalyst = """
    INSERT INTO Catalyst (
                refcata,
                opening_date,
                expiry_date,
                no_pot
                )
         VALUES (%s,%s,%s,%s);
"""
catalystValues = (
        ("O222695080","2023-05-19","2024-08-19","1"),
        ("O222695081","2023-05-20","2025-08-19","2"),
        ("O222695082","2024-05-20","2025-09-19","2"),
        )

print("Filling Stycast Table")
for val in stycastValues:
    cursor.execute(fill_stycast, val)
conn.commit()

print("Filling Catalyst Table")
for val in catalystValues:
    cursor.execute(fill_catalyst, val)
conn.commit()

cursor.close()
conn.close()

