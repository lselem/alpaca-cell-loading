# -*-coding:utf-8 -*-
import argparse
from access_db import *

#-- Get a connector object to the DB
parser = argparse.ArgumentParser()
parser.add_argument(
        "--db", type=str,
        help="The configuration file containing the DB access parameters",
        required=False,
    )
parser.add_argument(
        "--host", type=str,
        help="Address of the server where the YalDB volume is stored",
        required=False,
    )
parser.add_argument(
        "--port", type=str,
        help="Exposed port on the server where the YalDB volume is stored",
        required=False,
    )
args = parser.parse_args()

conn = connectionDB( args )
cursor = conn.cursor()

#-- SQL commands for the DB structure creation
create_table = """
    CREATE TABLE test_touch(
        id SERIAL PRIMARY KEY,
        num INT
    );"""

insert_table = """
    INSERT INTO test_touch(num)
    VALUES (%s);
    """
values = (
        ("1",),
        ("21",),
        ("31",),
        )
select_table = """
    SELECT *
    FROM test_touch
    ;"""
drop_table = """
    DROP TABLE test_touch;
    """

print("=== create table ===")
cursor.execute(create_table)

for val in values:
    print("=== insert",val,"===")
    cursor.execute(insert_table,val)

print("=== read table ===")
cursor.execute(select_table)
print(cursor.fetchall())

print("=== drop table ===")
cursor.execute(drop_table)

conn.commit()
cursor.close()
conn.close()
print("=== The end ===")
