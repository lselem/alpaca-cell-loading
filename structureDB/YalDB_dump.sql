--
-- PostgreSQL database dump
--

-- Dumped from database version 14.12 (Ubuntu 14.12-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.12 (Ubuntu 14.12-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: catalyst; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.catalyst (
    catalyst_id integer NOT NULL,
    refcata text NOT NULL,
    opening_date date,
    expiry_date date,
    no_pot integer
);


ALTER TABLE public.catalyst OWNER TO postgres;

--
-- Name: catalyst_catalyst_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.catalyst_catalyst_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalyst_catalyst_id_seq OWNER TO postgres;

--
-- Name: catalyst_catalyst_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.catalyst_catalyst_id_seq OWNED BY public.catalyst.catalyst_id;


--
-- Name: cells; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cells (
    cell_id text NOT NULL,
    photo_front json NOT NULL,
    photo_back json NOT NULL,
    mass double precision NOT NULL,
    cond_preparation_fk integer NOT NULL,
    result_preparation boolean,
    module_fk text,
    cond_alignment_fk integer,
    result_alignment boolean,
    cond_gluing_fk integer,
    result_gluing boolean,
    cond_press_fk integer,
    result_press boolean
);


ALTER TABLE public.cells OWNER TO postgres;

--
-- Name: conditions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conditions (
    cond_id integer NOT NULL,
    datetime timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    temperature integer NOT NULL,
    humidity integer NOT NULL,
    operator text NOT NULL,
    comment text
);


ALTER TABLE public.conditions OWNER TO postgres;

--
-- Name: conditions_cond_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.conditions_cond_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conditions_cond_id_seq OWNER TO postgres;

--
-- Name: conditions_cond_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.conditions_cond_id_seq OWNED BY public.conditions.cond_id;


--
-- Name: gluedeposit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gluedeposit (
    cell_fk text NOT NULL,
    stycast_fk integer NOT NULL,
    catalyst_fk integer NOT NULL,
    cond_fk integer NOT NULL,
    mass_stycast double precision NOT NULL,
    mass_gluemix double precision NOT NULL,
    ini_pressure double precision NOT NULL,
    mass_bare_sample double precision NOT NULL,
    mass_glued_sample double precision NOT NULL,
    corr_slope double precision NOT NULL,
    corr_pressure double precision NOT NULL,
    datetime_deposit timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    duration_sample time without time zone NOT NULL,
    duration_deposit time without time zone NOT NULL,
    result boolean NOT NULL,
    depot_id integer NOT NULL
);


ALTER TABLE public.gluedeposit OWNER TO postgres;

--
-- Name: gluedeposit_depot_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gluedeposit_depot_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gluedeposit_depot_id_seq OWNER TO postgres;

--
-- Name: gluedeposit_depot_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gluedeposit_depot_id_seq OWNED BY public.gluedeposit.depot_id;


--
-- Name: modules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.modules (
    module_id text NOT NULL,
    carrier_id text NOT NULL,
    photo_reception json NOT NULL,
    is_stored boolean,
    cond_reception_fk integer NOT NULL,
    result_reception boolean,
    cond_inspection_fk integer,
    result_inspection boolean,
    cond_cutting_fk integer,
    result_cutting boolean
);


ALTER TABLE public.modules OWNER TO postgres;

--
-- Name: nullable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nullable (
    id integer NOT NULL,
    boola boolean,
    boolb boolean
);


ALTER TABLE public.nullable OWNER TO postgres;

--
-- Name: nullable_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nullable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nullable_id_seq OWNER TO postgres;

--
-- Name: nullable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nullable_id_seq OWNED BY public.nullable.id;


--
-- Name: stycast; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stycast (
    stycast_id integer NOT NULL,
    refglue text NOT NULL,
    opening_date date,
    expiry_date date,
    no_pot integer,
    no_cartridge integer
);


ALTER TABLE public.stycast OWNER TO postgres;

--
-- Name: stycast_stycast_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.stycast_stycast_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stycast_stycast_id_seq OWNER TO postgres;

--
-- Name: stycast_stycast_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.stycast_stycast_id_seq OWNED BY public.stycast.stycast_id;


--
-- Name: visualdefects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.visualdefects (
    defect_id integer NOT NULL,
    photo json NOT NULL,
    module_fk text NOT NULL,
    cond_fk integer NOT NULL
);


ALTER TABLE public.visualdefects OWNER TO postgres;

--
-- Name: visualdefects_defect_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.visualdefects_defect_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.visualdefects_defect_id_seq OWNER TO postgres;

--
-- Name: visualdefects_defect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.visualdefects_defect_id_seq OWNED BY public.visualdefects.defect_id;


--
-- Name: catalyst catalyst_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalyst ALTER COLUMN catalyst_id SET DEFAULT nextval('public.catalyst_catalyst_id_seq'::regclass);


--
-- Name: conditions cond_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conditions ALTER COLUMN cond_id SET DEFAULT nextval('public.conditions_cond_id_seq'::regclass);


--
-- Name: gluedeposit depot_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gluedeposit ALTER COLUMN depot_id SET DEFAULT nextval('public.gluedeposit_depot_id_seq'::regclass);


--
-- Name: nullable id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nullable ALTER COLUMN id SET DEFAULT nextval('public.nullable_id_seq'::regclass);


--
-- Name: stycast stycast_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stycast ALTER COLUMN stycast_id SET DEFAULT nextval('public.stycast_stycast_id_seq'::regclass);


--
-- Name: visualdefects defect_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visualdefects ALTER COLUMN defect_id SET DEFAULT nextval('public.visualdefects_defect_id_seq'::regclass);


--
-- Data for Name: catalyst; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.catalyst (catalyst_id, refcata, opening_date, expiry_date, no_pot) FROM stdin;
1	O222695060	2023-05-19	2024-08-19	1
2	O222695061	2023-05-20	2023-08-19	2
3	O222695062	2024-05-20	2025-08-19	2
4	O222695063	2024-06-20	2025-09-19	2
\.


--
-- Name: catalyst_catalyst_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.catalyst_catalyst_id_seq', 2, true);


--
-- Data for Name: stycast; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stycast (stycast_id, refglue, opening_date, expiry_date, no_pot, no_cartridge) FROM stdin;
3	O223065113	2023-05-19	2024-08-19	1	1
4	O223064112	2023-05-20	2024-01-19	2	1
5	O223064114	2023-10-20	2025-01-19	2	1
6	O223064115	2023-12-20	2025-03-19	2	1
\.


--
-- Name: stycast_stycast_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.stycast_stycast_id_seq', 4, true);


-- Name: catalyst catalyst_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalyst
    ADD CONSTRAINT catalyst_pkey PRIMARY KEY (catalyst_id);


--
-- Name: cells cells_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cells
    ADD CONSTRAINT cells_pkey PRIMARY KEY (cell_id);


--
-- Name: conditions conditions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conditions
    ADD CONSTRAINT conditions_pkey PRIMARY KEY (cond_id);


--
-- Name: gluedeposit gluedeposit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gluedeposit
    ADD CONSTRAINT gluedeposit_pkey PRIMARY KEY (depot_id);


--
-- Name: modules modules_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_pkey PRIMARY KEY (module_id);


--
-- Name: nullable nullable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nullable
    ADD CONSTRAINT nullable_pkey PRIMARY KEY (id);


--
-- Name: stycast stycast_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stycast
    ADD CONSTRAINT stycast_pkey PRIMARY KEY (stycast_id);


--
-- Name: visualdefects visualdefects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visualdefects
    ADD CONSTRAINT visualdefects_pkey PRIMARY KEY (defect_id);


--
-- Name: cells cells_cond_alignment_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cells
    ADD CONSTRAINT cells_cond_alignment_fk_fkey FOREIGN KEY (cond_alignment_fk) REFERENCES public.conditions(cond_id);


--
-- Name: cells cells_cond_gluing_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cells
    ADD CONSTRAINT cells_cond_gluing_fk_fkey FOREIGN KEY (cond_gluing_fk) REFERENCES public.conditions(cond_id);


--
-- Name: cells cells_cond_preparation_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cells
    ADD CONSTRAINT cells_cond_preparation_fk_fkey FOREIGN KEY (cond_preparation_fk) REFERENCES public.conditions(cond_id);


--
-- Name: cells cells_cond_press_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cells
    ADD CONSTRAINT cells_cond_press_fk_fkey FOREIGN KEY (cond_press_fk) REFERENCES public.conditions(cond_id);


--
-- Name: cells cells_module_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cells
    ADD CONSTRAINT cells_module_fk_fkey FOREIGN KEY (module_fk) REFERENCES public.modules(module_id);


--
-- Name: gluedeposit gluedeposit_catalyst_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gluedeposit
    ADD CONSTRAINT gluedeposit_catalyst_fk_fkey FOREIGN KEY (catalyst_fk) REFERENCES public.catalyst(catalyst_id);


--
-- Name: gluedeposit gluedeposit_cell_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gluedeposit
    ADD CONSTRAINT gluedeposit_cell_fk_fkey FOREIGN KEY (cell_fk) REFERENCES public.cells(cell_id);


--
-- Name: gluedeposit gluedeposit_cond_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gluedeposit
    ADD CONSTRAINT gluedeposit_cond_fk_fkey FOREIGN KEY (cond_fk) REFERENCES public.conditions(cond_id);


--
-- Name: gluedeposit gluedeposit_stycast_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gluedeposit
    ADD CONSTRAINT gluedeposit_stycast_fk_fkey FOREIGN KEY (stycast_fk) REFERENCES public.stycast(stycast_id);


--
-- Name: modules modules_cond_cutting_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_cond_cutting_fk_fkey FOREIGN KEY (cond_cutting_fk) REFERENCES public.conditions(cond_id);


--
-- Name: modules modules_cond_inspection_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_cond_inspection_fk_fkey FOREIGN KEY (cond_inspection_fk) REFERENCES public.conditions(cond_id);


--
-- Name: modules modules_cond_reception_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modules
    ADD CONSTRAINT modules_cond_reception_fk_fkey FOREIGN KEY (cond_reception_fk) REFERENCES public.conditions(cond_id);


--
-- Name: visualdefects visualdefects_cond_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visualdefects
    ADD CONSTRAINT visualdefects_cond_fk_fkey FOREIGN KEY (cond_fk) REFERENCES public.conditions(cond_id);


--
-- Name: visualdefects visualdefects_module_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visualdefects
    ADD CONSTRAINT visualdefects_module_fk_fkey FOREIGN KEY (module_fk) REFERENCES public.modules(module_id);


--
-- PostgreSQL database dump complete
--

