# -*-coding:utf-8 -*-
import sys

import psycopg2 as psycopg
from psycopg2 import OperationalError

#import ruamel.yaml as yaml
from yaml_tools import YAML

def connectionDB_withYAML( db_access ):
    #-- Get the db access parameters
    #with open(db_access, 'r') as file:
    yaml = YAML(typ="safe", pure=True)
    with open(db_access) as f:
        try:
            #db_config = yaml.safe_load(file)
            db_config = yaml.load(f.read())
        except Exception as err:
            print("A YAML exception occured:\n", err)
            sys.exit(1)
    
    #-- Try connecting to the DB
    try:
        conn = psycopg.connect(
            host     = db_config['host'],
            dbname   = db_config['dbname'],
            user     = db_config['user'],
            password = db_config['password'],
            port     = db_config['port'],
            )
    except OperationalError as err:
        print("ERROR - OperationalError while accessing the DB:",err)
        sys.exit(1)

    return conn

def connectionDockerisedYalDB( host, port = 5432 ):
    #-- Try connecting to the DB
    try:
        conn = psycopg.connect(
            host     = host,
            dbname   = "yaldb",
            user     = "postgres",
            password = "secret",
            port     = port,
            )
    except OperationalError as err:
        print("ERROR - OperationalError while accessing the DB:",err)
        sys.exit(1)

    return conn

def connectionDB( args ):
    if args.db:
        conn = connectionDB_withYAML( args.db )
    elif args.host:
        if args.port:
            conn = connectionDockerisedYalDB( args.host, args.port )
        else:
            conn = connectionDockerisedYalDB( args.host )
    else:
        raise ValueError("Missing argument either --db or --host")

    return conn
