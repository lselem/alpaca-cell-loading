# -*-coding:utf-8 -*-
import argparse
from access_db import *

#-- Get a connector object to the DB
parser = argparse.ArgumentParser()
parser.add_argument(
        "--db", type=str,
        help="The configuration file containing the DB access parameters",
        required=True,
    )
parser.add_argument(
        "--host", type=str,
        help="Address of the server where the YalDB volume is stored",
        required=False,
    )
parser.add_argument(
        "--port", type=str,
        help="Exposed port on the server where the YalDB volume is stored",
        required=False,
    )
parser.add_argument(
        "--recreate", action="store_true",
        help="Flag to drop all tables and recreate them empty.",
        required=False,
    )
args = parser.parse_args()

conn = connectionDB( args )
cursor = conn.cursor()

#-- SQL commands for the DB structure creation
conditions_creation = """
    CREATE TABLE IF NOT EXISTS Conditions (
        cond_id SERIAL PRIMARY KEY,
        datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        temperature INT NOT NULL,
        humidity INT NOT NULL,
        operator TEXT NOT NULL,
        comment TEXT
    );
    """

#TODO module_id CHAR(14),
#TODO carrier_id CHAR(14),
module_creation = """
    CREATE TABLE IF NOT EXISTS Modules (
        module_id TEXT PRIMARY KEY,
        carrier_id TEXT NOT NULL,
        photo_reception JSON NOT NULL,
        is_stored BOOLEAN, 
        cond_reception_fk INT NOT NULL,
        result_reception BOOLEAN,
        cond_inspection_fk INT,
        result_inspection BOOLEAN,
        cond_cutting_fk INT,
        result_cutting BOOLEAN,
        FOREIGN KEY (cond_reception_fk)
            REFERENCES Conditions(cond_id),
        FOREIGN KEY (cond_inspection_fk)
            REFERENCES Conditions(cond_id),
        FOREIGN KEY (cond_cutting_fk)
            REFERENCES Conditions(cond_id)
    );
    """

#TODO module_fk CHAR(14),
inspection_creation = """
    CREATE TABLE IF NOT EXISTS VisualDefects (
        defect_id SERIAL PRIMARY KEY,
        photo JSON NOT NULL,
        module_fk TEXT NOT NULL,
        cond_fk INT NOT NULL,
        FOREIGN KEY (module_fk)
            REFERENCES Modules(module_id),
        FOREIGN KEY (cond_fk)
            REFERENCES Conditions(cond_id)
    );
    """

#TODO cell_id CHAR(14),
cell_creation = """
    CREATE TABLE IF NOT EXISTS Cells (
        cell_id TEXT PRIMARY KEY,
        photo_front JSON NOT NULL,
        photo_back JSON NOT NULL,
        mass FLOAT NOT NULL,
        cond_preparation_fk INT NOT NULL,
        result_preparation BOOLEAN,
        module_fk TEXT,
        cond_alignment_fk INT,
        result_alignment BOOLEAN,
        cond_gluing_fk INT,
        result_gluing BOOLEAN,
        cond_press_fk INT,
        result_press BOOLEAN,
        FOREIGN KEY (module_fk)
            REFERENCES Modules(module_id),
        FOREIGN KEY (cond_preparation_fk)
            REFERENCES Conditions(cond_id),
        FOREIGN KEY (cond_alignment_fk)
            REFERENCES Conditions(cond_id),
        FOREIGN KEY (cond_gluing_fk)
            REFERENCES Conditions(cond_id),
        FOREIGN KEY (cond_press_fk)
            REFERENCES Conditions(cond_id)
    );
    """

stycast_creation = """
    CREATE TABLE IF NOT EXISTS Stycast (
        stycast_id SERIAL PRIMARY KEY,
        refglue TEXT NOT NULL,
        opening_date DATE,
        expiry_date DATE,
        no_pot INT,
        no_cartridge INT
    );
    """

catalyst_creation = """
    CREATE TABLE IF NOT EXISTS Catalyst (
        catalyst_id SERIAL PRIMARY KEY,
        refcata TEXT NOT NULL,
        opening_date DATE,
        expiry_date DATE,
        no_pot INT
    );
    """

#TODO: GlueDeposit = JSON ?
#     --> flexibility !
deposit_creation = """
    CREATE TABLE IF NOT EXISTS GlueDeposit (
        depot_id SERIAL PRIMARY KEY,
        cell_fk TEXT NOT NULL,
        stycast_fk INT NOT NULL,
        catalyst_fk INT NOT NULL,
        mass_stycast FLOAT NOT NULL,
        mass_gluemix FLOAT NOT NULL,
        ini_pressure FLOAT NOT NULL,
        mass_bare_sample FLOAT NOT NULL,
        mass_glued_sample FLOAT NOT NULL,
        corr_slope FLOAT NOT NULL,
        corr_pressure FLOAT NOT NULL,
        datetime_deposit TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        duration_sample TIME NOT NULL,
        duration_deposit TIME NOT NULL,
        cond_fk INT NOT NULL,
        result BOOLEAN NOT NULL,
        FOREIGN KEY (cell_fk)
            REFERENCES Cells(cell_id),
        FOREIGN KEY (stycast_fk)
            REFERENCES Stycast(stycast_id),
        FOREIGN KEY (catalyst_fk)
            REFERENCES Catalyst(catalyst_id),
        FOREIGN KEY (cond_fk)
            REFERENCES Conditions(cond_id)
    );
    """

cleaning_query = """
    DROP TABLE IF EXISTS
        GlueDeposit,
        Stycast,
        Catalyst,
        Cells,
        VisualDefects,
        Modules,
        Conditions;
    """

#-- Executing the commands
if args.recreate:
    print("Dropping all tables")
    cursor.execute(cleaning_query)

print("Creating Conditions table")
cursor.execute(conditions_creation)
print("Creating Modules table")
cursor.execute(module_creation)
print("Creating VisualDefects table")
cursor.execute(inspection_creation)

print("Creating Cells table")
cursor.execute(cell_creation)
print("Creating Stycast table")
cursor.execute(stycast_creation)
print("Creating Catalyst table")
cursor.execute(catalyst_creation)
print("Creating GlueDeposit table")
cursor.execute(deposit_creation)

conn.commit()
cursor.close()
conn.close()

