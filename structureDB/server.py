from flask import Flask

app = Flask(__name__)

# Route principale pour lancer le serveur local
@app.route('/')
def index():
    return "Serveur local en cours d'exécution..."

if __name__ == '__main__':
    app.run(debug=True)   

    

 