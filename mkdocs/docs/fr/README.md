# :clap: CLApp ! 

*Une App pour le Cell Loading*

Cette application permet de cadrer le processus *Cell loading* permettant le
collage des modules Pixel ITKPixV2 sur les cellules en graphite pyrolitique.
L'activité _Cell Loading_ sera effectuée par deux opérateurs, l'un réalisant
les tâches techniques, l'autre contrôlant le bon déroulement des opérations,
guidé pas à pas par l'application CLApp sur une tablette numérique.

CLApp sert le double objectif de guider précisément l'opérateur tout en
renseignant en continue une base de données (YalDB) avec les paramètres à surveiller.
L'objectif est ainsi de :

* minimiser les erreurs induites par des tâches répétitives,
* avoir un processus particulièrement opérateur-indépendant, 
* stocker informations nécessaires de façon transparentes,
* s'assurer de l'intégrité des valeurs stockées. 

En bout chaine l'application permet de verser automatiquement dans la base de
données de production d'ITk les informations nécessaires.
Les paramètres ainsi gardés permettront ensuite un suivi de la production afin
de détecter en amont des problèmes systématiques ou des dérives de certains
paramètres.

Un démonstrateur de cette application a été déployé sur CERN Openshift. Il est accessible [ici](https://demonstrator-clapp.app.cern.ch/).
