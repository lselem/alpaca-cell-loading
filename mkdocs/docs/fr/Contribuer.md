Le présent [projet](https://gitlab.cern.ch/lselem/alpaca-cell-loading) est
structuré en différents dossier:

* `mkdocs/` : contient la présente documentation.

* `config/` : contient les `dockerfiles` utilisés par compose pour construire
  les services __CLApp__ et __YalDB__. Les fichiers de configuration `*.env`
  sont égalements rangés ici. Enfin un fichier de log example est présent
  également pour être utilisé par le démonstrateur.

* `structureDB/` : contient des scripts permettant d'interagir facilement avec
  la base de données, notamment le script `makeDBTables.py` qui recrée toutes
  les tables de la base de données et permet ainsi d'archiver sa structure. Le
  fichier de *dump* SQL `YalDB_dump.sql` est également stocké ici.

* `appCode/` : contient le code python de l'application à proprement
  parler.

    * `appMain.py`: fichier principal de l'application. Gère l'architecture de
      la page d'accueil.
  
    * `generalAppConfig.py` : contient la configuration générale de
      l'application, notamment en ce qui concerne le laboratoire d'accueil ou la
      langue utilisée. 
  
    * `allText.py` : contient un dictionnaire qui archive tous les morceaux de
      textes utilisés dans l'application en version française et anglaise.
    
    * `utils/` : contient des fonctions générales utilisées à travers tout le
      code.
  
    * `figures/` : contient des images utilisées par l'application pour
      documenter cetaines étapes.

    * Les différentes pages sont dans le dossier `pages/`. Elles ont les noms qui
      apparaissent dans l'application préfixé d'un numéro qui gouverne leur ordre
      d'apparition dans la barre latérale.
