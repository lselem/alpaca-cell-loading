## Infrastructure

L'application, qui prend le nom de __CLApp__ (*Cell Loading App*), se présente
sous la forme d'une page `html` développée sous Python avec le module
`Streamlit`.  Cette page `html` interactive sert à renseigner une base de
donnée `PostGreSQL` baptisée __YalDB__ (*Yet Another Local DataBase*).
__CLApp__ est capable de renseigner ou d'interroger __YalDB__ grâce au module
python `psycopg2`.

L'application est complètement contenèrisée via *Docker* et son greffon *Docker
Compose*.  Elle a vocation a être déployée facilement par ce biais sur un
serveur interne à chaque laboratoire en ayant le besoin.  __YaldDB__ est
hébergée dans un *volume* sur le serveur. Les images ne sont pas directement
stockées dans __YalDB__ mais plutôt dans un autre volume, la base de donnée
ne retenant que leur adresse dans ledit volume.  Enfin, un répertoire sur le
serveur hébergeant l'application est monté avec l'application. Ainsi on peut y
déposer des fichiers pendant le processus qui sont automatiquement accessibles
depuis l'application. 

L'application est accessible en utilisant un navigateur web connecté au réseau
interne du laboratoire. L'opérateur muni d'une tablette tactile peut donc y
accéder via un simple navigateur internet. __Le laboratoire utilisateur n'a
donc besoin que d'un serveur pour faire tourner cette application et une
tablette tactile__ (ou à défaut un ordinateur portable) pour pouvoir y accéder
pendant les opérations.

## Déploiement :rocket:

### Le projet Git :material-gitlab:

On supposera que l'utilisateur possède l'infrastructure décrite ci-dessus. Pour
déployer l'application, il faut dans un premier temps cloner ce projet git sur
le serveur qui fera tourner l'application python:
```
git clone https://gitlab.cern.ch/lselem/alpaca-cell-loading.git
``` 

!!! Note

    En réalité, s'il s'agit d'être uniquement utilisateur de l'application, le
    fichier `compose.yaml` et un fichier de configuration`.env` (voir ci-dessous)
    suffisent.

### Configuration dépendant du laboratoire :material-microscope:

L'application a besoin de certaines variables globales dépendantes de la
configuration du laboratoire l'utilisant. Ces variables globale sont récupérées
par le module `appCode/generalAppConfig.py` qui s'occupe de charger la
configuration correspondante pour le reste du code.

Les fichiers de configuration en `*.env` sont archivés dans le dossier
`config/` avec les `Dockerfiles` utilisés par l'application. Par défaut il
contient le modèle `template_config.env`:
``` python
#-- Tag to identify the docker images of both CLApp and YalDB
APP_TAG= "dev"

#-- Institute where the loading is happening
#   --> Used to check component indeed registered in this lab in the ProdDB
#   --> Used to adapt the process to specificities of the lab
LAB_NAME= "Loading Site"

#-- List of operators in the lab to be tracked in the DB
#   --> Separate each name with ':'
OPERATORS= "Alice:Bob:Charly"

#-- Language used by the application in all text
#   For now, only French and English supported
LANGUAGE= "English"

#-- Directory where log files will appear in the app server
#   --> A bind mount to this location is done in the app
DIR_LOG_FILE= "/path/to/log/files/"

#-- To be used to store images locally outside of a docker volume 
DIR_IMAGES= "/path/to/image/server/" #FIXME: not used yet. To be used ?
```

On notera que les paramètres d'accès à la base de données sont codés en dur
dans `generalAppConfig.py` puisque tout est défini au moment de la
contenèrisation.

### Configuration initiale du projet

Quand le fichier de configuration du laboratoire est réalisé, il suffit
d'exécuter à la racine du projet
```
source setup.sh
```
Ce script demandera de choisir le fichier de configuration à utiliser par
l'application. Il en créera ensuite un raccourci à la racine du projet sous le
nom `.env`. 

Pour s'assurer que toute la configuration de l'application est correcte, on
peut exporter manuellement toutes les variables d'environnement du fichier de
configuration puis exécuter :
```
cd appCode/
python generalAppConfig.py
```
Ceci affichera toutes les variables globales telles qu'elles sont utilisées par
l'application.

Pour changer de fichier de configuration, il suffit de supprimer le lien
symbolique `.env` à la racine du projet relancer le script `setup.sh` avec un
nouveau fichier. 

### Base de données :material-database:

!!! Note

    Si le volume de la base de données n'existe pas déjà, la base de données
    est initialisée avec le fichier `structureDB/YalDB_dump.sql` au lancement du
    conteneur. Il n'y a donc généralement rien de particulier à faire. 

On peux tester l'accès à __YalDB__ grâce au script `structureDB/touchDB.py`. En
l'exécutant, on test qu'on arrive bien à se connecter, écrire et lire la DB.
```
cd structureDB/
python touchDB.py --host {server}
```
Se script prend en entrée l'adresse du serveur sur lequel tourne l'application
et donc aussi le volume contenant __YalDB__. On peut optionnellement utiliser le
paramètre `--port` si le port par défaut 5432 n'est pas utilisé.


Le script `structureDB/makeDBTable.py` contient à tout moment les commandes SQL
nécessaires à la construction complète de toutes les tables de la base de
données attendues par l'application.
```
cd structureDB/
python makeDBTable.py --host {server}
```

!!! Warning

    N'exécutez ce script que pour initialiser une base de données vierge ou si
    vous savez ce que vous faites ! Normalement docker démarre avec toute la
    structure de base de donnée nécessaire.

Si ce script est utilisé sur une base de données existante, en l'absence du
paramètre `--recreate` rien ne se passe. Si ce paramètre est présent, ce script
va alors détruire toutes les tables de la base de données pour les remplacer
par des tables vides. Ça peut être utile en période de conception pour
s'adapter à des évolutions de structure tandis que les données conservées ne
sont que des tests. Mais ce n'est pas à utiliser à la légère.

## Démarrer / arrêter l'application sur le serveur :fontawesome-solid-power-off:

Pour lancer l'application (en supposant que le lien `.env` a été déjà été créé)
on ouvre un terminal sur le serveur où elle doit tourner puis, à la racine du
projet, on lance *Docker Compose* en mode détaché:
```
docker compose up -d
```
On peut ensuite fermer le terminal, l'application continuera à tourner.
Pour fermer l'application on retourne sur le serveur où elle tourne et 
on exécute :
```
docker compose down
```
On peut ensuite revenir sur le serveur modifier des morceaux de code. Il faudra
alors redémarrer l'application avec:
```
docker compose down
docker compose build
docker compose up -d
```

On accède à l'application avec un navigateur web en se connectant à l'adresse
du serveur sur lequel elle tourne au port `8501`. Ce port est utilisé par
défaut par `streamlit` mais il est possible d'en choisir un autre en changeant
le port exposé par le conteneur dans le fichier compose.
