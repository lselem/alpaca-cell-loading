!!! Warning
    
    Cette page n'est pas à jour ! 

## Principe général

L'application remplit plusieurs rôles :

* Elle sert de _check list_ au travers de boîtes de contrôles tout ou rien,
  permettant de rappeler les étapes importantes de la liste des tâches à
  réaliser afin d’éviter les oublis.

* Elle sert de fiche de calcul, pour donner à l’utilisateur les informations
  dont il a besoin pour réaliser les tâches (par exemple : calcul de la masse
  de catalyseur qui doit être ajouté a la colle lors de la pesée de celle-ci ;
  calcul de la pression d’air à après la mesure d’un échantillon de dépôt).

* Elle sert à tracer les activités en archivant toutes les données acquises
  pendant une opération de collage dans une base de donnée locale.

* Enfin elle permettra de renseigner la base de donnée de production du projet
  ITk de façon automatique.

La traçabilité des opérations sera un outil essentiel pendant les opérations
pour mener les investigations sur l’origine de problèmes qui pourraient
survenir.  Par ailleurs, l’archivage des opérations permettra un suivi des
activités pour avoir une vue en temps réelle de l’avancement de la
construction. On détectera ainsi d'éventuelles dérives de certains paramètres,
ce qui pourrait indiquer une usure, un déréglage ou une mauvaise utilisation
des procédures.

## Structure de l'application côté utilisateur


### Page d'accueil

L'application se présente sous la forme d'une page accessible avec un
navigateur web.  Sa structure hérite de son développement basé sur le paquet
python `streamlit`. On a un page d'accueil `appMain` et une barre latérale qui
contient sept autres pages.  La page d'accueil sert à renseigner la référence
de l'objet sur lequel on travail. On peut choisir entre un module, une cellule,
de la résine Stycast ou du catalyseur. 

!!! Note
    
    Les options Stycast et Catalyseur ne sont pas encore actives

La sélection d'un objet permet d'entrer le numéro de série ITk de l'objet au
clavier.

!!! Note

    À l'avenir, on pourra obtenir le meme résultat en scannant un QR code sur l'objet.

    Un appel à la ProdDB ITk permettra également de vérifier l'existence de l'objet.

<!---
![Page d'accueil](AppOrga.png "Page d'accueil")
-->

### Interactions avec la base de donnée

Une fois un objet choisi, la page d'accueil indique vers quel page se rendre
pour commencer (ou poursuivre) le processus avec cet objet. Ces pages
correspondent chacune à une étape définie du processus et le succès d'une étape
débloque la (ou les) page(s) de l'étape suivante. Ainsi l'opérateur est
contraint par l'application à effectuer les étapes dans un certain ordre. À la
fin d'une étape, on verse dans la base de donnée son succès ou non sous la
forme d'un booléen, accompagné d'un certain nombre de paramètres.

Toutes ces étapes ont en commun de conserver la traçabilité de la personne
effectuant l'opération, la date et l'heure de l'opération, la température de la
salle et l'humidité de celle-ci. Ces conditions environnementales sont remplies
dans la barre latérale (à l'exception de la date qui est automatiquement
obtenue par la base de donnée). Au commencement d'une session, l'opérateur doit
impérativement remplir ces conditions environnementales avant d'effectuer
quelque étape que ce soit. Ensuite, ces conditions sont conservée en mémoire
pendant toute la session et il revient à l'opérateur de les mettre à jour si
nécessaire.

### Étapes du Module

Le choix de l'option `Module` dans le sélecteur et l'entrée d'un numéro de série valide
permet de débloquer les pages correspondant aux étapes de production concernant
le module.  Ces pages se débloquent successivement au fur et à mesure de la
progression du module dans le processus. Il s'agit des pages:

* __RÉCEPTION MODULE__ : Données et étapes à faire à la réception du module
  dans son *carrier*. Une nouvelle entrée est crée dans la base de donnée locale et
  les informations entrées y sont versées.

* __INSPECTION VISUELLE__ : On enregistre dans la base de donnée les photos des
  éventuels défauts trouvés sur le module. Au premier passage, on peut choisir
  de passer l'étape en cochant l'absence de défaut. Ceci débloquera l'étape
  suivante. Si cette étape a été validée, on peut toujours ajouter de nouveaux
  défauts. On peut également afficher les défauts déjà enregistrés. 

* __DÉCOUPE DU FLEX__ : La procédure étant encore en développement, cette étape
  se contente de demander un commentaire et une validation à l'utilisateur. 

### Étapes de la Cellule

Le choix de l'option `Cellule` dans le sélecteur et l'entrée d'un numéro de
série valide débloquera de la même manière les pages correspondant aux
cellules~: 

* __PRÉPARATION CELLULE__ : On enregistre une nouvelle cellule dans la base de donnée.

* __ALIGNEMENT CELLULE MODULE__ : Ici, on apparie la cellule à un module. Le
  module choisi doit avoir fini ses trois étapes préliminaires, sinon
  l'utilisateur ne peut pas progresser plus loin dans cette étape. On a ensuite
  une *checklist* qui sert à guider l'opérateur à travers les différentes étapes
  du processus d'alignement et des images explicatives. Les informations de cette
  étape (notamment l'appariement à un module) sont ensuite versées dans la
  base de donnée.

* __DÉPÔT DE COLLE__ : Cette section assiste l'opérateur dans le calcul des
  différents paramètres du dépôt de colle (préparation du mélange avec le
  catalyseur, pression du dispenseur, etc.). À nouveau, les données sont versées
  dans la base de donnée.
