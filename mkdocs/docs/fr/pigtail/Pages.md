# **Pages**     :page_facing_up:

## **AppMain**

### Principe général :gear:
La page `AppMain.py` sert de page d'accueil de l'application. Elle permet à l'utilisateur de sélectionner et d'identifier l'objet sur lequel il va travailler. Le choix de l’objet `pigtail` mène à la section dédiée à l'intégration des `pigtails sur cellule`.

### Fonctionnement   :rocket:
Sur cette page, l'utilisateur peut choisir l’une des 4 `saveurs` pour le pigtail entre `Top`, `Bot`, `Front` et `Back`. Il peut sélectionner son choix via un menu déroulant ou scanner la `datamatrix` directement. 

L’opérateur a également le choix de rentrer manuellement le numéro `d’identification` correspondant si le scan est difficile.

!!! Note
    Actuellement, l’intégration avec la base de données n’est pas implémentée. La vérification du pigtail sera développée plus tard dans l’application.


## **LAPP Menu**

### Principe général :gear:
Le `LAPP_Menu.py` est la page principale qui suit `AppMain.py`. Elle sert de menu central pour entrer des informations importantes et choisir le processus souhaité.

### Fonctionnement   :rocket:
Après avoir sélectionné et validé le choix 'pigtail' dans 'AppMain.py', l'utilisateur est invité à se rediriger vers la page `LAPP Menu`, ce qui lui permet de vérifier ou de changer son choix d'objet avant de poursuivre. Cette page permet de garder en mémoire les informations telles que la `température` et l'`humidité`, qui sont essentielles pour le suivi et la traçabilité.

L'utilisateur peut naviguer entre les différentes étapes du processus à partir de ce menu via la `barre latérale`.

!!! Note
    L'idée est que l'utilisateur puisse modifier les informations importantes saisies au départ grâce à la barre latérale, si nécessaire.


## **Position of Tools**

### Principe général :gear:
La page `Position_of_Tools.py` guide l'utilisateur dans le positionnement correct des outils nécessaires pour le processus de `montage des pigtails` sur cellule.

### Fonctionnement :rocket:
L'utilisateur peut visualiser des `images` et des `schémas` détaillant le positionnement des outils sur la `breadboard` ainsi que spécifiquement pour la `saveur` choisie.

!!! Note
    Les images sont récupérées depuis la base de données pour garantir une précision et une conformité aux standards.


## **Adjustement and Tools**

### Principe général :gear:
La page `Adjustement_and_Tools.py` est conçue pour assister l'utilisateur dans l'ajustement des outils nécessaires pour le processus de `montage des pigtails`.

### Fonctionnement   :rocket:
La page Adjustement and Tools permet à l’utilisateur de choisir entre deux options, `First adjustments` ou `Already Adjusted`, dans un menu déroulant.

Le premier choix permet de voir les images correspondantes aux étapes d’ajustements des outils `un à un`. Cela force l’utilisateur à suivre toutes les étapes et à cocher des `cases` sur la page pour indiquer les étapes réellement effectuées, reflété dans la `barre latérale` gauche.

La deuxième option permet de choisir l’image correspondante à l’`étape voulue` par l’utilisateur sans devoir passer par toutes les étapes, via un menu déroulant qui affiche l’image correspondante à l’`ajustement sélectionné`.

!!! Note
    Le suivi des étapes via la base de données n'est pas encore implémenté.


## **Mounting on Cells**

### Principe général :gear:
La page `Mounting_on_Cells.py` guide l'utilisateur à travers le processus d'intégration des `pigtails sur cellule` en suivant des étapes précises et séquentielles.

### Fonctionnement   :rocket:
Chaque étape est présentée avec des images et des instructions détaillées. Les `cases à cocher` permettent de confirmer la complétion de chaque étape. L'utilisateur peut naviguer entre les pages pour anticiper les prochaines étapes grâce aux `boutons de navigation`, mais il ne peut cocher que les `cases` de la page actuelle. Les étapes doivent être suivies dans l'ordre, garantissant ainsi le respect du processus.

Chaque étape complétée est reflétée dans la `barre latérale` pour aider les opérateurs à se rappeler des étapes réalisées et pour assurer un suivi dans la base de données.

### Interactivité    :fontawesome-solid-tablet-screen-button:
Ce processus inclut une partie interactive pour l'utilisation de la `résine de colle`.

L’utilisateur a **deux choix** de résine : 

Une avec un temps de collage de `30 minutes` et une autre avec un temps de collage de `24 heures`. Le processus exige d'attendre impérativement la fin du collage avant de passer aux étapes suivantes. Une base de données `PostgreSQL` est utilisée pour fournir cette information, remplaçant ainsi l’utilisation d’un `timer`, qui bloquait le code.

!!! Note
    Le suivi des étapes via la base de données n'est pas encore implémenté.

!!! Warning
    La dernière case à cocher validera toutes les étapes.

## **LAPP to prodDB**

### Principe général :gear:
La page LAPP to prodDB est destinée à l'intégration des `données` dans la `base de données` de production.

### Fonctionnement   :rocket:

!!! Note
    Bien que cette page ne soit pas encore implémentée, son objectif sera de transférer automatiquement les données collectées pendant le processus de montage vers la base de données de production, assurant ainsi une traçabilité complète et une mise à jour en temps réel des informations.