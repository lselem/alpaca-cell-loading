# Description de l'application :simple-streamlit:

La `WebApp` sert à guider les utilisateurs à travers différentes étapes de `montage des pigtails sur cellules`, du détecteur ITk d’ATLAS. Elle a plusieurs rôles essentiels:

- **Checklist interactive** : L'application présente des cases à cocher pour chaque étape du processus, garantissant que toutes les étapes sont suivies et complétées dans le bon ordre, ce qui évite les oublis.
- **Guide d'instructions** : Elle fournit des instructions détaillées et des images pour aider les opérateurs à positionner et ajuster les outils correctement.
- **Enregistrement et traçabilité** : Toutes les données acquises pendant les opérations sont archivées dans une base de données (à réaliser). Cela inclut les conditions environnementales comme la température et l'humidité, ainsi que l'état de chaque étape de montage.
- **Interaction avec la base de données** : L'application est conçue pour s'intégrer avec une base de données de production, permettant de vérifier et de valider les composants, et de mettre à jour automatiquement l'état des opérations.
- **Support pour la prise de décisions** : En offrant une interface claire et interactive, l'application aide les opérateurs à anticiper les étapes suivantes et à se préparer en conséquence, garantissant ainsi une meilleure planification et exécution des tâches.

!!! Note
    Cette application assure la qualité et la traçabilité des opérations de montage des pigtails, tout en facilitant le travail des opérateurs grâce à une interface utilisateur intuitive et interactive.
