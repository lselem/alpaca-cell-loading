# **Ressources**

## Guide :material-file-document:
Un guide simplifié est disponible, nommé `Guide_Installation_Streamlit`, qui a été utile lors d’un test rapide de la webapp sur une autre machine. Il permet de donner plus de détails notamment pour la partie permettant de configurer le `serveur` ainsi que la `base de données`.

!!! Note
    Le guide est disponible dans le dossier `/utility` du repository.

## Scripts :material-script-text-outline:

### Serveur FLASK :simple-flask:
Le script FLASK `server.py` encapsule l'application `Streamlit` pour l'isoler du travail des autres développeurs sur le serveur du laboratoire. Il lance un serveur local `FLASK` qui affiche un message confirmant que le serveur est en marche.

### Script d'insertion des images :simple-postgresql:
Ce script `insert_images.py` permet d'ajouter des images dans la base de données de test. Les images sont lues et stockées dans la base de données pour être utilisées dans les étapes spécifiques des processus de l'application.

### InfluxDB :simple-influxdb:
Le script `InfluxDB.py` utilise la bibliothèque InfluxDB pour interagir avec une base de données de séries temporelles. Il se connecte à une instance InfluxDB, exécute une requête pour obtenir des données de température et affiche les résultats. Cette fonctionnalité est utile pour surveiller les conditions environnementales en temps réel pendant les processus.

!!! Note
    Les scripts sont disponibles dans le dossier `/utility` du repository.


### Commandes SQL utiles pour la base de données :material-database:
Il y a plusieurs commandes SQL importantes :

- **Création de la table images** : Stocke les images utilisées dans le processus.
- **Table pigtails** : Stocke des informations telles que les données des QR codes et les conditions environnementales.
- **Table process_steps** : Suivi des étapes de chaque processus de pigtail.
- **Table adjustment_checks** : Suivi des ajustements effectués pour chaque pigtail.
- **Table mounting_checks** : Suivi des vérifications de montage pour chaque pigtail.

!!! Note
    Les commandes SQL sont disponibles dans le dossier `/utility` du repository.