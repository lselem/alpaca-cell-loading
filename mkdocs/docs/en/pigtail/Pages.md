# **Pages**    :page_facing_up:

## **AppMain**

### General Principle    :gear:
The `AppMain.py` page serves as the application's home page. It allows the user to select and identify the object they will work on. Choosing the `pigtail` option leads to the section dedicated to integrating `pigtails on cells`.

### Functioning  :rocket:
On this page, the user can choose one of the 4 `flavors` for the pigtail: `Top`, `Bot`, `Front`, and `Back`. They can select their choice via a dropdown menu or scan the `datamatrix` directly.

The operator also has the option to manually enter the corresponding `identification number` if scanning is difficult.

!!! Note
    Currently, integration with the database is not implemented. Verification of the pigtail will be developed later in the application.

## **LAPP Menu**

### General Principle    :gear:
The `LAPP_Menu.py` is the main page that follows `AppMain.py`. It serves as a central menu to enter important information and choose the desired process.

### Functioning  :rocket:
After selecting and validating the `pigtail` choice in `AppMain.py`, the user is invited to redirect to the `LAPP Menu` page, allowing them to verify or change their object choice before proceeding. This page retains information such as `temperature` and `humidity`, which are essential for tracking and traceability.

The user can navigate between the different process steps from this menu via the `sidebar`.

!!! Note
    The idea is that the user can modify the important information entered at the beginning using the sidebar if necessary.

## **Position of Tools**

### General Principle    :gear:
The `Position_of_Tools.py` page guides the user in correctly positioning the tools needed for the `pigtail mounting` process on cells.

### Functioning  :rocket:
The user can view `images` and `diagrams` detailing the positioning of tools on the `breadboard` as well as specifically for the chosen `flavor`.

!!! Note
    The images are retrieved from the database to ensure precision and compliance with standards.


## **Adjustement and Tools**

### General Principle    :gear:
The `Adjustement_and_Tools.py` page is designed to assist the user in adjusting the necessary tools for the `pigtail mounting` process.

### Functioning  :rocket:
The Adjustement and Tools page allows the user to choose between two options, `First adjustments` or `Already Adjusted`, in a dropdown menu.

The first choice allows viewing images corresponding to the tool adjustment steps `one by one`. This forces the user to follow all steps and check `boxes` on the page to indicate the steps actually completed, reflected in the left `sidebar`.

The second option allows the user to choose the image corresponding to the `desired step` without going through all the steps, via a dropdown menu that displays the image corresponding to the `selected adjustment`.

!!! Note
    The tracking of steps via the database is not yet implemented.


## **Mounting on Cells**

### General Principle :gear:
The `Mounting_on_Cells.py` page guides the user through the process of integrating `pigtails on cells` by following precise and sequential steps.

### Functioning  :rocket:
Each step is presented with detailed images and instructions. `Checkboxes` allow confirmation of each step's completion. The user can navigate between the pages to anticipate the next steps using the `navigation buttons`, but can only check the `boxes` for the current page. Steps must be followed in order, ensuring process compliance.

Each completed step is reflected in the `sidebar` to help operators remember completed steps and ensure tracking in the database.

### Interactivity :fontawesome-solid-tablet-screen-button:
This process includes an interactive part for using `resin glue`. 

The user has **two resin** choices: 

One with a bonding time of `30 minutes` and another with a bonding time of `24 hours`. The process requires waiting for the bonding to complete before proceeding to the next steps. A `PostgreSQL` database is used to provide this information, replacing the use of a `timer`, which blocked the code.

!!! Note
    The tracking of steps via the database is not yet implemented.

!!! Warning
    The last checkbox will validate all steps.

## **LAPP to prodDB**

### General Principle    :gear:
The LAPP to prodDB page is intended for integrating `data` into the production `database`.

### Functioning  :rocket:

!!! Note
    Although this page is not yet implemented, its objective will be to automatically transfer the data collected during the mounting process to the production database, ensuring complete traceability and real-time information updates.