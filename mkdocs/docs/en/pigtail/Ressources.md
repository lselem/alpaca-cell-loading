# **Ressources**

## Guide :material-file-document:
A simplified guide is available, named `Guide_Installation_Streamlit`, which was useful during a quick test of the web app on another machine. It provides more details, especially for configuring the `server` and the `database`.

!!! Note
    The guide is available in the `/utility` folder of the repository.

## Scripts :material-script-text-outline:

### FLASK Server  :simple-flask:
The FLASK script `server.py` encapsulates the `Streamlit` application to isolate it from the work of other developers on the laboratory server. It launches a local `FLASK` server that displays a message confirming the server is running.

### Image insertion script    :simple-postgresql:
This script `insert_images.py` adds images to the test database. The images are read and stored in the database for use in specific steps of the application's processes.

### InfluxDB  :simple-influxdb:
The script `InfluxDB.py` uses the InfluxDB library to interact with a time-series database. It connects to an InfluxDB instance, executes a query to get temperature data, and displays the results. This functionality is useful for monitoring environmental conditions in real-time during the processes.

!!! Note
    The scripts are available in the `/utility` folder of the repository.

## SQL Commands :material-database:
There are several important SQL commands:

- **Creation of the images table**: Stores images used in the process.
- **Pigtails table**: Stores information such as QR code data and environmental conditions.
- **Process_steps table**: Tracks the steps of each pigtail process.
- **Adjustment_checks table**: Tracks adjustments made for each pigtail.
- **Mounting_checks table**: Tracks mounting verifications for each pigtail.

!!! Note
    The SQL commands are available in the `/utility` folder of the repository.