# General Application :simple-streamlit:

The `WebApp` serves to guide users through the various steps of `mounting pigtails on cell`, part of the ATLAS ITk detector. It has several essential roles:

- **Interactive checklist**: The application presents control boxes for each step of the process, ensuring that all steps are followed and completed in the correct order, preventing omissions.
- **Instruction guide**: It provides detailed instructions and images to help operators position and adjust the tools correctly.
- **Recording and traceability**: All data acquired during operations is archived in a database (to realize). This includes environmental conditions such as temperature and humidity, as well as the status of each mounting step.
- **Database interaction**: The application is designed to integrate with a production database, allowing components to be verified and validated, and automatically updating the status of operations.
- **Decision support**: By offering a clear and interactive interface, the application helps operators anticipate the next steps and prepare accordingly, ensuring better planning and task execution.

!!! Note
    This application ensures the quality and traceability of pigtail mounting operations, while facilitating the work of operators with an intuitive and interactive user interface.
