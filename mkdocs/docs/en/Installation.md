## Infrastructure required

The application, called __CLApp__ (*Cell Loading App*), takes the form of
an `html` page developed in Python with the `Streamlit` module.
This interactive `html` page is used to populate a `PostGreSQL`
database called __YalDB__ (*Yet Another Local DataBase*).  __CLApp__ is able to
populate or query __YalDB__ thanks to the python `psycopg2` module.

The application is fully contained via `Docker` and its `Docker compose`
plugin. In this way, it can be easily deployed on an internal server for each
laboratory requiring it.  __YaldDB__ is hosted in a *volume* on the server.
Images are not directly stored in __YalDB__ but rather in another *volume*,
with the database only retaining their address in said *volume*.  Finally, a
directory on the server hosting the application is mounted in `Docker`, so that
the user can access files deposited there during the process through the
application.

The application can be accessed using a web browser connected to the
laboratory's network. Operators equipped with a touchscreen tablet can access
it through a simple web browser.  __All the user lab needs is a server to run the
application on, and a touchscreen tablet__ (or laptop) to access it during operations.

## Deployment :rocket:
### The Git project :material-gitlab:

We'll assume that the user has the infrastructure described above. To deploy
the application, we first need to clone this git project on the server running
the python application:
```
git clone https://gitlab.cern.ch/lselem/alpaca-cell-loading.git
``` 

!!! Note

    In fact, to be only a user of the application, the `compose.yaml` file and
    one `.env` configuration files (see below) are enough.

### Lab-dependent configuration :material-microscope:

The application requires certain global variables depending on
the configuration of the laboratory using it. These global variables are
retrieved by the `appCode/generalAppConfig.py` module, which then
takes care of loading the corresponding configuration
for the rest of the code.

The `*.env` configuration files are archived in the `config/` folder along with
the `Dockerfiles` used by the application. By default it contains the
`template_config.env` template:
``` bash
#-- Tag to identify the docker images of both CLApp and YalDB
APP_TAG= "dev"

#-- Institute where the loading is happening
#   --> Used to check component indeed registered in this lab in the ProdDB
#   --> Used to adapt the process to specificities of the lab
LAB_NAME= "Loading Site"

#-- List of operators in the lab to be tracked in the DB
#   --> Separate each name with ':'
OPERATORS= "Alice:Bob:Charly"

#-- Language used by the application in all text
#   For now, only French and English supported
LANGUAGE= "English"

#-- Directory where log files will appear in the app server
#   --> A bind mount to this location is done in the app
DIR_LOG_FILE= "/path/to/log/files/"

#-- To be used to store images locally outside of a docker volume 
DIR_IMAGES= "/path/to/image/server/" #FIXME: not used yet. To be used ?
```

Note that the database access parameters are hard-coded in
in `generalAppConfig.py` since everything is defined at the time of
the dockerisation.

### Initial project configuration

Once the laboratory configuration file has been created, simply run
at the root of the project
```
source setup.sh
```
This script will ask you to select the configuration file to be used by the
application. It will then create a symbolique link at the root of the project
under the name `.env`

To ensure that all the application's configuration is correct, you can manually
export all the environment variables in the configuration file and then run :
```
cd appCode/
python generalAppConfig.py
```
This will display all variables as they are used by the application.

To change the configuration file, simply remove the `.env` symbolic link at
the root of the project and relaunch the `setup.sh` script with a new file. 

### Database :material-database:

!!! Note

    If the database volume does not already exist, the database is initialized
    with the file `structureDB/YalDB_dump.sql` at container launch.  container. So
    there's usually nothing special to do.

You can test access to __YalDB__ using the `structureDB/touchDB.py` script.
Executing it, we test that we can connect to, write to and read the DB.
```
cd structureDB/
python touchDB.py --host {server}
```
This script takes as input the address of the server on which the application
is running and therefore also the volume containing __YalDB__. You can
optionally use the `--port` parameter if the default port 5432 is not used.

The `structureDB/makeDBTable.py` script contains at all times the SQL commands
necessary for the complete construction of all database tables expected by the
application.
```
cd structureDB/
python makeDBTable.py --host {server}
```

!!! Warning

    Only run this script to initialize a blank database, or if you
    you know what you are doing! Normally, docker starts with all the necessary
    database structure.

If this script is used on an existing database, in the absence of the parameter
`--recreate` nothing happens. If this parameter is present, this script will
then destroy all tables in the database and replace them with empty tables.
This can be useful during the design phase to adapt to changes in structure,
while the data retained are for testing purposes only. But it should not be
used lightly.

## Start / stop the application on the server :fontawesome-solid-power-off:

To launch the application (assuming the `.env` link has already been created)
open a terminal on the server where it is to run, then, at the root of the
project root, launch *Docker Compose* in detached mode:
```
docker compose up -d
```
You can then close the terminal, and the application will continue to run. To
close the application, return to the server where it is running and execute :
```
docker compose down
```
You can then return to the server to modify pieces of code. You will then need
to restart the application:
```
docker compose down
docker compose build
docker compose up -d
```

The application is accessed with a web browser by connecting to the address of
the server on which it runs, to port `8501`. This port is used by by
`streamlit`, but it is possible to choose another by changing the port exposed
by the container in the compose file.
