This [project](https://gitlab.cern.ch/lselem/alpaca-cell-loading) is
structured into different folders:

* `mkdocs/` : contains the present documentation.

* `config/`: contains the `dockerfiles` used by compose to build
  the __CLApp__ and __YalDB__ services. The `*.env` configuration files
  are also stored here. Finally, an example log file is present
  to be used by the demonstrator.

* `structureDB/`: contains scripts for easy interaction with the database.
  Notably there is the `makeDBTables.py` script, which recreates all the
  tables in the database, enabling its structure to be archived. The
  SQL *dump* file `YalDB_dump.sql` is also stored here.

* `appCode/`: contains the application's python code.
  itself.

    * `appMain.py`: main application file. Manages the architecture of the home page.
  
    * `generalAppConfig.py`: contains the application general
      configuration, particularly with regard to the home laboratory or the
      language used. 
  
    * `allText.py`: contains a dictionary which archives all the text used in
      the application in both French and English.

    * `utils/`: contains general functions used throughout the code.
  
    * `figures/` : contains images used by the application to document certain
      steps of the process.
    
    * The various pages are located in the `pages/` folder. They have the names
      prefixed by a number that governs their order of appearance in the sidebar.
