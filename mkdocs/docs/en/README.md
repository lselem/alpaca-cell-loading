# :clap: CLApp !

*The Cell Loading App*

This application provides a framework for the *Cell loading* process, enabling
the loading of ITKPixV2 pixel modules onto pyrolitic graphite cells. The _Cell Loading_
activity will be carried out by two operators, one performing the technical
tasks, the other monitoring the running of operations, guided step by
step by the CLApp application on a digital tablet.

CLApp serves the dual purpose of precisely guiding the operator while
continuously updating a database (YalDB) with the parameters to be monitored. The aim
is to :

* minimize errors induced by repetitive tasks,
* have a particularly operator-independent process, 
* store the necessary information transparently,
* ensure the integrity of stored values. 

At the end of the chain, the application automatically feeds ITk
production database. The parameters stored in this way can then be used to
monitor production in order to detect early-on systematic problems or
deviations in certain parameters.


A demonstrator of this application has been deployed on CERN Openshift. It is available [here](https://demonstrator-clapp.app.cern.ch/).
