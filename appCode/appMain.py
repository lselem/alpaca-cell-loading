import os

from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *
from utils.ProdDBaccess import AuthenticateUser
from utils.ProdDBaccess import DbGet
from utils.menu import Menu, Empty_menu
from streamlit_extras.switch_page_button import switch_page

#from pylibdmtx.pylibdmtx import decode
from PIL import Image


st.set_page_config(
    page_title=txt['homeTitle'],
    page_icon=":clap:",
    layout="centered",
    initial_sidebar_state="expanded",
    menu_items={
        'Get Help': 'https://cell-loading-app.docs.cern.ch/fr/',
        #'Report a bug': "luka.selem@lpsc.in2p3.fr",
        'About': "CLApp, a cell loading app. Developped by LPSC and CPPM."
        }
    )

# Initialisation de `st.session_state`
if 'informations' not in st.session_state:
    st.session_state['informations'] = {
        'Name': '',
        'Temperature': '',
        'Humidity': '',
        'selected_object': None
    }

if 'section8_stage' not in st.session_state:
    st.session_state['section8_stage'] = -1

if 'flavour_selected' not in st.session_state:
    st.session_state['flavour_selected'] = False


#================================================================================
#-- Initialise status of all pages
#---> Glue (only register so far, need for stages ?) TODO
if f'glue_stage1' not in st.session_state.keys():
    st.session_state[f'glue_stage1'] = -1
#---> Module
for iS in range(1,NSTAGE_MODULE+1):
    if f'module_stage{iS}' not in st.session_state.keys():
        st.session_state[f'module_stage{iS}'] = -1
#---> Cell
for iS in range(1,NSTAGE_CELL+1):
    if f'cell_stage{iS}' not in st.session_state.keys():
        st.session_state[f'cell_stage{iS}'] = -1
#---> LLS
for iS in range(1,NSTAGE_LLS+1):
    if f'lls_stage{iS}' not in st.session_state.keys():
        st.session_state[f'cell_stage{iS}'] = -1
#---> Upload to ProdDB
if f'upload_stage' not in st.session_state.keys():
    st.session_state[f'upload_stage'] = -1
#---> Pigtail
for iS in range(1,NSTAGE_PIGTAIL+1):
    if f'pigtail_stage{iS}' not in st.session_state.keys():
        st.session_state[f'pigtail_stage{iS}'] = -1
#---> Upload to LAPP_ProdDB
#   FIXME: use two different upload pages ?
if f'lapp_upload_stage' not in st.session_state.keys():
    st.session_state[f'lapp_upload_stage'] = -1


#-- Initialise whole app session states
if 'allmsg' not in st.session_state.keys():
    st.session_state.allmsg = list()
if 'testMode' not in st.session_state.keys():
    st.session_state.testMode = True
if 'refModule' not in st.session_state.keys():
    st.session_state.refModule = ''
if 'refCarrier' not in st.session_state.keys():
    st.session_state.refCarrier = ''
if 'refCell' not in st.session_state.keys():
    st.session_state.refCell = ''
if 'refPigtail' not in st.session_state.keys():
    st.session_state.refPigtail = '' 
if 'refLLS' not in st.session_state.keys():
    st.session_state.refLLS = ''
if 'refLoaded' not in st.session_state.keys():
    st.session_state.refLoaded = False
if 'currentObj' not in st.session_state.keys():
    st.session_state.currentObj = None

if 'nextStep' not in st.session_state.keys():
    st.session_state.nextStep = ''

if 'conditions_checked' not in st.session_state.keys():
    st.session_state.conditions_checked = False
if 'current_operator' not in st.session_state.keys():
    st.session_state.current_operator = None
if 'current_temp' not in st.session_state.keys():
    st.session_state.current_temp = 20
if 'current_hr' not in st.session_state.keys():
    st.session_state.current_hr = 50

st.title("👏 "+txt['homeTitle'])
st.subheader(txt['homeSubtitle'])

#-- Try connecting to YalDB (always needed)
if 'conn' not in st.session_state.keys():
    st.session_state['conn'] = None
if st.session_state.conn is None:
    with st.spinner(txt['connectingYal']):
        try:
            st.session_state.conn = connectionDB()
        except Exception as err:
            st.error(str(err))
            Menu()
            st.stop()

st.text(txt['connectionDoneYal'])

#-- Toggle to avoid constrains from format and ITk ProdDB
isTest = st.toggle("TEST",True)
st.session_state.testMode = isTest

#-- Try connecting to ITk ProdDB
if 'itkdbCli' not in st.session_state.keys():
    st.session_state['itkdbCli'] = None

if isTest:
    st.text(txt['noConnectionITk'])
else:
    if st.session_state.itkdbCli == None:
        code1=st.text_input("code 1:",type="password")
        code2=st.text_input("code 2:",type="password")
        if not st.button(txt['connectingITk']):
            Menu()
            st.stop()
        try:
            st.session_state.itkdbCli = AuthenticateUser(code1, code2)
        except Exception as err:
            st.error(str(err))
            Menu()
            st.stop()
        st.rerun()
    else:
        st.text(txt['connectionDoneITk'])

st.write("---")

#================================================================================
#--General methods for the home page

def CheckToBeLoaded(retVal, debug=False):

    """ Stolen and adapted from
    https://gitlab.cern.ch/jecouthu/itk-web-apps-pigtails/
    -/blob/JC-pigtails/commonCode/PDBQueries.py

    Result from ITk ProdDB query is used to check that the component is ready for loading:
        1. not already loaded = no parent OB_LOADED_MODULE_CELL
        2. ready to be loaded: state ready (FIXME: and stage = MODULERECEPTION)
    and returns the module if found.
    """

    isOk = True
    compType = retVal['componentType']['code']

    # Check in ready state #FIXME Needed ? Check at proper stage ?
    if debug: st.write("check state")
    try:
        if retVal['state'] != "ready":
            if debug: st.write("Component not ready:", retVal['state'])
            st.session_state.allmsg.append( [1,compType+txt['itkdbWrongState']] )
            isOk = False
    except TypeError:
        if debug: st.write("Problem with database return")
        st.session_state.allmsg.append( [1,txt['itkdbReturn']] )
        isOk = False

    # Check not already loaded
    try:
        if retVal["parents"] is None:
            used = []
        else:
            used = [c["component"]["serialNumber"] for c in retVal["parents"] if c["componentType"]["code"] == "OB_LOADED_MODULE_CELL"]
        if len(used)>0:
            if debug: st.write("Component already loaded:", used[0])
            st.session_state.allmsg.append( [1,compType+txt['itkdbLoaded']] )
            isOk=False
    except TypeError:
        if debug: st.write("Problem with database return")
        st.session_state.allmsg.append( [1,txt['itkdbReturn']] )
        isOk = False

    return isOk

def GetModuleInCarrier(retVal, debug=False):

    """ Stolen and adapted from
    https://gitlab.cern.ch/jecouthu/itk-web-apps-pigtails/
    -/blob/JC-pigtails/commonCode/PDBQueries.py

    Result from ITk ProdDB query is used to check that the carrier is ready to be used:
        1. State is ready (FIXME: check is final stage "MODULE/WIREBOND_PROTECTION" ??)
        2. containing a MODULE
    and returns the module if found.
    """

    # Check in ready state #FIXME Needed ? Check at proper stage ?
    if debug: st.write("check state")
    try:
        if retVal['state'] != "ready":
            if debug: st.write("Component not ready:", retVal['state'])
            st.session_state.allmsg.append( [1,"MODULE_CARRIER "+txt['itkdbWrongState']] )
            return None
    except TypeError:
        if debug: st.write("Problem with database return")
        st.session_state.allmsg.append( [1,txt['itkdbReturn']] )
        return None

    # Check contains a module
    try:
        if retVal["parents"] is None:
            return None
        else:
            used = [c["component"] for c in retVal["parents"] if c["componentType"]["code"] == "MODULE"]
        if len(used) == 0:
            if debug: st.write("No module found in this carrier")
            st.session_state.allmsg.append( [1,txt['itkdbCarrierEmpty']] )
            return None
    except TypeError:
        if debug: st.write("Problem with database return")
        st.session_state.allmsg.append( [1,txt['itkdbReturn']] )
        return None

    if debug: st.write("Found "+str(len(used))+" module(s) in this carrier.")
    if debug: st.write(used[0])

    return used[0]["serialNumber"]

def CheckComponent(sn, compType=None, curLoc=None, debug=False):

    """ Stolen and adapted from
    https://gitlab.cern.ch/jecouthu/itk-web-apps-pigtails/
    -/blob/JC-pigtails/commonCode/PDBQueries.py

    The ITk ProdDB is queried to check the Serial Number indeed corresponds to
    a component:
        1. that exists in ITk ProdDB
        2. corresponds to the expected component Type
        3. actually present in the laboratory.
    """

    isOk=True

    #-- Try getting the component from its SN
    if debug: st.write("getComponent")
    try:
        retVal=DbGet(st.session_state.itkdbCli,'getComponent', {'component':sn})
        if debug: st.write("found:",sn,"("+retVal['componentType']['code']+")")
    except Exception as err:
        if debug: st.write("No component found")
        st.session_state.allmsg.append( [1,sn+txt['itkdbNoComp']+"\n --> "+str(err)] )
        return None

    #-- Check type of component is as expected
    if debug: st.write("check type")
    try:
        if retVal['componentType']['code'] != compType:
            if debug: st.write("Unexpected component type for "+compType+":", retVal['componentType']['code'])
            st.session_state.allmsg.append( [1,sn+txt['itkdbWrongType']+compType+"."] )
            isOk=False
    except TypeError:
        if debug: st.write("Problem with database return")
        st.session_state.allmsg.append( [1,txt['itkdbReturn']] )
        isOk=False

    #-- Check at correct institution
    if debug: st.write("check inst.")
    try:
        if retVal['currentLocation']['code'] != curLoc:
            if debug: st.write("Component not "+curLoc+", it\'s:", retVal['currentLocation']['code'])
            st.session_state.allmsg.append( [1,compType+txt['itkdbWrongLoc']+curLoc] )
            isOk=False
    except TypeError:
        if debug: st.write("Problem with database return")
        st.session_state.allmsg.append( [1,txt['itkdbReturn']] )
        isOk=False

    if not isOk:
        return None
    return retVal

def CheckSN(obj):
    """ Check the format of the obj matches the ATLAS Serial Number
    specification.
    """
    try:
        obj = str(obj)
        assert obj != ""
        assert len(obj) > 0
    except AssertionError: #--obj is empty
        st.session_state.allmsg.append( [0,txt['loadErrorEmpty']] )
        return False
    except Exception as err: #--Error in formating of the obj ( str() for now )
        st.session_state.allmsg.append( [0,str(err)] )
        return False

    if (
       (len(obj) != 14)             #Serial number of 14 character
       or (obj[:3] != "20U")        #Standard ATLAS Upgrade prefix
       or (not obj[7:].isnumeric()) #Last 7 characters are numbers
       ):

        if not st.session_state.testMode:
            st.session_state.allmsg.append( [0,obj+" - "+txt['wrongSNformat']] )
            return False
        st.session_state.allmsg.append( [1,obj+" - "+txt['wrongSNformat']] )

    return True

def GetPairedModule(cell):

    conn = st.session_state.conn

    paired_module="""
        SELECT module_fk
          FROM Cells
         WHERE cell_id = %s
         """
    try:
        data = execute_read_query(conn, paired_module, (cell,) )
        assert len(data) == 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #By now, one and only one module should be in the DB
        st.session_state.allmsg.append( [0,"Not exactly one object found with id "+id] )
        return ''
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return ''

    return data[0][0]

def ShowCell():
    cell = st.session_state.inputRef
    cell = cell.upper()

    if not CheckSN(cell):
        return

    if not st.session_state.testMode:
        itkdbVal = CheckComponent(cell,"OB_BARE_MODULE_CELL",LAB_NAME, False)
        if itkdbVal == None:
            st.session_state.allmsg.append( [0,txt['notFoundSN']] )
            return False

        if not CheckToBeLoaded( itkdbVal ):
            st.session_state.allmsg.append( [0,txt['notReadySN']] )

    #-- Check Status of the Cell
    status_code = Cell_Status(cell)

    if status_code == 0: #An error occured, log in allmsg to be dumped
        return
    elif status_code == 1: #New cell
        st.session_state.allmsg.append( [2,txt['introCell']+str(cell)] )
    elif status_code == 2: #Known cell
        st.session_state.allmsg.append( [2,txt['loadCell']+str(cell)] )
    else: #Bug, shouldn't happen
        st.session_state.allmsg.append( [0,"Wrong status code from Cell_Status()"] )
        return

    st.session_state.allmsg.append( [2,txt['goSec'] + st.session_state.nextStep] )
    st.session_state.refLoaded = True
    st.session_state.refCell = cell

    if st.session_state['cell_stage2'] > 1:#Alignment step has been done
        st.session_state.refModule = GetPairedModule(cell)

    return
    
def ShowModule(module=None):

    if module == None:
        module = st.session_state.inputRef
    module = module.upper()

    if not CheckSN(module):
        return

    if not st.session_state.testMode:
        itkdbVal = CheckComponent(module,"MODULE",LAB_NAME, False)
        if itkdbVal == None:
            st.session_state.allmsg.append( [0,txt['notFoundSN']] )
            return False

        if not CheckToBeLoaded( itkdbVal ):
            st.session_state.allmsg.append( [0,txt['notReadySN']] )

    #-- Check Status of the Module
    status_code = Module_Status(module)

    if status_code == 0: #An error occured, log in allmsg to be dumped
        return
    elif status_code == 1: #New module
        st.session_state.allmsg.append( [2,txt['introModule']+str(module)] )
    elif status_code == 2: #Known module
        st.session_state.allmsg.append( [2,txt['loadModule']+str(module)] )
    else: #Bug, shouldn't happen
        st.session_state.allmsg.append( [0,"Wrong status code from Module_Status()"] )
        return

    st.session_state.allmsg.append( [2,txt['goSec'] + st.session_state.nextStep] )
    st.session_state.refLoaded = True
    st.session_state.refModule = module
    return

def ShowCarrier():
    carrier = st.session_state.inputRef
    carrier = carrier.upper()

    if not CheckSN(carrier):
        return

    if st.session_state.testMode:
        module = carrier[:3]+"MODU"+carrier[7:] if len(carrier) > 7 else carrier+"MODU"
    else:
        itkdbVal = CheckComponent(carrier,"MODULE_CARRIER",LAB_NAME, False)
        if itkdbVal == None:
            st.session_state.allmsg.append( [0,txt['notFoundSN']] )
            return False

        module = GetModuleInCarrier( itkdbVal, False )
        if module == None:
            st.session_state.allmsg.append( [0,txt['notReadySN']] )
            return

    st.session_state.allmsg.append( [2,txt['moduleInCarrier']+module] )

    ShowModule(module)
    st.session_state.refCarrier = carrier

    return

def ShowLLS():
    # FIXME: Placeholder for ShowLLS() method after SN registration
    st.session_state.refLoaded = True
    st.session_state['lls_stage1'] = 1;#--Registration
    st.session_state.allmsg.append( [2,txt['goSec']+txt['LLSmenu']] )
    st.session_state.allmsg.append( [1,"*Fonctionnalité à venir*"] )
    return

def ShowAll(tagObj):
    """
    Upon entering a reference in the inputRef field, will show the available
    sections.  This is done by calling the component specific ShowXXX function
    identified by tagObj given as input.  If the specific 'ShowXXX' function
    succeeds, a Reset button appears and the input reference is made non
    interactive.

    NB: if the input reference is reset, the function is not executed.
    """

    #-- Avoid triggering Show All if input ref firld is re-initialised
    if st.session_state.inputRef == '':
        return

    #-- Apply component type specific function
    if tagObj == "refModule":
        ShowModule()
    elif tagObj == "refCarrier":
        ShowCarrier()
    elif tagObj == "refCell":
        ShowCell()
    elif tagObj == "refLLS":
        ShowLLS()
    elif tagObj == "refPigtail":
        ShowPigtail()
    else: #Bug, shouldn't happen
        st.session_state.allmsg.append( [0,"Wrong tag in ShowAll!"] )

    return

def ResetAll():
    """
    Upon clicking on the reset button,
    - will close all the sections
    - reset the component reference and make it interactive again
    - remove the Reset button
    """

    #-- Re-initialise pages status
    #---> Glue (only register so far, need for stages ?) TODO
    st.session_state[f'glue_stage1'] = -1
    #---> Module
    for iS in range(1,NSTAGE_MODULE+1):
        st.session_state[f'module_stage{iS}'] = -1
    #---> Cell
    for iS in range(1,NSTAGE_CELL+1):
        st.session_state[f'cell_stage{iS}'] = -1
    #---> LLS
    for iS in range(1,NSTAGE_LLS+1):
        st.session_state[f'lls_stage{iS}'] = -1
    #---> Upload to ProdDB
    st.session_state[f'upload_stage'] = -1
    #---> Pigtail
    for iS in range(1,NSTAGE_PIGTAIL+1):
        st.session_state[f'pigtail_stage{iS}'] = -1
    #---> Upload to LAPP_ProdDB
    st.session_state[f'lapp_upload_stage'] = -1    


    #-- Re-initialise all references
    st.session_state.refModule = ''
    st.session_state.refCarrier = ''
    st.session_state.refCell = ''
    st.session_state.refLLS = ''
    st.session_state.inputRef  = ''
    st.session_state.refLoaded = False
    st.session_state.refPigtail = ''  

    #-- Save widget state
    st.session_state.currentObj = st.session_state.whatComponent

    st.session_state.allmsg.append( [2,txt['reset']] )
    return

def ShowPigtail():
    return

# This function decodes the image and returns the data - needs pylibdmtx
# def decode_image(img):
#     try:
#         decoded_objects = decode(img)

#         if not decoded_objects:
#             return None
#         else:
#             return decoded_objects[0].data.decode('utf-8')
#     except Exception as e:
#         st.error(f"An error occurred during decoding: {e}")
#         return None


#================================================================================
#-- Page display

#-- Recover widget current state
st.session_state.whatComponent = st.session_state.currentObj
whatComponent = st.radio(
        label = txt['whatComponent'],
        options = [
                    txt['loadGlue'],
                    txt['module'],
                    txt['carrier'],
                    txt['cell'],
                    txt['functLong'],
                    txt['functIHR'],
                    txt['pigtail'],                                                                            
                  ],
        captions = [
                     txt['loadGlueCpt'],
                     txt['moduleCpt'],
                     txt['carrierCpt'],
                     txt['cellCpt'],
                     txt['functLongCpt'],
                     txt['functIHRCpt'],                    
                     txt['pigtailCpt'],                                         
                   ],
        key="whatComponent",
        on_change = ResetAll,
)

if st.session_state.currentObj is None:
    Menu()
    st.stop()
elif st.session_state.currentObj == txt['loadGlue']:
    # FIXME: Placeholder, to be seen how loading of glue is managed
    st.session_state.refLoaded = True
    st.session_state['glue_stage1'] = 1;#--Registration
    st.session_state.allmsg.append([2, txt['goSec'] + txt['regGlue']])
    st.session_state.allmsg.append([1, "*Fonctionnalité à venir*"])
    #-- For the glue, likely no registration of SN foreseen: stopping the page here
    Menu()
    Print_out_messages()
    st.stop()
elif st.session_state.currentObj == txt['module']:
    tagObj = 'refModule'
elif st.session_state.currentObj == txt['carrier']:
    tagObj = 'refCarrier'
elif st.session_state.currentObj == txt['cell']:
    tagObj = 'refCell'
elif st.session_state.currentObj == txt['functLong']:
    tagObj = 'refLLS'
elif st.session_state.currentObj == txt['functIHR']:
    tagObj = 'refLLS'
elif st.session_state.currentObj == txt['pigtail']:
    tagObj = 'refPigtail'
    st.session_state.refLoaded = True
    
    with st.container(border=True):
        # Toggle switch for enabling or disabling the QR code scanner
        scan = st.toggle("SCAN QR CODE",False)
        if scan:
            #==========================================================
            # REAL TIME SCANNING METHOD
            #==========================================================
            st.title("Data Matrix Scanner")
            st.info("Ensure good lighting and move close to the data matrix")

            # Use the qrcode_scanner component to scan Data Matrix codes
            scanned_code = qrcode_scanner(key="qrcode_scanner",)
            if scanned_code:
                st.success(f"Scanned Code: {scanned_code}")  # Display the scanned code
                # Code to use scanned_code for DB or Flavour selection can be added here
            else:
                st.write("Scanning...")
            st.subheader('', divider='gray')

            # Manual entry field for Data Matrix code
            flavour_scan = st.text_input("Manual Entry", key="flavour", help="Enter the manual datamatrix", placeholder='Digits only')      
            submit_button = st.button("OK", key="ok")

            # Code to handle manual entry submission can be added here
            # if submit_button:
            #     st.session_state['informations']['selected_object'] = flavour_scan
            #     st.session_state.refLoaded = True
            #     st.session_state.refPigtail = flavour_scan
            #     st.session_state['pigtail_stage1'] = 1
            #     st.rerun()

            #================================================================================
            # ALTERNATIVE METHOD USING CAMERA PHOTO
            #================================================================================
            # st.title("Data Matrix Scanner")

            # # Use the tablet's camera to capture an image
            # img_file_buffer = st.camera_input("Take a picture")

            # if img_file_buffer is not None:
            #     # Read the image
            #     image = Image.open(img_file_buffer)
                    
            #     # Display the captured image
            #     # st.image(image, caption='Captured Image', use_column_width=True)
                    
            #     # Decode the Data Matrix
            #     result = decode_image(image)  # Use result for DB or Flavour selection
            #     st.write(result)
            #================================================================================
        else:
            # Display the dropdown menu and OK button, and disable them if a flavour has been selected
            flavour = st.selectbox(label="Choose your Flavour :", options=["Top", "Bot", "Front", "Back"], disabled=st.session_state['flavour_selected'])
            submit_button = st.button(label='OK', disabled=st.session_state['flavour_selected'])

            # If the OK button is pressed, update the session state to disable the elements
            if submit_button and not st.session_state['flavour_selected']:
                st.session_state['informations']['selected_object'] = flavour.lower()
                st.session_state['flavour_selected'] = True  # Disable the dropdown and OK button
                st.session_state.refLoaded = True
                st.session_state.pigtail_stage1 = 1  # Activate the LAPP Application Menu
                st.session_state.allmsg.append([2, txt['goSecLAPP'] + txt['titleLappMenu']])
                # st.rerun()  # Redirect to the appropriate page
                Menu()
                Print_out_messages()
                st.stop()


else:
    st.session_state.allmsg.append([1, "*Fonctionnalité à venir*"])
    Menu()
    Print_out_messages()
    st.stop()


if tagObj != 'refPigtail':
    #-- Recover widget current state
    st.session_state.inputRef = st.session_state[tagObj]
    inputRef = st.text_input(txt[tagObj],
                            help=txt['formatSN'],
                            disabled= (st.session_state[tagObj] != ''),
                            key="inputRef",
                            on_change=ShowAll,
                            kwargs = {'tagObj' : tagObj},
                            )

    if st.session_state.refLoaded :
        resetBtn = st.button( txt['resetBtn'], on_click=ResetAll )


Menu()
Print_out_messages()
