# -*-coding:utf-8 -*-
from generalAppConfig import *

def save_img_and_get_dict(img_buffer,tag='') -> str:#, title: str = Path(p).stem) -> str:
    """
    Saves the image in the $BASE_DIR_IMAGE directory and outputs the json
    string to be stored in the database.
    """

    if tag != '':
        tag = "_"+tag
    #-- Make a unique name for the image and save it #FIXME Hardcoded type 'png'
    img_name = hex(tm.time_ns())[2:]+tag+".png"
    img_name = BASE_DIR_IMAGES / img_name
    img_PIL = Image.open(img_buffer)
    img_PIL.save(img_name)

    try:
        p = Path(img_name)
        assert p.exists()
    except AssertionError as err:
        raise FileNotFoundError(txt['fileNotFound']+"\n --> "+str(err))
        return

    try:
        os.chmod(BASE_DIR_IMAGES / p.name, stat.S_IRUSR | stat.S_IWUSR | stat.S_IROTH)
    except Exception as err:
        raise Exception(txt['failedCopy']+"\n --> "+str(err))#FIXME
        return

    #---Prepare JSON format entry to the database
    #url = BASE_URL + "/" + p.name
    size = os.path.getsize(p)
    title = p.stem
    filetype = p.suffix
    mime=""
    #TODO hardcoded to png ??
    if filetype == '.jpeg' or filetype == '.jpg':
        mime = "image/jpeg"
    elif filetype == '.png':
        mime = "image/png"
    else:
        raise Exception(txt['wrongImageFormat']+" : "+str(filetype))
        return

    try:
        #img = [{"url": url,"path": str(img_name), "title": title, "mimetype": mime, "size": size}]
        img = [{"path": str(img_name), "title": title, "mimetype": mime, "size": size}]
        img = json.dumps(img)
    except Exception as err:
        raise Exception(txt['failedJSON']+"\n --> "+str(err))
        return

    return img

def connectionDB():
    """
    Connect through the global variable 'connectionString'.
    """
    connection = None
    try:
        connection = psycopg.connect(
            host     = connection_dict['host'],
            dbname   = connection_dict['dbname'],
            user     = connection_dict['user'],
            password = connection_dict['password'],
            port     = connection_dict['port'],
            )
    except OperationalError as err:
        raise OperationalError(txt['connectionFailedYal']+"\n --> "+str(err))
        return
    except KeyError as err:
        raise KeyError("Field missing in config file:"+str(err))
        return
    else:
        return connection

def execute_read_query(connection, query, args=None):
    """
    Execute any query that reads data from database.
    """
    result = None
    cursor = connection.cursor()
    try:
        cursor.execute(query, args)
        result = cursor.fetchall()
    except Exception as err:
        raise Exception(txt['queryFailed']+query+"\n -->"+str(err))
        return
    return result

def execute_query(connection, query, args=None):
    """
    Execute any query that modifies the database.
    """
    cursor = connection.cursor()
    try:
        cursor.execute(query, args)
    except Exception as err:
        connection.rollback()
        raise Exception(txt['queryFailed']+query+"\n --> "+str(err))
        return
    connection.commit()
    return

