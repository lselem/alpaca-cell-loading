# -*-coding:utf-8 -*-
"""
Stolen from https://gitlab.cern.ch/wraight/itkpdbtemplatemulti/
-/blob/master/core/DBaccess.py and adapted to this project
"""
from generalAppConfig import *
###
import itkdb.exceptions as itkX

def AuthenticateUser(ac1,ac2, useEos=False):
    user = itkdb.core.User(access_code1=ac1, access_code2=ac2)
    try:
        user.authenticate()
        client = itkdb.Client(user=user, use_eos=useEos)
        return client
    except itkX.ResponseException as err:
        iMessage  = str(err).find('"message": ')+len('"message": ')
        iNext = str(err)[iMessage:].find(',')
        err = '**'+str(err)[ iMessage : iMessage + iNext]+'**' 
        raise Exception(txt['connectionFailedITk']+"\n --> "+err)
        return None

@st.cache_data(show_spinner=True)
def DbGet(_client, myAction, inData, listFlag=False):
    outData=None
    if listFlag:
        try:
            outData = list(_client.get(myAction, json=inData ) )
        except itkX.BadRequest as err: # catch double registrations
            iMessage  = str(err).find('"message": ')+len('"message": ')
            iNext = str(err)[iMessage:].find(',')
            err = '**'+str(err)[ iMessage : iMessage + iNext]+'**' 
            raise Exception(err)
        #    st.write(myAction+": went wrong for "+str(inData))
        #    st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    else:
        try:
            outData =_client.get(myAction, json=inData)
        except itkX.BadRequest as err: # catch double registrations
            iMessage  = str(err).find('"message": ')+len('"message": ')
            iNext = str(err)[iMessage:].find(',')
            err = '**'+str(err)[ iMessage : iMessage + iNext]+'**' 
            raise Exception(err)
        #    st.write(myAction+": went wrong for "+str(inData))
        #    st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return outData

#@st.cache_data #(suppress_st_warning=True)
#def DbPost(client, myAction, inData):
#    outData=None
#    try:
#        outData=client.post(myAction, json=inData)
#    except itkX.BadRequest as b: # catch double registrations
#        st.write(myAction+": went wrong for "+str(inData))
#        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
#        try:
#            st.write('**'+str(b)[str(b).find('"paramMap": ')+len('"paramMap": '):-8]+'**') # sucks
#        except:
#            pass
#    except itkX.ServerError as b: # catch double registrations
#        st.write(myAction+": went wrong for "+str(inData))
#        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
#    return outData
#
#@st.cache_data
#def GetInstList(client):
#    myList=[]
#    try:
#        myList = list(client.get('listInstitutions'))
#    except itkX.BadRequest as b: # catch double registrations
#        st.write('listInstitutions'+": went wrong for "+str(inData))
#        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
#    return myList
#
#@st.cache_data
#def GetProjList(client):
#    myList=[]
#    try:
#        myList= list(client.get('listProjects'))
#    except itkX.BadRequest as b: # catch double registrations
#        st.write('listProjects'+": went wrong for "+str(inData))
#        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
#    return myList
