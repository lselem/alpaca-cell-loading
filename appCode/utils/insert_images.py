# import sys, os
# from app_utils import insert_image


###################### PAGE POSITION OF TOOLS ######################
# insert_image('position_of_tools', 'breadboard', 'position_breadboard', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/breadboard/position_breadboard.PNG')
# insert_image('position_of_tools', 'top', 'position_top_1', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/top/position_top_1.PNG')
# insert_image('position_of_tools', 'top', 'position_top_2', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/top/position_top_2.PNG')
# insert_image('position_of_tools', 'bot', 'position_bot_1', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/bot/position_bot_1.PNG')
# insert_image('position_of_tools', 'bot', 'position_bot_2', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/bot/position_bot_2.PNG')
# insert_image('position_of_tools', 'front', 'position_front_1', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/front/position_front_1.PNG')
# insert_image('position_of_tools', 'front', 'position_front_2', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/front/position_front_2.PNG')
# insert_image('position_of_tools', 'back', 'position_back_1', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/back/position_back_1.PNG')
# insert_image('position_of_tools', 'back', 'position_back_2', '/home/omeed/WebApp/alpaca-cell-loading-master/images/position_of_tools/back/position_back_2.PNG')

# print("Images Position of Tools Successful.")
#####################################################################



# ###################### ADJUSTEMENTS OF TOOLS ######################
# insert_image('adjustements_and_tools', 'zif_locking', 'adjustements_zif_locking', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_zif_locking.PNG')
# insert_image('adjustements_and_tools', 'strain_relief', 'adjustements_strain_relief', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_strain_relief.PNG')
# insert_image('adjustements_and_tools', 'zif_insertion_v1', 'adjustements_zif_insertion_v1', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_zif_insertion_v1.PNG')
# insert_image('adjustements_and_tools', 'zray_clamp', 'adjustements_zray_clamp', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_zray_clamp.PNG')
# insert_image('adjustements_and_tools', 'clm_insertion', 'adjustements_clm_insertion', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_clm_insertion.PNG')
# insert_image('adjustements_and_tools', 'clm_unmating', 'adjustements_clm_unmating', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_clm_unmating.PNG')
# insert_image('adjustements_and_tools', 'vacuum_system', 'adjustements_vacuum_system', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_vacuum_system.PNG')
# insert_image('adjustements_and_tools', 'zif_insertion_v2', 'adjustements_zif_insertion_v2', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_zif_insertion_v2.PNG')
# insert_image('adjustements_and_tools', 'carrier_box', 'adjustements_carrier_box', '/home/omeed/WebApp/alpaca-cell-loading-master/images/adjustements_and_tools/adjustements_carrier_box.PNG')

# print("Images Adjustements and Tools Successful.")
# #####################################################################


# ###################### PAGE MOUNTING ON CELLS ######################
# # Insérer des images pour le flavour TOP/BOTTOM - Page Mounting on Cells
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_1', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_1.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_2', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_2.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_3', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_3.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_4', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_4.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_5', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_5.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_6', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_6.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_7', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_7.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_8', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_8.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_9', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_9.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_10', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_10.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_11', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_11.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_12', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_12.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_13', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_13.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_14', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_14.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_15', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_15.PNG')
# insert_image('mounting_on_cells', 'top_bot', 'mounting_top_bot_16', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/top_bot/mounting_top_bot_16.PNG')

# print("Images Mounting TOP/BOTTOM Successful.")

# #####################################################################
# # Insérer des images pour le flavour FRONT - Page Mounting on Cells
# insert_image('mounting_on_cells', 'front', 'mounting_front_1', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_1.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_2', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_2.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_3', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_3.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_4', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_4.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_5', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_5.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_6', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_6.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_7', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_7.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_8', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_8.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_9', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_9.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_10', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_10.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_11', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_11.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_12', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_12.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_13', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_13.PNG')
# insert_image('mounting_on_cells', 'front', 'mounting_front_14', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/front/mounting_front_14.PNG')

# print("Images Mounting FRONT Successful.")

# #####################################################################
# # Insérer des images pour le flavour BACK - Page Mounting on Cells
# insert_image('mounting_on_cells', 'back', 'mounting_back_1', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_1.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_2', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_2.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_3', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_3.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_4', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_4.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_5', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_5.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_6', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_6.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_7', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_7.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_8', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_8.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_9', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_9.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_10', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_10.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_11', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_11.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_12', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_12.PNG')
# insert_image('mounting_on_cells', 'back', 'mounting_back_13', '/home/omeed/WebApp/alpaca-cell-loading-master/images/mounting_on_cells/back/mounting_back_13.PNG')

# print("Images Mounting BACK Successful.")