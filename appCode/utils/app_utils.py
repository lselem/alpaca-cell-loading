# -*-coding:utf-8 -*-
from generalAppConfig import *
from utils.YalDBaccess import *

from ruamel.yaml import YAML


def CheckFormat(operator, hr, temp):
    """
    Check operator in the LAB operator list and that
    Temperature and Humidity are of the correct format.
    """
    if operator == None:
        raise ValueError(txt['errSelOp'])

    if operator not in operator_list:
        raise ValueError(txt['operatorCheck'])

    #FIXME: Not necessary since constrained by streamlit widgets
    try:
        temp = float(temp)
        assert ( temp >= 0. and temp <= 50)
    except AssertionError:
        if temp < 0:
            raise ValueError(txt['tempLow'])
        else:
            raise ValueError(txt['tempHigh'])
    except ValueError as err:
        raise ValueError(txt['tempCheck']+"\n --> "+str(err))

    try:
        hr = float(hr)
        assert ( hr == 0. or (hr >= 1. and hr <= 100.))
    except AssertionError:
        raise ValueError(txt['hrBound'])
    except ValueError as err:
        raise ValueError(txt['hrCheck']+"\n --> "+str(err))

#================================================================================
def Print_out_messages():
    """
    Function to dump the messages session state 'allmsg' at once with a color
    depending on each message level. The session state entry is subsequently
    cleared.
    """
    if len(st.session_state.allmsg) > 0 :
        for msg in st.session_state.allmsg:
            if msg[0] == 0 : st.error(msg[1])
            if msg[0] == 1 : st.warning(msg[1])
            if msg[0] == 2 : st.info(msg[1])
            if msg[0] == 3 : st.success(msg[1])
    st.session_state.allmsg = list()

#================================================================================
#-- ENVIRONMENTAL CONDITIONS
def Update_Conditions(operator,hr,temp):
    """
    Call back function for the conditions in sidebar button. Will update the
    current environmental conditions in the session state (if everything is
    correctly filled).
    """
    try:
        CheckFormat(operator, hr, temp)
    except ValueError as err:
        st.sidebar.error(str(err))
        return

    for index_op, op in enumerate(operator_list):
        if op == operator:
            st.session_state.current_operator = index_op
    st.session_state.current_temp = temp
    st.session_state.current_hr = hr
    st.session_state.conditions_checked = True

def Validate_Conditions():
    """
    Call back function for the  validate conditions button. Will release the
    rest of the page if pressed. The same effect is alternatively obtained by
    changing the conditions in the sidebar. 
    """
    st.session_state.conditions_checked = True

def Conditions_in_sidebar(tag=''):
    """
    Will display in the sidebar input widgets for the environmental conditions
    needed in most step of the loading. At the beginning of the session it must
    be filled to access to the rest of the app. Otherwise it displays the
    latest environmental conditions loaded and offers the possibility to update
    them.
    """


    with st.sidebar:
        st.subheader(txt['condEnv'])
        #NOTE: The timestamp is automatically taken from the database clock
        Operator = st.selectbox(label=txt['operator'], options=operator_list,
                index=st.session_state.current_operator, placeholder="...",
                key="condOperator"+tag)
        HR   = st.slider(label=txt['humidity'],
                min_value=0, max_value=100, step=5,
                value=st.session_state.current_hr,
                key="condHR"+tag)
        Temp = st.slider(label=txt['temperature'],
                min_value=10, max_value=30,
                value=st.session_state.current_temp,
                key="condTemp"+tag)

        #-- Submit button label depends if it's the first time conditions are
        #   uploaded (operator still initialised to None) or not
        if st.session_state.current_operator != None:
            submitLabel = txt['updateCondBtn']
        else:
            submitLabel = txt['fillCondBtn']
        if st.button(submitLabel,
                  on_click = Update_Conditions,
                  kwargs = {
                            'operator' : Operator,
                            'hr' : HR,
                            'temp':Temp
                           }
                  ):
            st.success(txt['newCond'])

    if st.session_state.current_operator == None:
        #-- no conditions uploaded yet == operator still initialised to None
        st.warning(txt['warningFillCond'])
        st.stop()

    if not st.session_state.conditions_checked:
        st.info(txt['infoFillCond'])
        st.button(label=txt['valCondBtn'],
                  on_click = Validate_Conditions,
                  )
        st.stop()

def Insert_Conditions(com = None):
    """ To insert in the Conditions table the environmental conditions of a
    processed step. This piece of code is shared across the app. It returns the
    id of the entry in the table to be used as a foreign key elsewhere.  """

    conn = st.session_state.conn

    #-- Create new conditions for this step
    if com is None:
        load_conditions="""
            INSERT INTO Conditions (
                        temperature,
                        humidity,
                        operator
                        )
                 VALUES (%s,%s,%s)
              RETURNING cond_id;
            """
        load_tuple = (
                st.session_state.current_temp,
                st.session_state.current_hr,
                operator_list[st.session_state.current_operator],
                )
    else:
        load_conditions="""
            INSERT INTO Conditions (
                        temperature,
                        humidity,
                        operator,
                        comment
                        )
                 VALUES (%s,%s,%s,%s)
              RETURNING cond_id;
            """
        load_tuple = (
                st.session_state.current_temp,
                st.session_state.current_hr,
                operator_list[st.session_state.current_operator],
                com,
                )
    try:
        cond_id = execute_read_query(conn, load_conditions, load_tuple)
        assert len(cond_id) == 1
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )

    return cond_id[0][0]

#================================================================================
def Get_all_status( results ):
    results = list(results)

    ### Check validity of results and reformat
    if len(results) <= 1:
        raise ValueError("There should be more than one result boolean.")

    isFailed = False
    for k in range( len(results) ):
        #-- If object is failed, all subsequent results are set to False
        if isFailed:
            st.warning("Object failed !")
            results[k] = 0

        #-- Convert NULL boolean to 0.5 value
        if results[k] is None:
            results[k] = 0.5
        #-- If one false boolean found, object flagged as failed
        elif results[k] == False and not isFailed:
            isFailed = True

        #-- By now, the sequence 'results' must be decreasing.
        #   ---> else, wrong order of step: there is a bug
        if k == 0:
            continue
        if results[k] > results[k-1]:
            raise ValueError("The results should be strictly decreasing. \
                    Sequence broken for the module ")

    ### Find first step where result is None = To Do
    i_todo = 0
    while i_todo < len(results) and results[i_todo] == 1 :
        i_todo += 1

    return isFailed, i_todo

def Module_Status(id):
    ### Computation of all statuses:
    #   ---> This is the configurable part adapted to the different components
    #   and evolution of the process. Only isFailed and i_todo should be
    #   needed.

    #-- Changing stage ? Check again the environmental conditions !
    st.session_state.conditions_checked = False

    #-- Considering a Module, Cell sections closed
    for iS in range(1,NSTAGE_CELL+1):
        st.session_state[f'cell_stage{iS}'] = -1

    conn = st.session_state.conn
    find_object = """
            SELECT
                   result_reception,
                   result_inspection,
                   result_cutting
            FROM
                   Modules
            WHERE
                   module_id = %s
            """
    try:
        data = execute_read_query(conn, find_object, (id,) )
        assert len(data) <= 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #->Shouldn't be allowed by the DB anyway: Primary Key
        st.session_state.allmsg.append( [0,"More than one object found with id "+id] )
        return 0
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return 0

    #-- No module found, initial state
    if data == []:
        #-- [OPEN, CLOSED, CLOSED]
        st.session_state['module_stage1'] = 1;#--Reception
        st.session_state['module_stage2'] = 0;#--Inspection
        st.session_state['module_stage3'] = 0;#--Cutting
        st.session_state.nextStep = moduleSectionName[0]
        return 1

    #-- One module found, len(data) == 1
    try:
        isFailed, i_todo = Get_all_status( data[0] ) # i_todo: index of stage to be done
    except ValueError as err:
        st.session_state.allmsg.append( [0,str(err)+module] )
        return 0
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return 0

    if isFailed:
        status = [3,3,3] #[DONE, DONE, DONE] #TODO:treament of failed objects
    elif i_todo == 0:
        status = [1,0,0] #[OPEN, CLOSED, CLOSED]
    elif i_todo == 1:
        status = [3,1,0] #[DONE, OPEN, CLOSED]
    elif i_todo == 2:
        status = [3,2,1] #[DONE, UPDATABLE, OPEN]
    else:
        status = [3,3,3] #[DONE, DONE, DONE]

    st.session_state['module_stage1'] = status[0];#--Reception
    st.session_state['module_stage2'] = status[1];#--Inspection
    st.session_state['module_stage3'] = status[2];#--Cutting
    if i_todo == 3:
        st.session_state.nextStep = txt['homeCell']
    else:
        st.session_state.nextStep = moduleSectionName[i_todo]

    return 2

def Cell_Status(id):

    #-- Changing stage ? Check again the environmental conditions !
    st.session_state.conditions_checked = False

    #-- Considering a Cell, Module sections closed
    for iS in range(1,NSTAGE_MODULE+1):
        st.session_state[f'module_stage{iS}'] = -1

    conn = st.session_state.conn
    find_object = """
            SELECT
                   result_preparation,
                   result_alignment,
                   result_gluing,
                   result_press
            FROM
                   Cells
            WHERE
                   cell_id = %s
            """
    try:
        data = execute_read_query(conn, find_object, (id,) )
        assert len(data) <= 1 #ID should be unique: 0 or 1 object found
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return 0
    except AssertionError as err:
        #->Shouldn't be allowed by the DB anyway: Primary Key
        st.session_state.allmsg.append( [0,"More than one object found with id "+id] )
        return 0

    #-- No cell found, initial state
    if data == []:
        #-- [OPEN, CLOSED, CLOSED, CLOSED]
        st.session_state['cell_stage1'] = 1;#--Preparation
        st.session_state['cell_stage2'] = 0;#--Alignment
        st.session_state['cell_stage3'] = 0;#--Gluing
        st.session_state['cell_stage4'] = 0;#--Press
        st.session_state.nextStep = cellSectionName[0]
        return 1

    #-- One cell found, len(data) == 1
    try:
        isFailed, i_todo = Get_all_status( data[0] ) # i_todo: index of stage to be done
    except ValueError as err:
        st.session_state.allmsg.append( [0,str(err)+module] )
        return 0
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return 0

    if isFailed:
        status = [3,3,3,3] #[DONE, DONE, DONE, DONE] #TODO:treament of failed objects
    elif i_todo == 0:
        status = [1,0,0,0] #[OPEN, CLOSED, CLOSED, CLOSED]
    elif i_todo == 1:
        status = [3,1,0,0] #[DONE, OPEN, CLOSED, CLOSED]
    elif i_todo == 2:
        status = [3,3,1,0] #[DONE, DONE, OPEN, CLOSED]
    elif i_todo == 3:
        status = [3,3,3,1] #[DONE, DONE, DONE, OPEN]
    else:
        status = [3,3,3,3] #[DONE, DONE, DONE, DONE]

    st.session_state['cell_stage1'] = status[0];#--Preparation
    st.session_state['cell_stage2'] = status[1];#--Alignment
    st.session_state['cell_stage3'] = status[2];#--Gluing
    st.session_state['cell_stage4'] = status[3];#--Press
    if i_todo == 4:
        st.session_state.nextStep = txt['homeDone']
    else:
        st.session_state.nextStep = cellSectionName[i_todo]

    return 2


#================================================================================
def display_sidebar():
    """
    Display the sidebar with information and edit options based on the application stage.
    """
    if 'informations' not in st.session_state:
        # Initialize session state for storing user information
        st.session_state['informations'] = {
            'Name': '',
            'Temperature': '',
            'Humidity': '',
            'selected_object': None
        }

    st.sidebar.markdown("---")  # Add a separator
    st.sidebar.title("Informations")  # Sidebar title

    # Determine if the sidebar fields should be editable based on the application stage
    editable = st.session_state.get('pigtail_stage1', -1) > 1
    if editable:
        # Editable fields for operator information
        name = st.sidebar.text_input("Operator Name", value=st.session_state['informations']['Name'])
        temperature = st.sidebar.text_input("Temperature (°C)", value=st.session_state['informations']['Temperature'])
        humidity = st.sidebar.text_input("Humidity", value=st.session_state['informations']['Humidity'])

        if st.sidebar.button("Modify"):
            # Update session state with new information
            st.session_state['informations']['Name'] = name
            st.session_state['informations']['Temperature'] = temperature
            st.session_state['informations']['Humidity'] = humidity
    else:
        # Display stored information in a non-editable format
        st.sidebar.text("Operator name : " + (st.session_state['informations']['Name'] or ''))
        st.sidebar.text("Temperature (°C) : " + (st.session_state['informations']['Temperature'] or ''))
        st.sidebar.text("Humidity : " + (st.session_state['informations']['Humidity'] or ''))
    st.sidebar.markdown("---")  # Add another separator - for clarity

def load_db_config():
    """
    Load the database configuration from a YAML file.
    """
    config_path = os.getenv('DB_TEMPLATE_PATH', '/home/itk/itk/WebApp_Omeed/alpaca-cell-loading-master/config/DB_template.yaml')
    if not os.path.exists(config_path):
        raise ValueError(f"Configuration file not found at {config_path}")

    # Read the YAML configuration file
    with open(config_path, 'r') as file:
        yaml = YAML()
        config = yaml.load(file)
    return config 

def get_db_connection():
    """
    Establish and return a connection to the PostgreSQL database.
    """
    config = load_db_config()
    conn = psycopg.connect(
        dbname=config['dbname'],
        user=config['user'],
        password=config['password'],
        host=config['host'],
        port=config['port']
    )
    return conn 

def insert_image(page, object, image_name, image_path):
    """
    Insert an image into the database.
    """
    conn = get_db_connection()
    cursor = conn.cursor()

    # Read the image file in binary mode
    with open(image_path, 'rb') as file:
        binary_data = file.read()

    # SQL query to insert the image into the database
    query = """INSERT INTO images (page, object, image_name, image_data) VALUES (%s, %s, %s, %s)"""
    cursor.execute(query, (page, object, image_name, binary_data))

    # Commit the transaction and close the connection
    conn.commit()
    cursor.close()
    conn.close()

#-- FIXME: pigtail hardcoded: to be merged with other "step-by-step" processes
#   -- FIXME: Add check on the image actually existing ?
#   -- FIXME: file extension always PNG ?
def get_image_path(page, object, image_name):
    return f"{DIR_APP_FIG}/pigtail/{page}/{object}/{image_name}.PNG"

#-- FIXME: To be removed, fully replaced by path search
def get_image_from_db(page, object, image_name):
    """
    Retrieve an image from the database (display on page).
    """
    conn = get_db_connection()
    cursor = conn.cursor()

    # SQL query to fetch the image data
    query = """SELECT image_data FROM images WHERE page = %s AND object = %s AND image_name = %s"""
    cursor.execute(query, (page, object, image_name))
    result = cursor.fetchone()
    cursor.close()
    conn.close()

    if result:
        return result[0]  # Return binary data
    else:
        return None  # Return None if no result is found
