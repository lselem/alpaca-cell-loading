#! /bin/python3 

from generalAppConfig import *
import math

def parseLine(line):

    #-- Separate name and value
    delim = '='
    try:
        iDelim = line.index(delim)
        name = line[:iDelim]
        value = line[iDelim+1:]
    except ValueError:
        print("ERROR: No delimiter", delim, "found.")
        name = line
        value = float('nan')

    #-- Remove blank spaces around deliminter
    name = name.strip()
    value = value.strip()

    #-- Try converting to float or datetime
    if 'AAAA-MM-JJTHH:MM:SS' in name:
        try:
            value = datetime.fromisoformat(value)
        except Exception as err:
            print(f"WARNING: for '{name}' could not convert value {value} to a datetime as expected.")
            print(err)
            value = float('nan')
    elif 'HH:MM:SS' in name:
        try:
            value = time.fromisoformat(value)
        except Exception as err:
            print(f"WARNING: for '{name}' could not convert value {value} to a time as expected.")
            print(err)
            value = float('nan')
    else:
        try:
            value = float(value)
        except ValueError as err:
            print(f"WARNING: for '{name}' could not convert value {value} to a float as expected.")
            print(err)
            value = float('nan')

    #-- Extract unit from name if any
    if ('(' in name) and (')' in name):
        unit = name[name.index('(')+1:name.index(')')]
        name = name.replace("("+unit+")","")
        name = name.strip()
    else:
        print(f"WARNING: no unit found in '{name}'")
        unit = "1"

    return name, value, unit

def List_Log_Depot( logdate=None ):

    files=[f for f in os.listdir(DIR_LOG_FILES)] 

    #-- Keep files with format :
    #   yymmdd_hhmmss_Depot_Monitorings_nXX.log
    logFiles = list()
    for f in files:
        if 'Depot_Monitorings' not in f:
            continue
        try:
            tmstp = datetime.strptime(f[:13],'%y%m%d_%H%M%S')
            name = f[14:-4] #TODO: extract number ?
        except Exception as err:
            continue

        #--Keep only logs of day 'logdate'
        if False:#TODO
            continue
        logFiles.append( [f,tmstp,name] )

    return logFiles

def Extract_Parser_Result(fname):
    samp_path = f"{DIR_LOG_FILES}/{fname}"
    r_file = open(samp_path,'r')

    dictLog = dict()
    for line in r_file:
        #--'@' tags lines to be parsed
        if line[0] != '@': 
            continue
        #-- Remove starting '@' and trailing '\n'
        line = line[1:]
        line = line[:-1] if line[-1] == "\n" else line 
    
        name,value,unit = parseLine(line)
        dictLog[name] = value #NOTE the unit is discarded

    r_file.close()
    return dictLog

def Collect_Expected_Fields( fname ):

    dictLog = Extract_Parser_Result(fname)

    keyDict = {
        'ini_pressure'      : 'Initial Pressure',
        'mass_bare_sample'  : 'Bare Sample Weight',
        'mass_glued_sample' : 'Glued Sample Mass',
        'corr_slope'        : 'Slope',
        'corr_pressure'     : 'Corrected Pressure',
        'datetime_deposit'  : 'Time',
        'duration_sample'   : 'Sample time',
        'duration_deposit'  : 'Depot Time',
        }

    depData = dict()
    missingKeys = list()
    nanKeys = list()
    for appKey, logKey in keyDict.items():
        try:
            depData[appKey] = dictLog[logKey]
            del dictLog[logKey]

            if isinstance(depData[appKey],float):
                assert (not math.isnan(depData[appKey]))
        except KeyError as err:
            missingKeys.append(appKey)
        except AssertionError as err:
            nanKeys.append(appKey)

    #-- Fail if a key expected by the app is missing or is nan
    if missingKeys != []:
        st.session_state.allmsg.append( [0,txt['missKeys']+str(missingKeys)] )
        return
    if nanKeys != []:
        st.session_state.allmsg.append( [0,txt['nanKeys']+str(nanKeys)] )
        return

    #-- Report any unused tag information in the log file
    if dictLog != {}:
        st.session_state.allmsg.append( [1,txt['leftKeys']+str(list(dictLog.keys()))] )

    return depData


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
            "--file", type=str,
            help="Name of log file to be parsed in DIR_LOG_FILES.",
            required=True,
            )
    args = parser.parse_args()

    dictLog = Extract_Parser_Result(args.file)
    print("\n=====================Extracted dictionnary=====================")
    print(dictLog.items())
    print("\n=====================Log files found=====================")
    print(List_Log_Depot())
