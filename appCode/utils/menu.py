from generalAppConfig import *

def Component_menu():
    # Show a navigation menu for module stages
    st.sidebar.page_link(
            "appMain.py",
            label=txt['home'],
            icon="👏",
            )

    if st.session_state.currentObj == txt['loadGlue']:
        st.sidebar.page_link(
                "pages/Glue_1_Register.py",
                label=txt['regGlue'],
                #disabled= st.session_state['glue_stage1'] < 1,
                icon="📤",
                )
    elif ( st.session_state.currentObj == txt['module'] or
           st.session_state.currentObj == txt['carrier'] ):
        st.sidebar.page_link(
                "pages/Module_1_Reception.py",
                label=txt['reception'],
                disabled= st.session_state['module_stage1'] < 1,
                icon="📤",
                )
        st.sidebar.page_link(
                "pages/Module_2_Inspection.py",
                label=txt['inspection'],
                disabled= st.session_state['module_stage2'] < 1,
                icon="🔎",
                )
        st.sidebar.page_link(
                "pages/Module_3_FlexCutting.py",
                label=txt['cutting'],
                icon="✂️",
                disabled= st.session_state['module_stage3'] < 1,
                )

    elif st.session_state.currentObj == txt['cell']:
        st.sidebar.page_link(
                "pages/Cell_1_Preparation.py",
                label=txt['prepCell'],
                disabled= st.session_state['cell_stage1'] < 1,
                icon="🔎",
                )
        st.sidebar.page_link(
                "pages/Cell_2_Alignment.py",
                label=txt['align'],
                disabled= st.session_state['cell_stage2'] < 1,
                icon="↕️",
                )
        st.sidebar.page_link(
                "pages/Cell_3_Glue.py",
                label=txt['glue'],
                disabled= st.session_state['cell_stage3'] < 1,
                icon="🗜️",
                )
    elif ( st.session_state.currentObj == txt['functLong'] or
           st.session_state.currentObj == txt['functIHR'] ):
        st.sidebar.page_link(
                "pages/LLS_1.py",
                label=txt['LLSmenu'],
                disabled= st.session_state['lls_stage1'] < 1,
                icon="📤",
                )

    elif st.session_state.currentObj == txt['pigtail']:
        st.sidebar.page_link(
                "pages/Pigtail_1_LAPP_Menu.py",
                label=txt['titleLappMenu'],
                disabled= st.session_state['pigtail_stage1'] < 1,
                icon="📤",
                )
        st.sidebar.page_link(
                "pages/Pigtail_2_Position_of_Tools.py",
                label=txt['titlePositionning'],
                disabled= st.session_state['pigtail_stage2'] < 1,
                icon="🔎",
                )
        st.sidebar.page_link(
                "pages/Pigtail_3_Adjustements_and_Tools.py",
                label=txt['titleAdjustement'],
                disabled= st.session_state['pigtail_stage3'] < 1,
                icon="↕️",
                )
        st.sidebar.page_link(
                "pages/Pigtail_4_Mounting_Process.py",
                label=txt['titleMounting'],
                disabled= st.session_state['pigtail_stage4'] < 1,
                icon="🗜️",
                )        
    

    #-- FIXME: Is there really a need for 2 different upload pages to ITk PDB ?
    #     ---> Both pages are empty as of now
    if st.session_state.currentObj != txt['pigtail']:
        st.sidebar.page_link(
                "pages/99_ToProdDB.py",
                label=txt['toProdDB'],
                icon="📨",
                disabled= st.session_state['upload_stage'] < 1,
                )
    else :
        st.sidebar.page_link(
                "pages/Pigtail_5_LAPP_ProdDB.py",
                label=txt['toLAPPProdDB'],
                icon="📨",
                disabled= st.session_state['lapp_upload_stage'] < 1,
                )
        
# st.sidebar.page_link(
#         "pages/99_ToProdDB.py",
#         label=txt['toProdDB'],
#         icon="📨",
#         disabled= st.session_state['upload_stage'] < 1,
#         )    

def Empty_menu():
    # Show a navigation menu for unauthenticated users
    st.sidebar.page_link(
            "appMain.py",
            label=txt['home'],
            icon="👏",
            )


def Menu():
    # Determine if a component reference is loaded or not, then show the correct
    # navigation menu
    if 'refLoaded' not in st.session_state or not st.session_state.refLoaded :
        Empty_menu()
        return
    Component_menu()


def Menu_with_redirect():
    # Redirect users to the main page if not logged in, otherwise continue to
    # render the navigation menu
    if 'refLoaded' not in st.session_state or not st.session_state.refLoaded :
        st.switch_page("appMain.py")
    Menu()
