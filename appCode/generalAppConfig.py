# -*-coding:utf-8 -*-
import streamlit as st

import psycopg2 as psycopg
from psycopg2 import OperationalError

import itkdb
import itkdb.exceptions as itkX

from utils.yaml_tools import YAML
from pathlib import Path
import sys
import os
import io
import json
import urllib
import shutil
import stat

#import yaml

from PIL import Image

from datetime import datetime, time
import time as tm

import pandas as pd

from allText import dictFR, dictEN
from streamlit_qrcode_scanner import qrcode_scanner

#=====================================================================
# NEW CONFIG WITH DOCKER
#=====================================================================

#--Using Dockerfile config: env variables
LAB_NAME = os.getenv("LAB_NAME")
LANGUAGE = os.getenv("LANGUAGE")
lang = LANGUAGE.lower()

OPERATORS = str( os.getenv("OPERATORS") )
operator_list = []
operator_name = ""
for x in OPERATORS:
    if x == ':':
        operator_list.append(operator_name)
        operator_name = ""
    else:
        operator_name += x
if operator_name != "":
    operator_list.append(operator_name)

#--Hardcoded in Docker
connection_dict = dict()
connection_dict['host'] = "yaldb"
connection_dict['dbname'] = "yaldb"
connection_dict['port'] = "5432"
connection_dict['user'] = "postgres"
connection_dict['password'] = "secret"

BASE_DIR_IMAGES = Path("/app/images/")
DIR_LOG_FILES = Path("/app/logloading/")


#-- Loading text fields of the requested language
if lang in ["francais","français","fr","french"]:
    txt = dictFR
elif lang in ["english","en","anglais"]:
    txt = dictEN
else:
    print(f"ERROR: No text available for language {lang}.")
    sys.exit(1)

#-- Directory for figures to be displayed in the app
DIR_APP_FIG = Path.cwd() / "figures"

#-- Alignment steps organisation and text fields

NSTAGE_MODULE=3
NSTAGE_CELL=3
NSTAGE_LLS=1
NSTAGE_PIGTAIL = 4  


moduleSectionName = [
        txt['reception'],
        txt['inspection'],
        txt['cutting'],
        ]
cellSectionName = [
        txt['prepCell'],
        txt['align'],
        txt['glue'],
        txt['press'],
        ]
pigtailSectionName = [
        txt['lapp_menu'],
        txt['position_pigtail'],
        txt['adjustement_pigtail'],
        txt['mounting_pigtail'],
        ]

aliStepLabel = [list(),list(),list()]
aliStepLabel[0] = [
        txt['step1'],
        txt['step1.1'],
        txt['step1.2'],
        txt['step1.3'],
        txt['step1.4'],
        txt['step1.5'],
        txt['step1.6'],
        txt['step1.7'],
        txt['step1.8'],
        txt['step1.9'],
        ]
aliStepLabel[1] = [
        txt['step2'],
        txt['step2.1'],
        txt['step2.2'],
        txt['step2.3'],
        txt['step2.4'],
        txt['step2.5'],
        txt['step2.6'],
        txt['step2.7'],
        ]
aliStepLabel[2] = [
        txt['step3'],
        txt['step3.1'],
        txt['step3.2'],
        txt['step3.3'],
        txt['step3.4'],
        txt['step3.5'],
        txt['step3.6'],
        txt['step3.7'],
        txt['step3.8'],
        txt['step3.9'],
        ]

nAliStep = sum( [len(aliStepLabel[k])-1 for k in range(len(aliStepLabel)) ] )

if __name__ == "__main__":
    print(f"In loading lab {LAB_NAME}:\n")
    print("BASE_DIR_IMAGES = ",BASE_DIR_IMAGES)
    print("DIR_LOG_FILES = ",DIR_LOG_FILES)
    print("operator_list =\n    ",operator_list)
    print("Connection =\n    '",connection_dict,"'")
    print("-------------------------")
    print("Dictionary using language {lang.upper()}:\n")
    print("--->'txt' =\n    ",txt)
    print("-------------------------")
    print("Figures used in the app:")
    print([x for x in DIR_APP_FIG.iterdir()])


#=====================================================================
# OLD CONFIG WITHOUT DOCKER
#=====================================================================

# #-- Preparing the YAML config
# #config_file = "lab_config.yaml"
# config_file = "/home/itk/itk/WebApp_Omeed/alpaca-cell-loading-master/config/lab_config.yaml"
# lab_config = {}

# yaml = YAML(typ="safe", pure=True)
# with open(config_file) as f:
#     try:
#         lab_config = yaml.load(f.read())
#     except Exception as err:
#         print("A YAML exception occurred:\n", err)
#         sys.exit(1)
