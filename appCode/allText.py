# -*-coding:utf-8 -*-

"""All written text of the app is stored in the global dictionnary
all_dictionary. It is then reassigned in language specific dictionnary for ease
of access in the rest of the code."""

all_dictionary={

### HOME PAGE
"home"                   : ["Accueil",
                            "Home page" ],
"homeTitle"              : ["CLApp !",
                            "CLApp !"],
"homeSubtitle"           : ["Une Application de *Cell Loading*",
                            "A Cell Loading App"],

#-- Connection to Databases
"connectingYal"          : ["Connexion à YalDB...",
                            "Connecting to YalDB..."],
"connectionFailedYal"    : ["Échec de la connexion YalDB !",
                            "Failed to connect to YalDB!"],
"connectionDoneYal"      : ["Connecté à YalDB.",
                            "Connected to YalDB."],

"noConnectionITk"        : ["Pas de connexion à ITk ProdDB en mode *TEST*",
                            "No connection to ITk ProdDB in TEST mode"],
"connectingITk"          : ["Connexion à ITk ProdDB",
                            "Connect to ITk ProdDB"],
"connectionFailedITk"    : ["Échec de la connexion à ITk ProdDB !",
                            "Failed to connect to ITk ProdDB !"],
"connectionDoneITk"      : ["Connecté à ITk ProdDB.",
                            "Connected to ITk ProdDB."],

#-- Itkdb messages
"itkdbReturn"            : ["Problème dans la réponse de ITk ProdDB.",
                            "Problem with ITk ProdDB return."],

"itkdbNoComp"            : [": ce numéro de série n'est pas trouvé dans ITk ProdDB.",
                            ": no such serial number found in ITk ProdDB."],

"itkdbWrongType"         : [" ne correspond pas au type ",
                            " does not match type "],

"itkdbWrongLoc"          : [" n'est pas localisé dans le laboratoire ",
                            " is not found in the laboratory "],

"itkdbLoaded"            : [" a déjà été *loadé* à un OB_LOADED_CELL_MODULE.",
                            " has already been loaded to a OB_LOADED_CELL_MODULE."],

"itkdbWrongState"        : [" pas dans l'état 'READY'.",
                            " not in the state 'READY'."],

"itkdbCarrierEmpty"      : ["Ce *carrier* ne contient pas de MODULE",
                            "This carrier does not contain a MODULE."],

#-- Component Choice
"whatComponent"          : ["Sur quel composant travaille-t-on ?",
                            "On what component are we working?" ],

"loadGlue"               : ["Colle du *Loading*",
                            "Loading Adhesive" ],
"loadGlueCpt"            : ["à enregistrer...",
                            "to register..." ],
"module"                 : ["Module",
                            "Module" ],
"moduleCpt"              : ["à coller sur une cellule...",
                            "to be glued on a cell..." ],
"refModule"              : ["Numéro de série du Module",
                            "Module serial number"],
"carrier"                : ["Carrier",
                            "Carrier" ],
"carrierCpt"             : ["avec un module à coller...",
                            "with a module to be glued..." ],
"refCarrier"             : ["Numéro de série du Carrier",
                            "Carrier serial number"],
"cell"                   : ["Cellule",
                            "Cell" ],
"cellCpt"                : ["à *loader* avec un module...",
                            "to be loaded with a module..." ],
"refCell"                : ["Numéro de série de la Cellule",
                            "Cell serial number"],

"functLong"              : ["Longeron fonctionnel nu",
                            "Bare Functional Longeron" ],
"functLongCpt"           : ["pour y intégrer des cellules loadées...",
                            "to have loaded cells integrated..." ],
"functIHR"               : ["Demi-anneau incliné fonctionnel nu",
                            "Bare Functional Inclined Half-Ring" ],
"functIHRCpt"            : ["pour y intégrer des cellules loadées...",
                            "to have loaded cells integrated..." ],
"refLLS"                 : ["Numéro de série du Support Local",
                            "Bare Local Support serial number" ],

"stycast"                : ["Stycast",
                            "Stycast" ],
"stycastCpt"             : ["Enregistrer un nouveau pot.",
                            "Register a new pot." ],
"cata"                   : ["Catalyseur",
                            "Catalyseur" ],
"cataCpt"                : ["Enregistrer un nouveau flacon.",
                            "Register a new bottle." ],


"formatSN"               : ["Format 20UXXYY1234567, insensible à la casse",
                            "Format 20UXXYY1234567, not case sensitive"],
"wrongSNformat"          : ["Numéro de série pas au format 20UXXYY1234567 !",
                            "Serial number not of the format 20UXXYY1234567 !"],
"notFoundSN"             : ["Le numéro de série ne correspond pas à un composant \
                             du bon type au laboratoire !",
                            "The serial number does not match a laboratory component \
                             of the correct type !"],
"notReadySN"             : ["Le numéro de série ne correspond pas à un composant \
                             prêt à être collé !",
                            "The serial number does not match a component \
                             ready to be loaded !"],
"moduleInCarrier"        : ["Le Carrier contient *normalement* le Module ",
                            "The Carrier *should* contain Module "], 

"resetBtn"               : ["Remise à zéro",
                            "Reset" ],

"reset"                  : ["*Remise à zéro*",
                            "*Reset*" ],

"introModule"            : ["__Nouveau module :__ ",
                            "__New module:__ "],

"loadModule"             : ["__Chargement du module :__ ",
                            "__Loading module:__ "],

"introCell"              : ["__Nouvelle cellule :__ ",
                              "__New cell:__ "],

"loadCell"               : ["__Chargement de la cellule :__ ",
                            "__Loading cell:__ "],

"goSec"                  : ["__Passez à l'étape__ ",
                            "__Go to step__ "],

"reception"              : ["Réception du module",
                            "Module reception"],
"inspection"             : ["Inspection visuelle",
                            "Visual inspection"],
"cutting"                : ["Découpage du flex",
                            "Flex cutting"],
"prepCell"               : ["Préparation de la cellule",
                            "Cell preparation"],
"align"                  : ["Alignement Cellule-Module",
                            "Cell-Module alignement"],
"glue"                   : ["Dépôt de colle",
                            "Glue deposit"],
"press"                  : ["Pressage",
                            "Press"],

"regGlue"                : ["Enregistrement de la colle",
                            "Loading adhesive registration"],

"LLSmenu"                : ["Support Local",
                            "Local Support"],

"toProdDB"               : ["Charger ITk ProdDB",
                            "Load ITk ProdDB"],
### PAGES STAGES
"Init"                   : ["*Veuillez d'abord entrer une référence \
                             à la page d'accueil.*",
                            "*First fill a reference in the home page.*"],

"Closed"                 : ["*Veuillez d'abord compléter la page* ",
                            "*First complete page* "],

"Ongoing"                : ["*Remplissez cette page pour passer \
                             à l'étape suivante.*",
                            "*Fill this page to go \
                             to the next step.*"],

"Updatable"              : ["*Cette page a déjà été remplie pour cet objet. \
                             Vous pouvez toujours la mettre à jour.*",
                            "*This page has already been filled for this object. \
                             You can still update it.*"],

"Final"                  : ["*Cette page est définitivement remplie pour cet objet \
                             et n'est plus modifiable.*",
                            "*This page is definitively filled for this object \
                             and is not updatable.*"],

"homeCell"              : ["*d'accueil avec un référence de* __Cellule__.",
                            "*of the beginning with a reference for a* __Cell__."],

"homeDone"              : ["*d'accueil avec une nouvelle référence.",
                            "*of the beginning with a new reference."],
### ERROR MESSAGES
"loadErrorEmpty"         : ["Veuillez entrer la référence du module !",
                            "Fill the module reference!"],

"queryFailed"            : ["Échec de la requête : ",
                            "Failed query: "],

#--FIXME: Env Cond checks (not fully necessary)
"operatorCheck"          : ["L'opérateur n'est pas dans la liste !",
                            "The operator is not in the list!"],

"hrCheck"                : ["L'humidité n'est pas au bon format.",
                            "Humidity is in the wrong format."],

"hrBound"                : ["L'humidité doit être un pourcentage entre 0 et 100.",
                            "Humidity must be a percentage between 0 and 100."],

"tempCheck"              : ["La température n'est pas au bon format.",
                            "Temperature is in the wrong format."],

"tempLow"                : ["La température est trop basse ! Ça va geler !",
                            "The temperature is to low! It will freeze!"],

"tempHigh"               : ["La température est trop haute ! \
                             Satané réchauffement climatique !",
                            "The temperature is to high! Damned global warming!"],

#--Picture 
"photoSchemeErr"         : ["La photo n'est pas censée être accédée via une adresse web.",
                            "The photo should not be accessed through a web address."],

"fileNotFound"           : ["La photo n'a pas été trouvée sur le serveur.",
                            "The photo could not be found on the server."],

"failedCopy"             : ["Échec de la copie de la photo dans le répertoire d'images.",
                            "The copy to the picture directory failed."],

"failedJSON"             : ["Échec du formatage en JSON.",
                            "Failed JSON formatting."],

"wrongImageFormat"       : ["La photo chargée n'est pas au format `png` ou `jpg`.",
                            "The loaded photo is not in the `png` or `jpg` format."],

### EXPERIMENTAL CONDITIONS
"condEnv"                : ["Conditions environnementales",
                            "Environmental conditions"],

"errSelOp"               : ["Veuillez sélectionner un opérateur dans le menu déroulant.",
                            "Please select an operator in the drop-down menu."],

"warningFillCond"        : ["Aucune condition environnementale n'est définie, \
                             veuillez les renseigner dans la barre latérale.",
                            "No environmental conditions are defined, \
                             please fill them in the sidebar."],

"infoFillCond"           : ["Validez les conditions environnementales actuelles ou \
                             mettez les à jour dans la barre latérale si nécessaire.",
                            "Validate the current environmental conditions or \
                             update them in the sidebar if necessary."],

"newCond"                : ["Nouvelles conditions environnementales enregistrées.",
                            "New environmental conditions loaded"],

"valCondBtn"             : ["Valider les conditions",
                            "Validate conditions"],

"fillCondBtn"            : ["Remplir les conditions",
                            "Fill conditions"],

"updateCondBtn"          : ["Màj des conditions",
                            "Update conditions"],

#--Fields in form
"noCom"                  : ["Sans commentaire",
                            "No Comment"],
"com"                    : ["Commentaires",
                            "Comments"],

"operator"               : ["Opérateur",
                            "Operator"],
"humidity"               : ["Humidité [%]",
                            "Humidity [%]"],
"temperature"            : ["Température [°C]",
                            "Temperature [°C]"],
"date"                   : ["Date",
                            "Date"],

#-- General button
"register"               : ["Enregistrement",
                            "Register"],

### TEXT IN SECTION 1
"section1"               : ["📤 RÉCEPTION DU MODULE",
                            "📤 MODULE RECEPTION"],

"logNoCarrier"           : ["Aucune référence de *carrier* n'est entrée !",
                            "No carrier reference is entered!"],

"photoModule"            : ["Photo du module",
                            "Module photo"],

"stockLog"               : ["Stockage dans l'armoire sèche",
                            "Stored in the dry cabinet"],

#NOTE : to be used
"logUpdate"              : ["Mise à jour de la table 'Modules' pour le module ",
                            "Updating table 'Modules' for module "],

"logAdd"                 : ["Ajout dans la table 'Modules' du module ",
                            "Adding in table 'Modules' the module "],

### TEXT IN SECTION 2
"section2"               : ["🔎 INSPECTION VISUELLE",
                            "🔎 VISUAL INSPECTION"],

"photoDefect"            : ["Photo défaut",
                            "Defect Photo"],

"showImage"              : ["Afficher les photos",
                            "Show photos"],

"noDefect"               : ["Pas de défaut trouvé ?",
                            "No defect found?"],

"validate"               : ["Valider l'inspection sans défauts",
                            "Validate inspection with no defects"],

"logNoDefect"            : ["Étape d'inspection visuelle passée sans défaut rapporté !",
                            "Visual inspection passed with no defect reported !"],

"logLoadImage"           : ["Chargement de la photo dans la table 'VisualDefects' \
                             pour le module ",
                            "Loading of the picture in the 'VisualDefects' table \
                            for module "],

"logNoImage"             : ["Aucune image n'est chargée !",
                            "No image is loaded!"],

"logModuleNoDefect"      : ["Il n'y a aucune photo de défaut pour le module ",
                            "There are no defect pictures for module "],

"logAfficheImage"        : ["Nombre de photos de défaut : ",
                            "Number of defect pictures: "],

"subsection1_show"       : ["Défauts déjà enregistrés",
                            "Defects already registered"],

"subsection2_show"       : ["Enregistrer un nouveau défaut",
                            "Register a new defect"],

### TEXT IN SECTION 3
"section3"               : ["✂️ DÉCOUPAGE DU FLEX",
                            "✂️ FLEX CUTTING"],

"validateCut"            : ["Valider le découpage du flex",
                            "Validate the flex cutting"],

"logCuttingDone"         : ["Succès du découpage du flex enregistré.",
                            "Successful flex cutting registered."],

"resultCut"              : ["Résultat du découpage",
                            "Cutting result"],


### TEXT IN SECTION 4
"section4"               : ["🔎 PRÉPARATION CELLULE",
                            "🔎 CELL PREPARATION"],

"massCell"               : ["Mass Cellule [g]",
                            "Cell Mass [g]"],

"photoFront"             : ["Photo cellule face avant",
                            "Cell front side photo"],

"photoBack"              : ["Photo cellule face arrière",
                            "Cell back side photo"],

#NOTE : to be used
"logUpdateCell"          : ["Mise à jour dans la table 'Cells' de la cellule ",
                            "Updating in table 'Cells' the cell "],

"logAddCell"             : ["Ajout dans la table 'Cells' de la cellule ",
                            "Adding in table 'Cells' the cell "],

### TEXT IN SECTION 5
"section5"               : ["↕️ ALIGNEMENT CELLULE-MODULE",
                            "↕️ CELL-MODULE ALIGNMENT"],

"pairedModule"           : ["Référence du Module à appairer",
                            "Reference of the Module to be paired"],

"fillPairedModule"       : ["Veuillez entrer la référence du Module à appairer.",
                            "Please enter the reference of the Module to be paired."],

"logModuleNotFound"      : ["La table 'Modules' ne contient pas ce module.",
                            "The table 'Modules' does not contain this module."],

"logModuleFailed"        : ["Ce module a échoué et ne peut être appairé.",
                            "This module has failed and cannot be paired."],

"logModuleNotReady"      : ["Ce module n'est pas prêt à être appairé.",
                            "This module is not ready to be paired."],

"logPairingReady"        : ["Ce module est prêt à être appairé.",
                            "This module is ready to be paired."],

"alignProcess"           : ["__Étapes de la procédure d'alignement__",
                            "__Steps of the alignment procedure__"],

"prevStep"               : ["⬅️ __Précédent__",
                            "⬅️ __Previous__"],
"nextStep"               : ["__Suivant__ ➡️",
                            "__Next__ ➡️"],
#--STEP 1
"step1"                  : ["Alignement initial du module",
                            "Initial module alignment"],

"step1.1"                : ["Avec le stylo aspirant, installer le module sur le \
                             bloc à vide (flèche rouge) ; **Le mettre en contact avec les 2 points, puis le dernier point \
                             de contact** (en orange).",
                            "With the vacuum pen, install the module on the vacuum block (red arrow); \
                             **Put it in contact with the 2 points, then the last point** (in orange)."],
"step1.2"                : ["**Tenir le module avec le stylo aspirant** et activer \
                             l'aspiration du bloc (flèche bleue).",
                            "**Hold the module with the vacuum pen** and switch on the \
                             suction of the vacuum block (blue arrow)."],

"step1.3"                : ["Placer le *Bridge* sur les colonnes ; **serrer la gauche \
                             puis la droite**",
                            "Place the Bridge on the columns; **tighten the left then \
                             the right**."],

"step1.4"                : ["Aligner les lignes sur la vis.",
                            "Align the lines on the screw."],

"step1.5"                : ["Relâcher l'articulation sphérique du *Bridge*.",
                            "Release the spherical joint of the Bridge"],

"step1.6"                : ["Monter lentement la plate-forme de levage tout en maintenant la \
                             tête d'aspiration avec la main lorsque le module la touche.",
                            "Slowly raise the lifting platform, holding the vacuum head \
                             with the hand as the Module touches it."],

"step1.7"                : ["S'arrêter à une force de contact de 1000 g ; bloquer \
                             l'articulation sphérique.",
                            "Stop at a contact force of 1000 g; lock the spherical \
                             joint."],

"step1.8"                : ["Activer l'aspiration de la tête du *Bridge* et désactiver \
                             l'aspiration du bloc.",
                            "Switch on the suction of the Bridge head and switch off \
                             the suction of the vacuum block."],

"step1.9"                : ["Descendre la plate-forme jusqu'à sa position la plus basse ; \
                             retirer le *Bridge* avec le module et le placer dans la \
                             zone de stockage",
                            "Lower the platform to the lowest position; remove the \
                             Bridge with the module and place it in the storage area."],

#--STEP 2
"step2"                  : ["Réglage du parallélisme entre le module et la cellule nue",
                            "Adjustment of parallelism between module and bare cell"],

"step2.1"                : ["Installer la cellule nue sur le bloc à vide à l'aide de \
                             l'axe à ressort ; faire attention au sens : **la broche \
                             de référence et la broche secondaire du bloc aspirant \
                             doivent être positionnées par une surface en \"V\" et \
                             une surface \"plate\" respectivement.**",
                            "Install the bare cell on the vacuum block with the \
                             spring-loaded axis; Pay attention to the orientation: \
                             **The reference and secondary pins of the vacuum block must \
                             be positioned by a \"V\" and a \"flat\" surface \
                             respectively.**"],

"step2.2"                : ["Activer l'aspiration du bloc aspirant.",
                            "Switch on the suction of the vacuum block."],

"step2.3"                : ["Replacer sur les colonnes le *Bridge* avec le module ; \
                             **serrer la gauche puis la droite** ; desserrer légèrement \
                             (30° de tour) l'articulation sphérique ; \
                             aligner les lignes sur la vis.",
                            "Put back on the columns the Bridge with the module; \
                             **tighten the left then the right**; slightly loosen \
                             (30° of a turn) the spherical joint; \
                             align the lines on the screw"],

"step2.4"                : ["Monter lentement la plate-forme de levage jusqu'à ce que \
                             le module touche les ventouses ; tenir la tête d'aspiration \
                             avec la main lorsque le module la touche ; \
                             s'arrêter à une force de contact de 1000 g.",
                            "Slowly raise the lifting platform until the module touches \
                             the suction cups; hold the vacuum head with the hand when \
                             the module touches it; stop at a contact force of 1000 g."],

"step2.5"                : ["**Verrouiller délicatement l'articulation sphérique** ; \
                             ne pas toucher !",
                            "**Gently lock the spherical joint**; Do not touch!"],

"step2.6"                : ["Descendre la plate-forme jusqu'à sa position la plus basse ; \
                             retirer le *Bridge* avec le module et le placer dans la \
                             zone de stockage",
                            "Lower the platform to the lowest position; remove the \
                             Bridge with the module and place it in the storage area."],

"step2.7"                : ["Désactiver l'aspiration du bloc aspirant ; retirer \
                             la cellule nue.",
                            "Switch off the suction of the vacuum block; Remove the \
                             bare cell"],

#--STEP 3
"step3"                  : ["Réalignement du module en XY",
                            "Module realignment in XY"],

"step3.1"                : ["Replacer sur les colonnes le *Bridge* avec le module ; \
                             **serrer la gauche puis la droite** ; \
                             aligner les lignes sur la vis.",
                            "Put back on the columns the Bridge with the module; \
                             **tighten the left then the right**; \
                             align the lines on the screw"],

"step3.2"                : ["Monter lentement la plate-forme de levage jusqu'à ce que \
                             le bloc aspirant touche le module ; force de contact = ...N.",
                            "Slowly raise the lifting platform until the vacuum block \
                             touches the module; contact force = ... N."],

"step3.3"                : ["Désactiver l'aspiration de la tête du *Bridge*.",
                            "Switch off the suction of the Bridge head."],

"step3.4"                : ["Descendre la plate-forme jusqu'à sa position la plus basse ; \
                             retirer le *Bridge* avec le module et le placer dans la \
                             zone de stockage",
                            "Lower the platform to the lowest position; remove the \
                             Bridge with the module and place it in the storage area."],

"step3.5"                : ["Réaligner le module sur ses 3 contacts cylindriques \
                             à l'aide du stylo aspirant ; **maintenir le module avec le \
                             stylo aspirant** et activer l'aspiration du bloc.",
                            "Re-align the module on its 3 cylindrical contacts with the \
                             vacuum pen; **hold the module with the vacuum pen** and \
                             switch on the suction of the vacuum block"],

"step3.6"                : ["Replacer sur les colonnes le *Bridge* ; \
                             **serrer la gauche puis la droite** ; \
                             aligner les lignes sur la vis.",
                            "Put back on the columns the Bridge; \
                             **tighten the left then the right**; \
                             align the lines on the screw"],

"step3.7"                : ["Monter lentement la plate-forme de levage jusqu'à ce que \
                             le module touche les ventouses ; \
                             S'arrêter à une force de contact de 1000 g.",
                            "Slowly raise the lifting platform until the module touches \
                             the suction cups; Stop at a contact force of 1000 g."],

"step3.8"                : ["Activer l'aspiration de la tête du *Bridge* et désactiver \
                             l'aspiration du bloc.",
                            "Switch on the suction of the Bridge head and switch off \
                             the suction of the vacuum block."],

"step3.9"                : ["Descendre la plate-forme jusqu'à sa position la plus basse ; \
                             retirer le *Bridge* avec le module et le placer dans la \
                             zone de stockage",
                            "Lower the platform to the lowest position; remove the \
                             Bridge with the module and place it in the storage area."],
#--STEPS DONE 

"photoProc"              : ["Photo procédure",
                            "Process picture"],

"alignDone"              : ["Alignement Cellule-Module réalisé avec succès ?",
                            "Cell-Module alignment done sucessfully?"],

"logAlignmentDone"       : ["Alignement Cellule-Module chargé dans la DB.",
                            "Cell-Module alignment loaded in the DB."],

### TEXT IN SECTION 6
"section6"               : ["🗜️ DÉPÔT DE COLLE",
                            "🗜️ GLUE DEPOSIT"],

"rSty"                   : ["Ref. Stycast",
                            "Ref. Stycast"],

"rCat"                   : ["Ref. Cataylseur",
                            "Ref. Catalyst"],

"noSty"                  : ["Aucun flacon de Stycast non périmé n'est disponible.",
                            "No unexpired Stycast bottle is available."],

"noCat"                  : ["Aucun flacon de Catalyseur non périmé n'est disponible.",
                            "No unexpired Catalyseur bottle is available."],

"verifP"                 : ["Avant toute opération, vérifier que le pression du dispenseur est à 1,1 bar",
                            "Before any operation, check that the dispenser pressure is at 1.1 bar."],

"gluePrep"               : ["Préparation du mélange de colle (Stycast+Catalyseur)",
                            "Preparation of the glue mix (Stycast+Catalyst)"],

"mesMSty"                : ["Masse du composant Stycast __[g]__",
                            "Mass of the Stycast component __[g]__"],

"theoMCat"               : ["Masse théorique du catalyseur à ajouter",
                            "Theoretical mass of catalyst to be added"],

"theoMGlue"              : ["Masse théorique du mélange Stycast+Catalyseur",
                            "Theoretical mass of the mix Stycast+Catalyst"],

"mesMGlue"               : ["Masse mesurée du mélange  \nStycast+Catalyseur __[g]__",
                            "Measured mass of the mix  \nStycast+Catalyst __[g]__"],

"errMGlue"               : ["Erreur relative sur la  \nmasse du mélange de colle",
                            "Relative error on  \nglue mix mass"],

"glueDep"                : ["Données du dépôt de colle",
                            "Glue deposit data"],

"loadMeth"               : ["Méthode :",
                            "Method:"],

"useLogFile"             : ["Charger un fichier de log ?",
                            "Load a log file?"],

"useManualInput"         : ["Entrer les données à la main ?",
                            "Enter manually the data?"],

"iniPr"                  : ["Pression initiale __[bar]__",
                            "Initial pressure __[bar]__"],

"verreM"                 : ["Masse de la lame de verre nue __[g]__",
                            "Mass of the bare glass slide __[g]__"],

"vInstr"                 : ["Maintenir à 0 si un tarrage est fait sur la lame de verre",
                            "Keep at 0 if the glass slide is tared"],

"tpsAtt"                 : ["Temps d'attente avant le lancement du programme de collage",
                            "Waiting time before starting the gluing program"],

"tpsSamp"                : ["Durée du dépôt sur l'échantillon __[sec]__",
                            "Duration of the deposit on the sample __[sec]__"],

"verreGlue"              : ["Masse de l'échantillon (lame de verre + colle) __[g]__",
                            "Sample mass (glass slide + glue) __[g]__"],

"corrPr"                 : ["Pression corrigée  \nà régler sur le dispenseur",
                            "Corrected pressure  \nto be set on the dispenser"],

"corrSl"                 : ["Pente de la  \ncourbe de correction",
                            "Slope of the  \ncorrection curve"],

"tpsDep"                 : ["Durée du dépôt sur la cellule __[sec]__",
                            "Duration of the deposit on the cell __[sec]__"],

"cellGlue"               : ["Masse Cellule+Colle __[g]__",
                            "Cell+Glue mass __[g]__"],

"depGlue"                : ["Masse de colle  \ndéposée sur la cellule",
                            "Mass of the glue  \ndeposited on the cell"],

"errDepGlue"             : ["Erreur relative en masse  \ndu dépôt de colle",
                            "Relative error on the mass  \nof the glue deposit"],

"logAddDep"              : ["Ajout dans la table 'GlueDeposit' du dépôt de colle.",
                            "Adding in table 'GlueDeposit' the glue deposit."],

"logTryAgain"            : ["Un nouveau dépôt de colle peut être réalisé sur \
                             cette cellule",
                            "A new glue deposit can be made on this cell."],

"logGluingDone"          : ["Succès du dépôt de colle enregistré dans la table 'Cells'.",
                            "Success of the glue deposit registered in table 'Cells'."],

#FIXME
"logAddDep2"             : [" pour la cellule ",
                            " for the cell "],

"missKeys"               : ["Les clés suivantes n'ont pas été trouvées \
                             dans le fichier de log :\n",
                            "The following keys have not been found in the log file:\n"],

"nanKeys"                : ["Les clés suivantes n'ont pas pu être converties \
                             au bon format :\n",
                            "The following keys could not be converted to the \
                             correct format:\n"],

"leftKeys"               : ["Les clés suivantes ont été extraites du fichier de log \
                             mais ne sont pas attendues par la DB :\n",
                            "The following keys have been extracted from the log file \
                             but are not expected by the DB:\n"],

"noLogFileDep"           : ["Aucun fichier de log de dépôt de colle trouvé.",
                            "No glue deposit log file found."],

"chooseLogF"             : ["Fichiers de log :",
                            "Log files:"],

"depSuccess"             : ["Le dépôt de colle à réussi ?",
                            "The glue deposit was successful?"],

"cleanCell"              : ["Nettoyez la cellule afin de la préparer à nouveau \
                             dépôt de colle.",
                            "Clean the cell to prepare it for a new glue deposit."],

"reuseCell"              : ["La cellule peut être réutilisée ?",
                            "Can the cell be reused?"],

"nbDep"                  : ["Nombre de dépôts de colle  \npour cette cellule",
                            "Number of glue deposits  \nfor this cell"],

### TEXT IN SECTION Register Glue
"secRegGlue"             : ["📨 ENREGISTREMENT DE LA COLLE",
                            "📨 LOADING GLUE REGISTRATION"],

"goRegGlue"              : ["Veuillez enregistrer des composants de la colle dans la page suivante.",
                            "📨 LOADING GLUE REGISTRATION"],
### TEXT IN SECTION LLS
"secLLS"                 : ["SUPPORT LOCAL CHARGÉ",
                            "LOADED LOCAL SUPPORT"],

### TEXT IN SECTION Print Database
"uploadStage"            : ["📨 Envoyer vers la `ProdDB`",
                            "📨 Send to `ProdDB`"],





"pigtail"                : ["Pigtail",
                            "Pigtail" ],
"pigtailCpt"             : ["à monter sur une cellule...",
                            "to be mounted on a cell..." ],
"refPigtail"             : ["Numéro de série du Pigtail",
                            "Pigtail serial number"],
"togPigtail"             : ["QR CODE du Pigtail",
                            "Pigtail QR CODE"],                            
                        
"introPigtail"           : ["__Nouveau pigtail :__ ",
                            "__New pigtail:__ "],

"loadPigtail"            : ["__Chargement du pigtail :__ ",
                            "__Loading pigtail:__ "],

"lapp_menu"              : ["Menu LAPP",
                            "LAPP Menu"],                            
"position_pigtail"       : ["Position des outils",
                            "Position of tools"],                            
"adjustement_pigtail"    : ["Ajustement des outils",
                            "Adjustement of tools"],                            
"mounting_pigtail"       : ["Montage des pigtails",
                            "Mounting the pigtails"],                            

"titleLappMenu"          : ["LAPP Application Menu"],
"titlePositionning"      : ["Positionning of tools"],
"titleAdjustement"       : ["Adjustement and tools"],
"titleMounting"          : ["Mounting on Cell"],                   


"NoticeTools"            : ["Please follow the instructions for tool positioning :"],
"NoticeAdjustement"      : ["**Please select an option.**\
                            \n(Steps are mandatory the first time you use the tool)"],
"section8"               : ["`LAPP` Application Menu"],
"NameLAPP"               : ["Operator Name"],
"TemperatureLAPP"        : ["Temperature"],
"HumidityLAPP"           : ["Humidity"],

"FlavourLAPP"            : ["**Choose your Flavour :**"],
"OkLAPP"                 : ["OK DONE"],
"SelectedLAPP"           : ["Selected"],

"LappMenu"               : ["LAPP Application Menu"],

"toLAPPProdDB"           : ["Send to LAPP ProdDB"],

"goSecLAPP"              : ["__Go to step__ "],

"selectFlavour"          : ["Select a flavour"],


#---------------------------------------------------------------------------------------#
### TEXT IN SECTION 9 - POSITION OF TOOLS
"section9"               : ["POSITION OF TOOLS"],

# Slide 4
"coordBreadboard"        : ["Coordinates on breadboards:Locations for the parts are indicated in **x** and **y** from [1;1] to [8;10].\
                            Other tools on optical poles locations will be adjusted with memory forks or painted mark.\
                            **The set-ups are presented with the tilting option for interposer use.\
                            They are presented as seen by the operator when pigtail is inserted.**\
                            **The following processes only describe the mechanical mounting, so no tilting option.**"],
# Slide 5-6
"toolTop"                : ["Cell support :shouldered M6 screws, shoulder 12mm. Location: [4;7] and [5;7]\
                            TOP bracket :shouldered M6 screws, shoulder 8mm. Location: [3;5] and [3;6]\
                            Hinges (optional) :shouldered M6 screws, shoulder 8mm. Location: [1;2] and [1;9]"], 
                            
# Slide 7-8
"toolBot"             : ["Cell support :shouldered M6 screws, shoulder 12mm. Location: [4;7] and [5;7]\
                            BOTTOM bracket :shouldered M6 screws, shoulder 8mm. Location: [1;5] and [1;6]\
                            Hinges (optional) :shouldered M6 screws, shoulder 8mm. Location: [1;2] and [1;9]"], 

# Slide 9-10
"toolFront"              : ["Cell support :shouldered M6 screws, shoulder 12mm. Location: [5;5] and [5;6]\
                            FRONT L3-L4 bracket :shouldered M6 screws, shoulder 8mm. Location: [3;9] and [4;9]\
                            Hinges (optional) :shouldered M6 screws, shoulder 8mm. Location: [1;10] and [8;10]"],  

# Slide 11-12
"toolBack"               : ["Cell support :shouldered M6 screws, shoulder 12mm. Location: [5;5] and [5;6]\
                            BACK L3-L4 bracket :shouldered M6 screws, shoulder 8mm. Location: [4;8] and [5;8]\
                            Stoppers :shouldered M6 screws, shoulder 8mm.\
                            **Stopper 1**, location: [4;6] and [4;7]**Stopper 2**, location: [6;8] and [7;8]\
                            **NO HINGES**"],

### TEXT IN SECTION 10 - ADJUSTEMENT AND TOOLS
"section10"               : ["ADJUSTEMENT AND TOOLS"],

"listTools"               : ["Here are the different adjustement and tools :1. ZIF locking tool \n2. Optical poles \n3. ZIF Insertion V1 \n4. ZRAY Clamp \
                             \n5. CLM Insertion \n6. CLM Un-mating \n7. Vacuum system \n8. ZIF Insertion V2 \n9. Base for carrier box"],
# Slide 14
"adjustZif"              : ["Adjustment for ZIF locking tool (only to be checked/done initially) :\
                            \nCheck that the locking tool close correctly the ZIF connector (with a real cell).\
                            \nThe locking tool must close the lever with no pressure on top of the ZIF body.\
                            \nIf needed, adjust the tool by bending it slightly or thanks to kapton tape.\
                            \nThe tool is permanently in contact with the protection frame during the locking process."],

# Slide 15
"adjustOpticalPoles"      : ["Strain relief and «Base for ZIF insertion» tools are mountedon poles that will\
                             be put in/removed from pole holders.\
                             \nHolders are initially located thanks to a cell or a dummy cell.\
                             \nVertical stoppers allow the tool to be at the right height.\
                             Strain relief tool:\nStrain relief tool in action must be parallel \
                             to the support and the fork must surround the strain relief hole.\
                             The weight of the tool is foreseen to maintain the pigtail on the strain\
                             relief washer, so the height will be tuned in order to have a slight gap in the «bayonet».\
                             \nFor inclined set-ups, take care that the fork does not compress the pigtails \
                             on the data connector but on the strain relief.\
                             The «bayonet» allows to switch the tool from garage position to active position."],
# Slide 16
"adjustPoles"             : ["«Base for ZIF insertion» tool:\n\
                             «Base for ZIF insertion» tool is adjusted with insertion tool clamped. \
                             Insertion tool clamping part must protrude by 2-3mm and roughly centered in the groove.\n\
                             The height is tuned the following way: when clamped, the insertion tool is in contact with \n\
                             the cell protection frame.\n«Base for ZIF insertion» tool must be parallel to the breadboard.\n\
                             Insertion tool pin is aligned with strain relief circle or washer.\nThe adjustment process is\
                             the same whatever the version of the tool used."],
# Slide 17
"adjustZrayClamp"         : ["In standard configuration, ZRAY connector is closed thanks to inserts and screws.\n\
                             In order to avoid this time-consuming step, a clamp system has been implemented on the bracket.\n\
                             Pigtail ZRAY side is maintained on Lakmin’s board thanks to a indexed rotating clamp.\n\
                             Two positions can be selected: clamping ZRAY and “garage”.\nA spring is constrained by a stopper. \
                             The compression can be adjusted thanks to the location of the stopper on the rod."],
# Slide 18
"ClmInsertion"            : ["It is mandatory to limit the applied force on the cell during the CLM power connector insertion.\n\
                             Insertion will be done thanks to a calibrated tool: by pressing the head, a spring is compressed and a rod protrudes out. \
                             A machined groove indicates the maximum tolerated force as a mark: the top edge of the tool should not overtake the groove."],
# Slide 19
"ClmUnMatting"            : ["It can be necessary to un-mate the CLM and FTM power connectors. CLM is on pigtail and FTM is on cell. \n\
                             The CLM un-mating tool is composed by two claws that surround the FTM connector.\n\
                             Pull vertically the power flex to extract the CLM."],
# Slide 20
"VacuumSystem"            : ["The vacuum system doesn’t need high power, the contact interface between cell and support \
                             should be good and leakage is very low if cell is well positioned.\n\
                             The diameter for the tube is 6mm. \nThe set-ups come with a section of tube and a 6mm-6mm tap.\n\
                             An important leakage that can be heard is usually a sign that the cell is not well positioned."],
# Slide 21
"BaseZifInsertion"        : ["Version 2 for «Base for ZIF insertion» tool will be tested during the travel of the set-ups.\n\
                             The adjustment is identical than for version 1, only the locker differs."],
# Slide 22
"BaseCarrierBox"          : ["The base for carrier box is a platform close to the cell destination that increases the safety for cell loading.\n\
                             The carrier box is maintained thanks two pins and so the cell can be loaded with minimum risk.\n\
                             The pole holder is common with the «Base for ZIF insertion» tool.\n\
                             Pictures show the set-up with a dummy cell in final position."],

### TEXT IN SECTION 11 - MOUNTING THE PIGTAIL
"section11"               : ["MOUNTING THE PIGTAIL"],  
## TOP & BOTTOM (24 - 39)

"TopBotDescription"      : ["The following description shows pigtail mounting for Flat TOP. \nThe process is the same for Flat BOTTOM. \n\
                            The process corresponds to a MECHANICAL MOUNTING ONLY WITH NO ELECTRICAL TESTS: \
                            the set-up and process are simplified, no need of tilting the set-up for interposer placement. \n\
                            Check that the ZRAY board flavour is “FRONT” (Lakmin’s board). \n\
                            Principal steps:\n- load the cell on the support & check.\n- ZIF insertion.\n- ZIF locking.\n\
                            - remove the ZIF locking tool.\n- CLM connection.\n- ZRAY clipping.\n- strain relief implementation.\n\
                            - unload the cell and storage."],

"TopBotLoadCheck"        : ["Load the cell & check, all tools are removed from holders\n\
                            1- Load the cell thanks 3-pods vacuum tool.\n2- Switch ON the vacuum for the support.\n\
                            3- Put the ZRAY clamp in “garage” position if not already.\n\
                            4- Put the protection rod over the ZRAY footprint if not already.\n\
                            5- Install the “base for ZIF insertion” tool."],

"TopBotZIFInsertion1"    : ["ZIF insertion:\n6- Place the (noted FLAT) locking tool for ZIF between ZIF and CLM for \
                            HV wire bond protection in the CLM area.\n7- Clamp the pigtail in the insertion tool (noted Flat/FRONT).\
                            Insertion tool clamps the data pigtail only.\nLocking is not possible if both flexes are clamped. \
                            The pin passes through the strain relief hole."],


"TopBotZIFInsertion2"    : ["ZIF insertion:\n8- Insert the data pigtail into the ZIF: \n\
                            1- approach the pigtail on the side of the cell, rigid ZRAY part of the  pigtail rests on the protection rod. \n\
                            2- insert data pigtail in ZIF connector with a slight angle."],

"TopBotZIFInsertion3"    : ["ZIF insertion:\nFlex auto aligns thanks to the degree of freedom around the pin axis."],


"TopBotZIFInsertion4"    : ["ZIF insertion:\n9- Clamp the insertion tool on its base in the groove.\nOnly the shaft is clamped. \n\
                             RK: the pigtail can be kept deeply inserted and operator can gain stability \
                             by clamping the tools with his left hand fingers and applying a slight pressure \
                             on the tool base at the same time. \n\n10- Check visually if data flex is totally inserted."],


"TopBotZIFLocking"       : ["ZIF locking:\n11- Slide the tool over the ZIF and lock the lever.\nLeave the tool between the data and power connectors."],

"TopBotRemoveZIF"        : ["Remove the ZIF insertion tool\n12- Unscrew the insertion tool and so free the pigtail.\n\
                            RK: the pigtail can be kept in place by the second operator thanks to a plier."],

"TopBotCLMConnect"       : ["CLM mating:\n14- Remove the ZIF locking tool.\n15- Align the pigtail by fingers or thanks to \
                            a plier and mate CLM thanks to the CLM insertion tool.\nDO NOT COMPRESS THE TOOL OVER \
                            THE MARK ON THE AXIS. If the CLM is still not fully inserted while the mark is reached, \
                            operator can swing the tool to finish the insertion."],

"TopBotZRAYClip1"        : ["ZRAY clamping:\nIn the case of a mounting with electrical tests, the full set-up is tilted in order to keep \
                            the interposer in place. The presented configuration corresponds to a mechanical mounting only, \
                            we did not use an interposer. \n\n16- Remove the protection rod. The pigtail is slightly stressed \
                            so it can bounce when the rod is removed.\nThe operator absorbs the bounce thanks to a plier."],

"TopBotZRAYClip2"        : ["ZRAY clamping:\n17- Place the pigtail on the board by finger or thanks to a plier, inserts in holes.\n\
                             If the pigtail doesn’t stay in position, maintain it thanks to a plier/finger.\n\
                             Turn the clamp from the garage position to the clamping position. \n\
                             The clamp can be handled on both sides."],


"TopBotStrainRelief1"    : ["Strain relief implementation:\n18- Remove the “base for ZIF insertion” tool.\n\
                            19- Place the strain relief tool.\nCheck the fork is in its higher position."],

"TopBotStrainRelief2"    : ["Strain relief implementation:\n20- Turn the fork harm and place it parallel to the breadboard.\n\
                            21- Turn the fork by 90° and let it slide in the bayonet groove. Adjust the whole system so that \
                            the strain relief hole is surrounded by the fork. Block the pole in its holder.\n\
                            Let the tool press the pigtail on the strain relief by its weight.\n\
                            If the fork is well adjusted in height, there should be a slight gap in the bayonet."],                           

"TopBotStrainRelief3"    : ["Strain relief implementation:\n22- Deposit the resin in the hole. Take care to fill correctly \
                            the strain relief and to have a contact between resin and the hybrid. \n\
                            Form a “mushroom” drop on top.\nResin deposition can be done by hand or thanks to a calibrated dispenser.\n\
                            23- Let cure, duration depends on the resin used (30min for DP100).\n\
                            The fork remains in position during the curing."],

"TopBotUnloadCell"       : ["Unload the cell and storage:\n24- Remove the strain relief tool.\n\
                            25- Put the clamp in garage position, so release the ZRAY.\n\
                            26- Switch OFF the vacuum for the support.\n27- Store carefully the cell in the storage tray."],



## FRONT (42 - 55)

"FrontDescription"      : ["The following description shows pigtail mounting for Inclined FRONT.\nThe process corresponds to a \
                           MECHANICAL MOUNTING ONLY WITH NO ELECTRICAL TESTS: the set-up and process are simplified, no need of \
                           tilting the set-up for interposer placement.\n\nCheck that the ZRAY board flavour is “FRONT” (Lakmin’s board).\n\n\
                           Principal steps:\n- load the cell on the support & check.\n- ZIF insertion.\n- ZIF locking.\n- remove the ZIF locking tool.\n\
                           - CLM connection.\n- ZRAY clipping.\n- strain relief implementation.\n- unload the cell and storage."],

"FrontLoadCheck"        : ["Load the cell & check, all tools are removed from holders\n\n1- Load the cell thanks 3-pods vacuum tool.\n\
                           2- Switch ON the vacuum for the support.\n3- Put the ZRAY clamp in “garage” position if not already.\n\
                           4- Install the “base for ZIF insertion” tool."],

"FrontZIFInsertion1"    : ["ZIF insertion\n5- Place the (noted INCLINED) locking tool for ZIF between ZIF and CLM for HV wire \
                           bond protection in the CLM area (in contact with the stops).\n6- Clamp the pigtail in the insertion tool \
                           (noted Flat/FRONT). Insertion tool clamps the data pigtail only.\nLocking is not possible if both flexes are clamped. \
                           The pin passes through the strain relief hole."],

"FrontZIFInsertion2"    : ["ZIF insertion\n7- Insert the data pigtail into the ZIF with a slight angle."],


"FrontZIFInsertion3"    : ["ZIF insertion\n8- Clamp the insertion tool on its base in the groove.\nOnly the shaft is clamped.\n\
                           RK: the pigtail can be kept deeply inserted and operator can gain stability by clamping the tools \
                           with his left hand fingers and applying a slight pressure on the tool base at the same time.\n\n\
                           9- Check visually if data flex is totally inserted."],

"FrontZIFLocking"       : ["ZIF locking\n10- Slide the tool over the ZIF and lock the lever. \nLeave the tool between the data and power connectors."],                           

"FrontRemoveZIF1"       : ["Remove the ZIF insertion tool\n11- Unscrew the insertion tool and so free the pigtail."],

"FrontRemoveZIF2"       : ["Remove the ZIF insertion tool\n12- Release and remove the insertion tool from its base."],

"FrontCLMConnect"       : ["CLM mating\n13- Remove the ZIF locking tool.\n14- Align the pigtail by fingers or thanks to \
                           a plier and mate CLM thanks to the CLM insertion tool.\nDO NOT COMPRESS THE TOOL OVER THE MARK \
                           ON THE AXIS. If the CLM is still not fully inserted while the mark is reached, operator can swing \
                           the tool to finish the insertion."],

"FrontZRAYClip"         : ["ZRAY clamping\nIn the case of a mounting with electrical tests, the full set-up is tilted in order \
                           to keep the interposer in place.\nThe presented configuration corresponds to a mechanical mounting \
                           only, we did not use an interposer.\n\n15- Place the pigtail on the board by finger or thanks to \
                           a plier, inserts in holes.\nIf the pigtail doesn’t stay in position, maintain it thanks to a \
                           plier/finger.\nTurn the clamp from the garage position to the clamping position.\nThe clamp can be handled on both sides."],

"FrontStrainRelief1"    : ["Strain relief implementation\n16- Remove the “base for ZIF insertion” tool.\n17- Place the strain relief tool.\n\
                           Check the fork is in its higher position."],

"FrontStrainRelief2"    : ["Strain relief implementation\n18- Turn the fork harm and place it parallel to the breadboard.\n19- Turn the fork by \
                            90° and let it slide in the bayonet groove. Adjust the whole system so that the strain relief hole is surrounded by \
                            the fork. Block the pole in its holder.\nLet the tool press the pigtail on the strain relief by its weight.\n\
                            If the fork is well adjusted in height, there should be a slight gap in the bayonet."],

"FrontStrainRelief3"    : ["Strain relief implementation\n20- Deposit the resin in the hole. Take care to fill correctly the strain relief and to \
                            have a contact between resin and the hybrid.\nForm a “mushroom” drop on top.\nResin deposition can be done by hand or \
                            thanks to a calibrated dispenser.\n21- Let cure, duration depends on the resin used (30min for DP100).\n\
                            The fork remains in position during the curing."],                                                        

"FrontUnloadCell"       : ["Unload the cell and storage.\n22- remove the strain relief tool.\n23- Turn the clamp and so release the ZRAY.\nPut the clamp in garage position.\n\
                           24- Switch OFF the vacuum for the support. \n25- Store carefully the cell in the storage tray."],



## BACK (58 - 70)

"BackDescription"       : ["The following description shows pigtail mounting for Inclined BACK.\nThe process corresponds to a \
                          MECHANICAL MOUNTING ONLY WITH NO ELECTRICAL TESTS.\nStrain relief implementation is not possible with \
                          the bracket and test board in place. Bracket and test board are used only if electrical tests are needed: \
                          The set-up is limited to the cell support.\nBack process doesn’t need to tilt the set-up, even for electrical tests.\n\n\
                          Check that the ZRAY board flavour is “BACK” (Lakmin’s board).\n\nPrincipal steps:\n- load the cell on the support & check.\n\
                          - ZIF insertion.\n- ZIF locking.\n- remove the ZIF locking tool.\n- CLM connection.\n- ZRAY clipping.\n- strain relief implementation.\n\
                           unload the cell and storage."],

"BackLoadCheck"         : ["Load the cell & check, all tools are removed from holders\n\n1- Load the cell thanks 3-pods vacuum tool.\n\
                          2- Switch ON the vacuum for the support.\n3- Install the “base for ZIF insertion” tool."],

"BackZIFInsertion1"     : ["ZIF insertion\n4- Place the (noted INCLINED) locking tool for ZIF between ZIF and CLM for HV wire bond \
                          protection in the CLM area (in contact with the stops).\n5- Clamp the pigtail in the insertion tool (noted BACK). \
                          Insertion tool clamps the data pigtail only.\nLocking is not possible if both flexes are clamped. The pin passes through the strain relief hole."],

"BackZIFInsertion2"     : ["ZIF insertion\n6- Insert the data pigtail into the ZIF with a slight angle."],

"BackZIFInsertion3"     : ["ZIF insertion\n7- Clamp the insertion tool on its base in the groove.\nOnly the shaft is clamped.\n\
                          RK: the pigtail can be kept deeply inserted and operator can gain stability by clamping the tools \
                          with his left hand fingers and applying a slight pressure on the tool base at the same time.\n\n\
                          8- Check visually if data flex is totally inserted."],

"BackZIFLocking"        : ["ZIF locking\n9- Slide the tool over the ZIF and lock the lever.\n10- Remove the ZIF locking tool."],

"BackRemoveZIF1"        : ["Remove the ZIF insertion tool\n11- Unscrew the insertion tool and so free the pigtail."],

"BackRemoveZIF2"        : ["Remove the ZIF insertion tool\n12- Release and remove the insertion tool from its base:\n\
                           1- slide the tool by the left.\n 2- remove the tool."],

"BackCLMConnect"        : ["CLM mating\n13- Align the pigtail by fingers or thanks to a plier and mate CLM thanks to the CLM insertion tool.\n\
                          DO NOT COMPRESS THE TOOL OVER THE MARK ON THE AXIS. If the CLM is still not fully inserted while the mark is reached, \
                          operator can swing the tool to finish the insertion."],

"BackStrainRelief1"     : ["Strain relief implementation\nNO TEST BOARD USED FOR BACK PIGTAILS\nNO ZRAY CLAMPING\n14- Remove the “base for ZIF insertion” tool.\n\
                          15- Place the strain relief tool.\nCheck the fork is in its higher position."],

"BackStrainRelief2"     : ["Strain relief implementation\n16- Turn the fork harm and place it parallel to the breadboard.\n17- Turn the fork by 90° and let it \
                           slide in the bayonet groove. Adjust the whole system so that the strain relief hole is surrounded by the fork. Block the pole in its holder.\n\
                           Let the tool press the pigtail on the strain relief by its weight.\nIf the fork is well adjusted in height, there should be a slight gap in the bayonet."],                         

"BackStrainRelief3"     : ["Strain relief implementation\nNO TEST BOARD USED FOR BACK PIGTAILS\nNO ZRAY CLAMPING\n14- Remove the “base for ZIF insertion” tool.\n\
                          15- Place the strain relief tool.\nCheck the fork is in its higher position."],

"BackStrainRelief4"     : ["Strain relief implementation\n18- Deposit the resin in the hole. Take care to fill correctly the strain relief and \
                           to have a contact between resin and the hybrid.\nForm a “mushroom” drop on top.\n\
                           Resin deposition can be done by hand or thanks to a calibrated dispenser.\n19- Let cure, duration depends on the resin \
                           used (30min for DP100).\nThe fork remains in position during the curing."],

"BackUnloadCell"        : ["Unload the cell and storage.\n20- Remove the strain relief tool.\n21- Switch OFF the vacuum for the support.\n\
                           22- Remove carefully the cell thanks to the 3-pods vaccum tool and store it in the storage tray."],

}


# Dictionnary of steps for each type of mounting for section 11

# Top/Bot
steps_top_bot = {
    1: "1-Load the module thanks 3-pods vacuum tool",
    2: "2-Switch ON the vaccum for the support",
    3: "3-Put the ZRAY clamp in « garage » position if not already",
    4: "4-Put the protection rod over the ZRAY footprint if not already",
    5: "5-Put the « base » for ZIF insertion « tool », parallel to the breadboard",
    6: "6-Place the FLAT locking tool for ZIF between ZIF and CLM",
    7: "7-Clamp the pigtail in the Flat/FRONT insertion tool, data pigtail only.",
    8: "8-Insert the data pigtail into the ZIF",
    9: "9-Lock the insertion tool on its base in the groove",
    10: "10-Check visually data flex is totally inserted",
    11: "11-Slide the locking tool over the ZIF and lock the lever",
    12: "12-Unscrew the insertion tool and so free the pigtail",
    13: "13-Release and remove the insertion tool from its base",
    14: "14-Remove the ZIF locking tool",
    15: "15-Mate the CLM connector",
    16: "16-Remove the protection rod. Absorb the bounce.",
    17: "17-Place the pigtail on the board and clamp it",
    18: "18-Remove the « base for ZIF insertion » tool",
    19: "19-Place the strain relief tool. Check the fork is in its higher position",
    20: "20-Turn the fork harm and place it parallel to the breadboard",
    21: "21-Turn the fork by 90° and let it press the pigtail",
    22: "22-Deposit the resin (Time of deposition)",
    23: "23-Let cure depending on the resin use (Used resin)",
    24: "24-Remove the strain relief tool",
    25: "25-Turn the clamp and so release the ZRAY. Put the clamp in garage position",
    26: "26-Switch OFF the vacuum for the support",
    27: "27-Store carefully the module in the storage tray"
}

# Front
steps_front = {
    1: "1-Load the module thanks 3-pods vacuum tool",
    2: "2-Switch ON the vaccum for the support",
    3: "3-Put the ZRAY clamp in « garage » position if not already",
    4: "4-Put the « base » for ZIF insertion « tool », parallel to the breadboard",
    5: "5-Place the INCLINED locking tool for ZIF between ZIF and CLM",
    6: "6-Clamp the pigtail in the Flat/FRONT insertion tool, data pigtail only.",
    7: "7-Insert the data pigtail into the ZIF",
    8: "8-Clamp the insertion tool on its base in the groove",
    9: "9-Check visually data flex is totally inserted",
    10: "10-Slide the locking tool over the ZIF and lock the lever",
    11: "11-Unscrew the insertion tool and so free the pigtail",
    12: "12-Release and remove the insertion tool from its base",
    13: "13-Remove the ZIF locking tool",
    14: "14-Mate the CLM connector",
    15: "15-Place the pigtail on the board and clamp it",
    16: "16-Remove the « base for ZIF insertion » tool",
    17: "17-Place the strain relief tool. Check the fork is in its higher position",
    18: "18-Turn the fork harm and place it parallel to the breadboard",
    19: "19-Turn the fork by 90° and let it press the pigtail",
    20: "20-Deposit the resin (Time of deposition)",
    21: "21-Let cure depending on the resin use (Used resin)",
    22: "22-Remove the strain relief tool",
    23: "23-Turn the clamp and so release the ZRAY. Put the clamp in garage position",
    24: "24-Switch OFF the vacuum for the support",
    25: "25-Store carefully the module in the storage tray"
}

# Back
steps_back = {
    1: "1-Load the module thanks 3-pods vacuum tool",
    2: "2-Switch ON the vaccum for the support",
    3: "3-Put the « base » for ZIF insertion « tool », parallel to the breadboard",
    4: "4-Place the INCLINED locking tool for ZIF between ZIF and CLM",
    5: "5-Clamp the pigtail in the Flat/FRONT insertion tool, data pigtail only.",
    6: "6-Insert the data pigtail into the ZIF",
    7: "7-Clamp the insertion tool on its base in the groove",
    8: "8-Check visually data flex is totally inserted",
    9: "9-Slide the locking tool over the ZIF and lock the lever",
    10: "10-Remove the ZIF locking tool",
    11: "11-Unscrew the insertion tool and so free the pigtail",
    12: "12-Release and remove the insertion tool from its base",
    13: "13-Mate the CLM connector",
    14: "14-Remove the « base for ZIF insertion » tool",
    15: "15-Place the strain relief tool. Check the fork is in its higher position",
    16: "16-Turn the fork harm and place it parallel to the breadboard",
    17: "17-Turn the fork by 90° and let it press the pigtail",
    18: "18-Deposit the resin (Time of deposition)",
    19: "19-Let cure depending on the resin use (Used resin)",
    20: "20-Remove the strain relief tool",
    21: "21-Switch OFF the vacuum for the support",
    22: "22-Store carefully the module in the storage tray"
}

# Sequence of images for Top/Bot, Front, Back
image_steps_top_bot = [
    [],  # Image 1
    [1, 2, 3, 4, 5],  # Image 2
    [6, 7],  # Image 3
    [8],  # Image 4
    [],  # Image 5
    [9, 10],  # Image 6
    [11],  # Image 7
    [12],  # Image 8
    [13],  # Image 9
    [14, 15],  # Image 10
    [16],  # Image 11
    [17],  # Image 12
    [18, 19],  # Image 13
    [20, 21],  # Image 14
    [22, 23],  # Image 15
    [24, 25, 26, 27]  # Image 16
]

image_steps_front = [
    [],  # Image 1
    [1, 2, 3, 4],  # Image 2
    [5, 6],  # Image 3
    [7],  # Image 4
    [8, 9],  # Image 5
    [10],  # Image 6
    [11],  # Image 7
    [12],  # Image 8
    [13, 14],  # Image 9
    [15],  # Image 10
    [16, 17],  # Image 11
    [18, 19],  # Image 12
    [20, 21],  # Image 13
    [22, 23, 24, 25]  # Image 14
]

image_steps_back = [
    [],  # Image 1
    [1, 2, 3],  # Image 2
    [4, 5],  # Image 3
    [6],  # Image 4
    [7, 8],  # Image 5
    [9, 10],  # Image 6
    [11],  # Image 7
    [12],  # Image 8
    [13],  # Image 9
    [14, 15],  # Image 10
    [16, 17],  # Image 11
    [18, 19],  # Image 12
    [20, 21, 22]  # Image 13
]

dictFR={}
dictEN={}

# for key in all_dictionary.keys():
#     dictFR[key] = all_dictionary[key][0]
#     dictEN[key] = all_dictionary[key][1]

for key, values in all_dictionary.items():
    dictFR[key] = values[0]  # 1ere entrée en français
    dictEN[key] = values[1] if len(values) > 1 else values[0]  # Si 2eme entrée, en anglais, sinon en français

# Content for each part (text and images)
# content = {
#     'Notice': {
#         1: all_dictionary['coordBreadboard'][0],
#     },
#     'Top': {
#         1: all_dictionary['toolTop'][0],
#     },
#     'Bot': {
#         1: all_dictionary['toolBot'][0],
#     },
#     'Front': {
#         1: all_dictionary['toolFront'][0],
#     },
#     'Back': {
#         1: all_dictionary['toolBack'][0],
#     }
# }


if __name__ == "__main__":
    print(dictFR)
    print(dictEN)
