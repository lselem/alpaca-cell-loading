# -*- Omeed BAHRAMI -*-
# -*-coding:utf-8 -*-
import sys, os

#================================================================================
# Add the path of the folder containing the project to sys.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))
# Set the environment variable
os.environ['DB_TEMPLATE_PATH'] = '/home/itk/itk/WebApp_Omeed/alpaca-cell-loading-master/config/DB_template.yaml'
#================================================================================

from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

#================================================================================
#-- Initialise session_state: 
#   * only upload_stage really necessary, then page stop anyway
if 'lapp_upload_stage' not in st.session_state.keys():
    st.session_state['lapp_upload_stage'] = -1

#-- Page stage management: 
st.title(txt['toLAPPProdDB'])

if st.session_state['lapp_upload_stage'] == -1:
    st.write(txt['Init'])
    st.stop()
elif st.session_state['lapp_upload_stage'] == 0:
    st.write(txt['Closed'] + st.session_state.nextStep)
    st.stop()
elif st.session_state['lapp_upload_stage'] == 1:
    st.write(txt['Ongoing'])
    Conditions_in_sidebar("DB")

    st.write("---")

elif st.session_state['lapp_upload_stage'] == 2:
    st.write(txt['Updatable'])
    st.error("UNREACHABLE STATE")

elif st.session_state['lapp_upload_stage'] == 3:
    st.write(txt['Final'])

    st.write("---")
