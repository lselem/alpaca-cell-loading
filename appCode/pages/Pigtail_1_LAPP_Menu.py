# -*- Omeed BAHRAMI -*-
# -*-coding:utf-8 -*-

from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *
from streamlit_extras.switch_page_button import switch_page

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

# Initialize session state information
if 'informations' not in st.session_state:
    st.session_state['informations'] = {
        'Name': '',
        'Temperature': '',
        'Humidity': '',
        'selected_object': None
    }

# Initialize the stage for the LAPP menu in session state if not already set
if 'pigtail_stage1' not in st.session_state.keys():
    st.session_state['pigtail_stage1'] = -1

# Display the title of the page
st.title(txt['section8'])

# Display the sidebar with the current state information
display_sidebar()

#================================================================================
# Stage 1: Initial information input
# This stage is responsible for collecting initial information from the user
if st.session_state['pigtail_stage1'] == 1:
    with st.container(border=True):
        col1 = st.columns(1)[0]
        with col1:
            # Collecting user information: Name, Temperature, and Humidity
            name = st.text_input(label=txt['NameLAPP'], value=st.session_state['informations']['Name'], placeholder='Your name')
            temperature = st.text_input(label=txt['TemperatureLAPP'], value=st.session_state['informations']['Temperature'], placeholder='In degree Celsius')
            humidity = st.text_input(label=txt['HumidityLAPP'], value=st.session_state['informations']['Humidity'], placeholder='In %')

        col2 = st.columns(1)[0]
        # Submit button to validate and save the entered information
        submit_button = st.button(label='Register')
        if submit_button:
            # Validate inputs before saving
            if not name or not name.strip():
                st.error("Name cannot be empty!")
            elif not any(char.isalpha() for char in name):
                st.error("Name must contain letters!")
            elif not temperature.isdigit():
                st.error("Temperature should be a number!")
            elif not humidity.isdigit():
                st.error("Humidity should be a number!")
            else:
                # Save valid inputs to session state and proceed to the next stage
                st.session_state['informations']['Name'] = name
                st.session_state['informations']['Temperature'] = temperature
                st.session_state['informations']['Humidity'] = humidity
                st.session_state['pigtail_stage1'] = 2
                st.rerun()

#================================================================================
# Stage 2: Option selection after entering initial information
# This stage allows the user to select a process option to proceed
elif st.session_state['pigtail_stage1'] == 2:
    with st.container(border=True):
        # Display a dropdown menu for process selection
        selected_option = st.selectbox("Select an option:", ["Position of Tools", "Adjustements and Tools", "Mounting on Cells"])        
        # Submit button to confirm the selected option and navigate to the corresponding process page
        submit_button = st.button(label='Selected')
        if submit_button:
            if selected_option == "Position of Tools":
                # Update session state and navigate to the Position of Tools page
                st.session_state.pigtail_stage2 = 1
                st.session_state.pigtail_stage3 = 0
                st.session_state.pigtail_stage4 = 0
                st.switch_page("pages/Pigtail_2_Position_of_Tools.py")
            elif selected_option == "Adjustements and Tools":
                # Update session state and navigate to the Adjustements and Tools page
                st.session_state.pigtail_stage2 = 0
                st.session_state.pigtail_stage3 = 1
                st.session_state.pigtail_stage4 = 0
                st.switch_page("pages/Pigtail_3_Adjustements_and_Tools.py")
            elif selected_option == "Mounting on Cells":
                # Update session state and navigate to the Mounting on Cells page
                st.session_state.pigtail_stage2 = 0
                st.session_state.pigtail_stage3 = 0
                st.session_state.pigtail_stage4 = 1
                st.switch_page("pages/Pigtail_4_Mounting_Process.py")
