# -*-coding:utf-8 -*-
from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *

st.set_page_config(
    page_title=txt['cutting'],
    page_icon=":mail:",
    layout="centered",
    initial_sidebar_state="expanded",
    #menu_items={
    #    'Get Help': 'mkDoc coming soon...',
    #    'Report a bug': "Contact luka.selem@lpsc.in2p3.fr",
    #    'About': "# Alpaca cell loading app. Using Streamlit !"
    #}
    )

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

def Validation_Cutting(com, result=True):

    #FIXME possibility of failed result

    module=str(st.session_state.refModule)
    conn = st.session_state.conn

    #-- Create new conditions for this step
    cond_id = Insert_Conditions(com)
    if cond_id is None: #-- Insert_Conditions failed
        return

    pass_inspection="""
            UPDATE Modules 
               SET cond_cutting_fk = %s,
                   result_cutting = %s
             WHERE module_id = %s
             """
    try:
        execute_query(conn, pass_inspection, (
            cond_id,
            result,
            module,
            ))
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    st.session_state.allmsg.append( [3,txt["logCuttingDone"]] )

    status_code = Module_Status(st.session_state.refModule)
    return

def Affichage_Cutting():

    module=str(st.session_state.refModule)
    conn = st.session_state.conn

    find_module = """
            SELECT
                   result_cutting,
                   Conditions.comment,
                   Conditions.operator,
                   Conditions.humidity,
                   Conditions.temperature,
                   Conditions.datetime
              FROM Modules
        INNER JOIN Conditions
                ON cond_cutting_fk = cond_id
             WHERE module_id = %s
    """
    data = None
    try:
        data = execute_read_query(conn, find_module, (module,) )
        assert len(data) == 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #By now, one and only one module should be in the DB
        st.session_state.allmsg.append( [0,"Not exactly one object found with id "+id] )
        return
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    moduleState = list(data[0])

    return moduleState

#================================================================================
#-- Initialise session_state: 
#   * only module_stage{IS} really necessary, then page stop anyway
if 'module_stage3' not in st.session_state.keys():
    st.session_state['module_stage3'] = -1

#-- Page stage management: 
st.title(txt['section3'])

if st.session_state['module_stage3'] == -1:
    st.write(txt['Init'])
    st.stop()
elif st.session_state['module_stage3'] == 0:
    st.write(txt['Closed'] + st.session_state.nextStep)
    st.stop()
elif st.session_state['module_stage3'] == 1:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCarrier'],value=st.session_state.refCarrier)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Ongoing'])
    Conditions_in_sidebar("Cut")

    st.write("---")

    Commentaires=st.text_area(label=txt['com'], height=20, value='')

    #-- Avoid accidentally loading empty comment
    if Commentaires == '':
        noComment = st.toggle(txt['noCom'],False)
    if (Commentaires != '') or noComment: 
        st.button(txt['validateCut'], on_click=Validation_Cutting,
                kwargs= { 'com' : Commentaires }
                )

    Print_out_messages()

elif st.session_state['module_stage3'] == 2:
    st.write(txt['Updatable'])
    st.error("UNREACHABLE STATE")

elif st.session_state['module_stage3'] == 3:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCarrier'],value=st.session_state.refCarrier)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Final'])

    st.write("---")

    moduleState = Affichage_Cutting()
    if moduleState is None:
        Print_out_messages()
        st.stop()

    df = pd.DataFrame( (moduleState,), columns = [
                        txt['resultCut'],  #result
                        txt['com'],        #Conditions.comment
                        txt['operator'],   #Conditions.operator",
                        txt['humidity'],   #Conditions.humidity",
                        txt['temperature'],#Conditions.temperature",
                        txt['date'],       #Conditions.datetime",
                     ])
    st.dataframe(df,height=20)

    Print_out_messages()
