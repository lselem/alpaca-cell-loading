# -*-coding:utf-8 -*-
from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *
from utils.logDepotParser import List_Log_Depot, Collect_Expected_Fields

st.set_page_config(
    page_title=txt['glue'],
    page_icon=":mail:",
    layout="centered",
    initial_sidebar_state="expanded",
    #menu_items={
    #    'Get Help': 'mkDoc coming soon...',
    #    'Report a bug': "Contact luka.selem@lpsc.in2p3.fr",
    #    'About': "# Alpaca cell loading app. Using Streamlit !"
    #}
    )

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

def ListStycast() :
    conn = st.session_state.conn

    all_stycast = """
          SELECT stycast_id,
                 refglue,
                 no_pot,
                 no_cartridge
            FROM Stycast
           WHERE expiry_date >= CURRENT_DATE
        ORDER BY expiry_date ASC
        """
    all_catalyst = """
          SELECT catalyst_id,
                 refcata,
                 no_pot
            FROM Catalyst
           WHERE expiry_date >= CURRENT_DATE
        ORDER BY expiry_date ASC
        """

    try:
        stycastDB  = execute_read_query(conn, all_stycast )
        catalystDB = execute_read_query(conn, all_catalyst)
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return [],[]

    Lstycast=[]
    for d in stycastDB :
        Lstycast.append( [d[0],
                "Batch_Num:"+str(d[1])+
                "__Pot_Num:"+str(d[2])+
                "__Cartridge_Num:"+str(d[3])
                ])

    Lcatalyst=[]
    for d in catalystDB:
        Lcatalyst.append( [d[0],
                "Batch_Num:"+str(d[1])+
                "__Flacon_Num:"+str(d[2])
                ])

    return Lstycast,Lcatalyst

def CalcMassCat(mSty):

    cat9   = float(mSty)*3.5/100
    result = round(cat9,2)
    return result

def CalcMassGlue(mSty):

    cat9=float(mSty)*3.5/100 + float(mSty)
    result = round(cat9,2)
    return result

def CalcErreurCat9(mMeas,mTheo):
    try:
        Err=100*(float(mMeas)-float(mTheo))/float(mTheo)
    except:
        Err=0

    result = round(Err,1)
    return result

def CalcCorrectionPression(mVerre, mSamp):

    ##################################################################
    # Masse de l'échantillon témoin les données mSamp, mVerre arrivent
    # en gramme le calcul se fait sur de mg
    MassGlueSample = (float(mSamp)-float(mVerre))*1000

    ###########################
    # Masse Cible (mg)
    MasseCible = 89.3
    ###########################
    # Temps de realisation d'un dépôt
    DureeDepot = 27

    ##################################################################
    # Temps d attente entre le dépôt cible et le dépôt final
    TempsAttente = 135  # Attente de 2.5 min

    ##################################################################
    # Pente de correction à appliquer
    PenteCorr = 0.0528

    ##################################################################
    # Calcul de la pression à appliquer lors du dépôt sur la cellule
    PressionDepotFinal = (1.1 * MasseCible) /  \
                         (MassGlueSample - PenteCorr * (TempsAttente + DureeDepot))

    result = round(PressionDepotFinal,2)
    return PenteCorr, result


def Get_Cell_Mass():

    conn = st.session_state.conn

    cell_mass = """
          SELECT mass
            FROM Cells
           WHERE cell_id = %s
        """
    try:
        data  = execute_read_query(conn, cell_mass, (st.session_state.refCell,) )
        assert len(data) == 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #By now, one and only one module should be in the DB
        st.session_state.allmsg.append( [0,"Not exactly one object found with id "+id] )
        return ''
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    return data[0][0]

def CalcErreurColleDeposee(mMeas,mCell):
    masseGlue=float(mMeas)-float(mCell)
    Err=100*(masseGlue/0.893 -1)

    return round(masseGlue,2), round(Err,2)

def Enregistrement_DepotColle(dictData, resDep, reuse, com):
    cell=str(st.session_state.refCell)
    conn = st.session_state.conn

    #-- TODO Check filled dict

    #-- Create new conditions for this step
    cond_id = Insert_Conditions( com )
    if cond_id is None: #-- Insert_Conditions failed
        return

    load_dep="""
        INSERT INTO GlueDeposit (
                    cell_fk,
                    """
    dep_tuple = (cell,)
    for d in dictData.keys():
        load_dep += str(d) +",\n"
        dep_tuple += (dictData[d],)
    load_dep += """ cond_fk,
                    result
                    )
            VALUES ("""+"%s,"*(len(dictData)+2)+"%s)"
    dep_tuple += (cond_id, resDep)

    #st.session_state.allmsg.append( [2,load_dep] )
    #st.session_state.allmsg.append( [2,str(dep_tuple)] )

    try:
        execute_query(conn, load_dep, dep_tuple)
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    st.session_state.allmsg.append( [3,txt["logAddDep"]] )

    #-- Reset VerifPressure and conditions update for the next deposit
    #   NOTE: Hack! Unchecking the VerifPression checkbox resets all subsequent
    #   widgets to default
    st.session_state.resetWidget = False
    st.session_state.conditions_checked = False

    #--Update Cells table 
    #  if the deposit succeeded or
    #  if the cell is not reusable
    if resDep:      #Successful deposit
        result = True
    elif not reuse: #Failed deposit and not reusable cell
        result = False
    else:           #Failed deposit but reusable cell
        st.session_state.allmsg.append( [1,txt["logTryAgain"]] )
        return

    pass_dep="""
        UPDATE Cells 
           SET cond_gluing_fk = %s,
               result_gluing = %s
         WHERE cell_id = %s
         """
    try:
        execute_query(conn, pass_dep, (
            cond_id,
            result,
            cell,
            ))
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    st.session_state.allmsg.append( [3,txt["logGluingDone"]] )

    Cell_Status(st.session_state.refCell)
    return

def Affichage_allDep():

    cell = str(st.session_state.refCell)
    conn = st.session_state.conn

    cell_alldep = """
            SELECT
                   Stycast.refglue,
                   Stycast.no_pot,
                   Stycast.no_cartridge,
                   Stycast.opening_date,
                   Stycast.expiry_date,
                   Catalyst.refcata,
                   Catalyst.no_pot,
                   Catalyst.opening_date,
                   Catalyst.expiry_date,
                   mass_stycast,
                   mass_gluemix,
                   ini_pressure,
                   mass_bare_sample,
                   mass_glued_sample,
                   corr_slope,
                   corr_pressure,
                   datetime_deposit,
                   duration_sample,
                   duration_deposit,
                   result,
                   Conditions.comment,
                   Conditions.operator,
                   Conditions.humidity,
                   Conditions.temperature,
                   Conditions.datetime    
              FROM GlueDeposit
        INNER JOIN Conditions
                ON cond_fk = cond_id
        INNER JOIN Stycast
                ON stycast_fk = stycast_id
        INNER JOIN Catalyst
                ON catalyst_fk = catalyst_id
             WHERE cell_fk = %s
          ORDER BY Conditions.datetime ASC
    """
    data = None
    try:
        data = execute_read_query(conn, cell_alldep, (cell,) )
        assert len(data) >= 1
    except AssertionError as err:
        st.session_state.allmsg.append( 
                [0,"There should be at least 1 deposit by now!"] )
        return 
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    cpt = 0
    for dep in data:
        #--Check at most one deposit succeeded
        if dep[19] :
            cpt +=1
        if cpt > 1:
            st.session_state.allmsg.append( [0,"There shouldn't be multiple \
                        successfull results for a given cell !"] )
            break #return FIXME

    return data

def Affichage_DepotColle( dep ):
    return

def Glue_Preparation(Lstycast,Lcatalyst):

    #--Select Stycast and catalyst
    Stycast = st.selectbox(label=txt['rSty'],options=[l[1] for l in Lstycast] ,index=0)
    Cat9    = st.selectbox(label=txt['rCat'],options=[l[1] for l in Lcatalyst],index=0)
    #-- Switch from object string description to database id
    for l in Lstycast:
        if Stycast == l[1]:
            Stycast = l[0]
            break;
    for l in Lcatalyst:
        if Cat9 == l[1]:
            Cat9 = l[0]
            break;

    #--Initial quantity of Stycast
    MassStycast = st.number_input(label=txt['mesMSty'],
                               value=15.0, min_value = 5.0, max_value=25.0)
    colG, colD = st.columns(2)
    TheoMassCat9 = CalcMassCat (MassStycast)
    TheoMassGlue = CalcMassGlue(MassStycast)
    colG.metric(label=txt['theoMCat'], value=str(TheoMassCat9)+" g")
    colD.metric(label=txt['theoMGlue'], value=str(TheoMassGlue)+" g")

    #--Quality of glue mix
    #  NOTE: Hard limit at +/- 50%
    MassGlue = colG.number_input(label=txt['mesMGlue'], value=TheoMassGlue,
            min_value =TheoMassGlue*0.5, max_value=TheoMassGlue*1.5)
    ErrMassGlue = CalcErreurCat9(MassGlue,TheoMassGlue)
    #-- Overthreshold error is labeled in red
    labelErr = txt['errMGlue']
    if ErrMassGlue > 10 or ErrMassGlue < -10:
        labelErr = ":red[__"+labelErr+"__]"
    colD.metric(label=labelErr, value=str(ErrMassGlue)+"%")

    glueData = {
        'stycast_fk'   : Stycast,
        'catalyst_fk'  : Cat9,
        'mass_stycast' : MassStycast,
        'mass_gluemix' : MassGlue,
        }
    #--TODO move to JSON if parameters to store too undecided
    return glueData

def Use_Log_File():

    logFiles = List_Log_Depot()
    if logFiles is None:
        st.warning(txt['noLogFileDep'])
        Print_out_messages()
        st.stop()

    fname = st.selectbox(label=txt['chooseLogF'],options=[f[0] for f in logFiles],
            index=None, placeholder="...")

    if fname is not None:
        depData = Collect_Expected_Fields(fname)

        if depData is None:
            Print_out_messages()
            st.stop()
        return depData

    Print_out_messages()
    st.stop()


def Use_Manual_Input():

    InitialPressure = st.number_input(label=txt['iniPr'],
                               value=2.0, min_value = 0., max_value=4.)
    #--Sample Deposit
    MassBareSample = st.number_input(label=txt['verreM'],help=txt['vInstr'],
                               value=3.9, min_value = 0., max_value=10.)
    WaitTime = st.metric(label=txt['tpsAtt'], value="2 min 30 s")
    DurationSample = st.number_input(label=txt['tpsSamp'], 
                               value=120, min_value=0, max_value=240)
    MassGluedSample = st.number_input(label=txt['verreGlue'],
                               value=4., min_value = 0., max_value=10.)
    colG1, colD1 = st.columns(2)
    CorrSlope, CorrPressure = CalcCorrectionPression(MassBareSample,MassGluedSample)
    colG1.metric(label=txt['corrSl'], value=CorrSlope)
    colD1.metric(label=txt['corrPr'], value=str(CorrPressure)+" bar")

    #--Cell Deposit
    mCell = Get_Cell_Mass()
    MassCellGlue = st.number_input(label=txt['cellGlue'],
                               value=mCell+0.9, min_value = mCell-5., max_value=mCell+5.)
    DurationDep = st.number_input(label=txt['tpsSamp'], 
                               value=270, min_value=0, max_value=1000)
    MassGlueDep, ErrDepGlue = CalcErreurColleDeposee(MassCellGlue,mCell)
    colG2, colD2 = st.columns(2)
    colG2.metric(label=txt['depGlue'], value=str(MassGlueDep)+" g")
    #-- Overthreshold error is labeled in red
    labelErr = txt['errDepGlue']
    if ErrDepGlue > 1 or ErrDepGlue < -1:
        labelErr = ":red[__"+labelErr+"__]"
    colD2.metric(label=labelErr, value=str(ErrDepGlue)+" %")

    depData = {
        'ini_pressure'      : InitialPressure,
        'mass_bare_sample'  : MassBareSample,
        'mass_glued_sample' : MassGluedSample,
        'corr_slope'        : CorrSlope,
        'corr_pressure'     : CorrPressure,
        #'datetime_deposit'  : None,
        'duration_sample'   : time(0,DurationSample//60,DurationSample%60),
        'duration_deposit'  : time(0,DurationDep//60,DurationDep%60),
        }
    #--TODO move to JSON if parameters to store too undecided
    #try:
    #    depData = json.dumps(depData)
    #except Exception as err:
    #    raise Exception(txt['failedJSON']+"\n --> "+str(err))
    #    return

    return depData


#================================================================================
#-- Initialise session_state: 
#   * only cell_stage{IS} really necessary, then page stop anyway
if 'cell_stage3' not in st.session_state.keys():
    st.session_state['cell_stage3'] = -1

#-- Page stage management: 
st.title(txt['section6'])

if st.session_state['cell_stage3'] == -1:
    st.write(txt['Init'])
    st.stop()
elif st.session_state['cell_stage3'] == 0:
    st.write(txt['Closed'] + st.session_state.nextStep)
    st.stop()
elif st.session_state['cell_stage3'] == 1:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCell'],value=st.session_state.refCell)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Ongoing'])
    Conditions_in_sidebar("Glue")

    st.write("---")

    VerifPression = st.checkbox(label=txt['verifP'], value=False, key="resetWidget")
    if not VerifPression:
        Print_out_messages()
        st.stop()

    #-------------------------------
    st.subheader(txt['gluePrep'])
    Lstycast, Lcatalyst = ListStycast()
    if len(Lstycast) == 0:
        st.error(txt['noSty'])
        Print_out_messages()
        st.stop()
    elif len(Lcatalyst) == 0:
        st.error(txt['noCat'])
        Print_out_messages()
        st.stop()

    glueData = Glue_Preparation(Lstycast,Lcatalyst)

    #-------------------------------
    st.subheader(txt['glueDep'])

    loadMeth = st.radio(txt['loadMeth'],
            [txt['useLogFile'],txt['useManualInput']],
            index=None
            )
    if loadMeth == txt['useLogFile']:
        depData = Use_Log_File()
    elif loadMeth == txt['useManualInput']:
        depData = Use_Manual_Input()
    else:
        Print_out_messages()
        st.stop()

    st.write('---')
    DepSuccess = st.toggle(txt['depSuccess'],True)
    ReuseCell = True
    if not DepSuccess:
        st.write(txt['cleanCell'])
        ReuseCell = st.toggle(txt['reuseCell'],True)
    Commentaires=st.text_area(label=txt['com'], height=20, value='')

    #-- Avoid accidentally loading empty comment
    if Commentaires == '':
        noComment = st.toggle(txt['noCom'],False)
    if (Commentaires != '') or noComment:
        st.button(txt['register'], on_click=Enregistrement_DepotColle,
                kwargs = {
                    'dictData': {**glueData,**depData},
                          #'stycast_fk'   : Stycast,
                          #'catalyst_fk'  : Cat9,
                          #'mass_stycast' : MassStycast,
                          #'mass_gluemix' : MassGlue,

                          #'ini_pressure'      : InitialPressure,
                          #'mass_bare_sample'  : MassBareSample,
                          #'mass_glued_sample' : MassGluedSample,
                          #'corr_slope'        : CorrSlope,
                          #'corr_pressure'     : CorrPressure,
                          #'datetime_deposit'  : None,
                          #'duration_sample'   : DurationSample,
                          #'duration_deposit'  : DurationDep,
                    'resDep'  : DepSuccess,
                    'reuse'   : ReuseCell,
                    'com'     : Commentaires,
                    }
                )
    Print_out_messages()

elif st.session_state['cell_stage3'] == 2:
    st.write(txt['Updatable'])
    st.error("UNREACHABLE STATE")

elif st.session_state['cell_stage3'] == 3:
    col1, col2, col3 = st.columns(3)
    col1.metric(label=txt['refCell'],value=st.session_state.refCell)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Final'])

    st.write("---")

    allDep = Affichage_allDep()
    if allDep is None:
        Print_out_messages()
        st.stop()

    col3.metric(txt['nbDep'],len(allDep))

    df = pd.DataFrame( allDep, columns = [
            'Stycast.refglue',       
            'Stycast.no_pot',        
            'Stycast.no_cartridge',  
            'Stycast.opening_date',  
            'Stycast.expiry_date',   
            'Catalyst.refcata',      
            'Catalyst.no_pot',       
            'Catalyst.opening_date', 
            'Catalyst.expiry_date',  
            'mass_stycast',          
            'mass_gluemix',          
            'ini_pressure',          
            'mass_bare_sample',      
            'mass_glued_sample',     
            'corr_slope',            
            'corr_pressure',         
            'datetime_deposit',      
            'duration_sample',       
            'duration_deposit',
            'result',
            'Conditions.comment',
            'Conditions.operator',
            'Conditions.humidity',
            'Conditions.temperature',
            'Conditions.datetime',    
            ])
    #for dep in allDep:
    #    glueMix, condDep = Affichage_DepotColle( dep )

    st.dataframe(df)#height=200)

    Print_out_messages()
