# -*-coding:utf-8 -*-
from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *

st.set_page_config(
    page_title=txt['align'],
    page_icon=":mail:",
    layout="centered",
    initial_sidebar_state="expanded",
    #menu_items={
    #    'Get Help': 'mkDoc coming soon...',
    #    'Report a bug': "Contact luka.selem@lpsc.in2p3.fr",
    #    'About': "# Alpaca cell loading app. Using Streamlit !"
    #}
    )

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

def Paired_Module(module):

    module = module.upper() #All references are capitalised

    conn = st.session_state.conn

    find_object = """
            SELECT
                   result_reception,
                   result_inspection,
                   result_cutting
            FROM
                   Modules
            WHERE
                   module_id = %s
            """
    try:
        data = execute_read_query(conn, find_object, (module,) )
        assert len(data) <= 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #->Shouldn't be allowed by the DB anyway: Primary Key
        st.session_state.allmsg.append( [0,"More than one object found with id "+id] )
        return 0
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return 0

    if data == []:
        st.session_state.allmsg.append( [0,txt['logModuleNotFound']] )
        return 0

    isFailed, i_todo = Get_all_status( data[0] )
    if isFailed:
        st.session_state.allmsg.append( [0,txt['logModuleFailed']] )
        return 0
    elif i_todo < 3:
        st.session_state.allmsg.append( [0,txt['logModuleNotReady']] )
        return 0
    else:
        st.session_state.allmsg.append( [3,txt['logPairingReady']] )
        return 1

def GetStepSubstep(iTot):
    if iTot < 0:
        st.error("The alignement step shouldn't be negative!")
        st.stop()

    nInPrevStep = 0
    for iS in range(len(aliStepLabel)):
        nInPrevStep += len(aliStepLabel[iS])-1
        if iTot < nInPrevStep:
            iStep = iS+1
            iSubstep = iTot+1 -nInPrevStep+len(aliStepLabel[iS])-1
            break

    if iTot >= nInPrevStep :
        st.error("The alignement step shouldn't be above total number of steps!")
        st.stop()

    return iStep,iSubstep

def Show_Instructions():

    stepTitle = st.container()
    if st.session_state.aliStep >1:
        iStep, iSubstep = GetStepSubstep(st.session_state.aliStep -2)
        if iSubstep > len(aliStepLabel[iStep-1])-3:
            st.caption("#### "+str(iStep)+" - "+aliStepLabel[iStep-1][0])
        else:
            st.write("#### "+str(iStep)+" - "+aliStepLabel[iStep-1][0])
        st.caption(":grey[*__"+ str(iStep)+"."+str(iSubstep)+"__  "+
                aliStepLabel[iStep-1][iSubstep]+"*]")
    if st.session_state.aliStep >0:
        iStep, iSubstep = GetStepSubstep(st.session_state.aliStep -1)
        if iSubstep == 1:
            st.write("#### "+str(iStep)+" - "+aliStepLabel[iStep-1][0])
        st.caption(":grey[*__"+ str(iStep)+"."+str(iSubstep)+"__  "+
                aliStepLabel[iStep-1][iSubstep]+"*]")
        
    #st.write('---')

    iStep, iSubstep = GetStepSubstep(st.session_state.aliStep)
    if iSubstep == 1:
        st.write("#### "+str(iStep)+" - "+aliStepLabel[iStep-1][0])
    st.write("__"+ str(iStep)+"."+str(iSubstep)+"__  "+
            aliStepLabel[iStep-1][iSubstep])

    colP,colN = st.columns(2)
    colP.button(txt['prevStep'], on_click=GoToPreviousSubstep ,
                disabled= (st.session_state.aliStep == 0),
               )
    colN.button(txt['nextStep'], on_click=GoToNextSubstep ,
                disabled= (st.session_state.aliStep == nAliStep-1),
               )

    st.image(f"{DIR_APP_FIG}/step{iStep}.{iSubstep}.png", caption=txt['photoProc'])
    if st.session_state.aliStep == nAliStep -1:
        return True

    return False

def GoToPreviousSubstep():
    if st.session_state.aliStep == 0:
        st.error("The alignement step is already 0, shouldn't be allowed to go down")
        st.stop()
    st.session_state.aliStep -= 1

def GoToNextSubstep():
    if st.session_state.aliStep == nAliStep:
        st.error("The alignement step is already maximal, shouldn't be allowed to go up")
        st.stop()
    st.session_state.aliStep += 1

def Enregistrement_Alignement(module, com, result=True):
    #FIXME possibility of failed result ?
    cell=str(st.session_state.refCell)
    module = module.upper() #All references are capitalised
    conn = st.session_state.conn

    #-- Create new conditions for this step
    cond_id = Insert_Conditions(com)
    if cond_id is None: #-- Insert_Conditions failed
        return

    pass_alignment="""
        UPDATE Cells 
           SET module_fk = %s,
               cond_alignment_fk = %s,
               result_alignment = %s
         WHERE cell_id = %s
         """
    try:
        execute_query(conn, pass_alignment, (
            module,
            cond_id,
            result,
            cell,
            ))
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    st.session_state.allmsg.append( [3,txt["logAlignmentDone"]] )

    st.session_state.refModule = module
    Cell_Status(st.session_state.refCell)
    return

def Affichage_Alignment():

    cell = str(st.session_state.refCell)
    conn = st.session_state.conn

    find_cell = """
            SELECT
                   module_fk,
                   result_alignment,
                   Conditions.comment,
                   Conditions.operator,
                   Conditions.humidity,
                   Conditions.temperature,
                   Conditions.datetime
              FROM Cells
        INNER JOIN Conditions
                ON cond_alignment_fk = cond_id
             WHERE cell_id = %s
    """
    data = None
    try:
        data = execute_read_query(conn, find_cell, (cell,) )
        assert len(data) == 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #By now, one and only one cell should be in the DB (Primary Key)
        st.session_state.allmsg.append( [0,"Not exactly one object found with id "+id] )
        return
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    cellState = list(data[0])

    if cellState[0] != st.session_state.refModule:
        st.session_state.allmsg.append( 
                [0,"Different refModule in session state and Cells table"] )
        return

    return cellState

#================================================================================
#-- Initialise session_state: 
#   * only cell_stage{IS} really necessary, then page stop anyway
if 'cell_stage2' not in st.session_state.keys():
    st.session_state['cell_stage2'] = -1

#-- Page stage management: 
st.title(txt['section5'])

if st.session_state['cell_stage2'] == -1:
    st.write(txt['Init'])
    st.stop()
elif st.session_state['cell_stage2'] == 0:
    st.write(txt['Closed'] + st.session_state.nextStep)
    st.stop()
elif st.session_state['cell_stage2'] == 1:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCell'],value=st.session_state.refCell)
    contRef = col2.container()

    st.write(txt['Ongoing'])
    Conditions_in_sidebar("Ali")

    st.write("---")

    pairedModule = st.text_input(txt['pairedModule'],
            key="pairedModule"
            )
    pairedModule = pairedModule.upper() #All references are capitalised
    if pairedModule == '':
        st.warning(txt['fillPairedModule'])
        st.stop()
    elif Paired_Module(pairedModule) == 0:
        Print_out_messages()
        st.stop()
    contRef.metric(label=txt['refModule'],value=pairedModule)
    Print_out_messages()

    with st.expander(txt['alignProcess'],expanded=True):
        if 'aliStep' not in st.session_state.keys():
            st.session_state['aliStep'] = 0
        isDone = Show_Instructions()

    alignDone = st.toggle(txt['alignDone'],value=isDone)
    Commentaires=st.text_area(label=txt['com'], height=20, value='')

    #-- Avoid accidentally loading empty comment
    if Commentaires == '':
        noComment = st.toggle(txt['noCom'],False)
    if (Commentaires != '') or noComment: 
        st.button(txt['register'], on_click=Enregistrement_Alignement ,
                kwargs = {
                          'module' : pairedModule,
                          'com'    : Commentaires,
                          'result' : alignDone,
                         }
                 )

elif st.session_state['cell_stage2'] == 2:
    st.write(txt['Updatable'])
    st.error("UNREACHABLE STATE")

elif st.session_state['cell_stage2'] == 3:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCell'],value=st.session_state.refCell)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Final'])

    st.write("---")

    cellState = Affichage_Alignment()
    if cellState is None:
        Print_out_messages()
        st.stop()

    df = pd.DataFrame( (cellState[1:],), columns = [
                        txt['alignDone'],  #result
                        txt['com'],        #Conditions.comment
                        txt['operator'],   #Conditions.operator",
                        txt['humidity'],   #Conditions.humidity",
                        txt['temperature'],#Conditions.temperature",
                        txt['date'],       #Conditions.datetime",
                     ])
    st.dataframe(df,height=20)

    Print_out_messages()
