# -*- Omeed BAHRAMI -*-
# -*-coding:utf-8 -*-

from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *
from streamlit_extras.switch_page_button import switch_page
from allText import steps_top_bot, steps_front, steps_back, image_steps_top_bot, image_steps_front, image_steps_back

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

# Initialize session state information
if 'informations' not in st.session_state:
    st.session_state['informations'] = {
        'Name': '',
        'Temperature': '',
        'Humidity': '',
        'selected_object': None
    }

# Initialize session state stage
if 'pigtail_stage4' not in st.session_state.keys():
    st.session_state['pigtail_stage4'] = 1  # Start at page 1 (for the first image)

# Initialize session state for resin choice
if 'resin_choice' not in st.session_state:
    st.session_state['resin_choice'] = None

if 'resin_selected_time' not in st.session_state:
    st.session_state['resin_selected_time'] = {}

if 'next_button_disabled' not in st.session_state:
    st.session_state['next_button_disabled'] = False

if 'needs_rerun' not in st.session_state:
    st.session_state['needs_rerun'] = False

if 'db_timer_button_enabled' not in st.session_state:
    st.session_state['db_timer_button_enabled'] = False

if 'db_timer_clicked' not in st.session_state:
    st.session_state['db_timer_clicked'] = False

if 'db_timer_activated' not in st.session_state:
    st.session_state['db_timer_activated'] = False

if 'final_stage_completed' not in st.session_state:
    st.session_state['final_stage_completed'] = False

# Display the sidebar
display_sidebar()

st.title(txt['section11'])

#================================================================================
# Function to initialize session state for steps
def initialize_step_state(steps):
    """
    Initialize the state of each step in the process.
    Each step has a checked and disabled state.
    """
    for step in steps:
        if f'step_{step}_checked' not in st.session_state:
            st.session_state[f'step_{step}_checked'] = False
        if f'step_{step}_checked_disabled' not in st.session_state:
            st.session_state[f'step_{step}_checked_disabled'] = False

# Functions to display the sidebars for different types of Pigtails
def display_sidebar_top_bot():
    """
    Display the sidebar for Top/Bot Pigtails.
    All checkboxes are displayed as disabled since they are only for reference.
    """
    st.sidebar.title("Steps for Top/Bot")
    for key in steps_top_bot.keys():
        checked = st.session_state.get(f'step_{key}_checked', False)
        st.sidebar.checkbox(f"Step {key}", key=f'step_{key}_sidebar', value=checked, disabled=True)

def display_sidebar_front():
    """
    Display the sidebar for Front Pigtails.
    All checkboxes are displayed as disabled since they are only for reference.
    """
    st.sidebar.title("Steps for Front")
    for key in steps_front.keys():
        checked = st.session_state.get(f'step_{key}_checked', False)
        st.sidebar.checkbox(f"Step {key}", key=f'step_{key}_sidebar', value=checked, disabled=True)

def display_sidebar_back():
    """
    Display the sidebar for Back Pigtails.
    All checkboxes are displayed as disabled since they are only for reference.
    """
    st.sidebar.title("Steps for Back")
    for key in steps_back.keys():
        checked = st.session_state.get(f'step_{key}_checked', False)
        st.sidebar.checkbox(f"Step {key}", key=f'step_{key}_sidebar', value=checked, disabled=True)

# Display the appropriate sidebar based on the selected object
selected_object = st.session_state['informations'].get('selected_object')

if selected_object:
    if selected_object in ["top", "bot"]:
        display_sidebar_top_bot()
    elif selected_object == "front":
        display_sidebar_front()
    elif selected_object == "back":
        display_sidebar_back()
else:
    st.sidebar.error("No Flavour selected.")

# Function to check if all checkboxes of the current stage are checked
def all_checkboxes_checked(step_numbers):
    """
    Check if all checkboxes for the given step numbers are checked.
    Returns True if all checkboxes are checked, otherwise False.
    """
    return all(st.session_state.get(f'step_{step_number}_checked', False) for step_number in step_numbers)

# Function to disable all checkboxes of the current stage
def disable_all_checkboxes(step_numbers):
    """
    Disable all checkboxes for the given step numbers.
    """
    for step_number in step_numbers:
        st.session_state[f'step_{step_number}_checked_disabled'] = True

# Function to get the last stage with checkboxes
def get_last_stage_with_checkboxes(stage, image_steps):
    """
    Get the last stage that has checkboxes.
    This is used to ensure the correct disabling logic across stages.
    """
    for s in range(stage - 1, 0, -1):
        if image_steps[s - 1]:  # Adjusting index for 0-based
            return s
    return 1

# Function to display each stage
def display_stage(stage, total_stages, object, image_prefix, steps, image_steps):
    """
    Display the current stage with the relevant image and checkboxes.
    Handles the logic for enabling/disabling checkboxes and buttons based on the current state.
    """
    try:
        image_key = f'{image_prefix}_{stage}'
        image = get_image_path('mounting_on_cells', object, image_key)
        
        with st.container(border=True):
            col1 = st.columns(1)[0]
            with col1:
                st.subheader(selected_object.upper(), divider='rainbow')  # For flavour title, can change it later
                st.image(image)
                st.subheader('', divider='gray')
                
                # Dropdown menu for resin selection
                resin_selection_needed = False
                if object == 'top_bot' and stage == 15:
                    resin_selection_needed = True
                elif object == 'front' and stage == 13:
                    resin_selection_needed = True
                elif object == 'back' and stage == 12:
                    resin_selection_needed = True

                if resin_selection_needed:
                    st.session_state['resin_choice'] = st.selectbox('Resin Selection', ['DP100/30min', '3M/2011/24h'], disabled=st.session_state['db_timer_clicked'])
                    if st.session_state['resin_choice']:
                        st.session_state['db_timer_button_enabled'] = True

                # Display the steps
                if stage - 1 < len(image_steps):  # Adjusting index for 0-based
                    step_numbers = image_steps[stage - 1]  # Adjusting index for 0-based
                    last_stage_with_checkboxes = get_last_stage_with_checkboxes(stage, image_steps)

                    for step_number in step_numbers:
                        if step_number in steps:
                            disabled = stage > 1 and (not all_checkboxes_checked(image_steps[last_stage_with_checkboxes - 1]) or st.session_state.get(f'step_{step_number}_checked_disabled', False))
                            if stage == total_stages and st.session_state['final_stage_completed'] and all_checkboxes_checked(step_numbers):
                                disabled = True
                            checked = st.checkbox(steps[step_number], key=f'step_{step_number}_{stage}', value=st.session_state.get(f'step_{step_number}_checked', False), disabled=disabled, on_change=sync_checkbox, args=(step_number,))
                            if checked and not disabled:
                                st.session_state[f'step_{step_number}_checked'] = True
                
                # Place the DB Timer button to the left of the checkboxes
                cols = st.columns([1, 5])
                col_db_timer, col_cases = cols[0], cols[1]
                if resin_selection_needed and st.session_state['db_timer_button_enabled']:
                    with col_db_timer:
                        db_timer_button = st.button('DB Timer', disabled=st.session_state['db_timer_clicked'])
                        if db_timer_button and not st.session_state['db_timer_clicked']:
                            st.session_state['db_timer_clicked'] = True
                            st.session_state['next_button_disabled'] = False
                            disable_all_checkboxes(step_numbers)
                            st.session_state['resin_choice_disabled'] = True
                            st.session_state['resin_choice'] = None
                            st.rerun()

            col2, col3, col4, col5 = st.columns(4)
            with col2:
                return_button = st.button(label='RETURN', disabled=stage == 1)
            with col3:
                home_button = st.button(label='HOME', disabled=stage == 1)
            with col4:
                next_button = st.button(label='NEXT', disabled=stage == total_stages or (resin_selection_needed and not st.session_state['db_timer_clicked']))
            with col5:
                st.write(f"Page : {stage}/{total_stages}")    

            if return_button:
                st.session_state['pigtail_stage4'] = max(1, stage - 1)
                if stage == total_stages:
                    st.session_state['final_stage_completed'] = True
                st.rerun()
            elif home_button:
                st.session_state['pigtail_stage4'] = 1
                if stage == total_stages:
                    st.session_state['final_stage_completed'] = True
                st.rerun()                
            elif next_button:
                if all_checkboxes_checked(step_numbers):
                    disable_all_checkboxes(step_numbers)
                st.session_state['pigtail_stage4'] = min(total_stages, stage + 1)
                if stage == total_stages:
                    st.session_state['final_stage_completed'] = True
                st.rerun()
    except Exception as e:
        st.error(f"Error loading image: {e}")

# Function to synchronize the checkbox state
def sync_checkbox(step_number):
    """
    Synchronize the checkbox state.
    Toggles the checked state of the given step number.
    """
    st.session_state[f'step_{step_number}_checked'] = not st.session_state.get(f'step_{step_number}_checked', False)

#================================================================================

if selected_object:
    if selected_object in ["top", "bot"]:
        st.session_state['selected_object'] = 'top_bot'
        st.session_state['image_prefix'] = 'mounting_top_bot'
        total_stages = len(image_steps_top_bot)
        steps = steps_top_bot
        image_steps = image_steps_top_bot
    elif selected_object == "front":
        st.session_state['selected_object'] = 'front'
        st.session_state['image_prefix'] = 'mounting_front'
        total_stages = len(image_steps_front)
        steps = steps_front
        image_steps = image_steps_front
    elif selected_object == "back":
        st.session_state['selected_object'] = 'back'
        st.session_state['image_prefix'] = 'mounting_back'
        total_stages = len(image_steps_back)
        steps = steps_back
        image_steps = image_steps_back

    # Initialize the step state for the selected steps
    initialize_step_state(steps)

    current_stage = st.session_state['pigtail_stage4']
    
    display_stage(current_stage, total_stages, st.session_state['selected_object'], st.session_state['image_prefix'], steps, image_steps)

else:
    st.error("No Flavour selected. Please go back to the previous page and select a Flavour.")

# Redirection note
st.info('Go to LAPP Menu for other processes.')

