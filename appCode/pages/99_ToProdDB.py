# -*-coding:utf-8 -*-
from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *

st.set_page_config(
    page_title=txt['toProdDB'],
    page_icon=":mail:",
    layout="centered",
    initial_sidebar_state="expanded",
    #menu_items={
    #    'Get Help': 'mkDoc coming soon...',
    #    'Report a bug': "Contact luka.selem@lpsc.in2p3.fr",
    #    'About': "# Alpaca cell loading app. Using Streamlit !"
    #}
    )

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

#================================================================================
#-- Initialise session_state: 
#   * only upload_stage really necessary, then page stop anyway
if 'upload_stage' not in st.session_state.keys():
    st.session_state['upload_stage'] = -1

#-- Page stage management: 
st.title(txt['uploadStage'])

if st.session_state['upload_stage'] == -1:
    st.write(txt['Init'])
    st.stop()
elif st.session_state['upload_stage'] == 0:
    st.write(txt['Closed'] + st.session_state.nextStep)
    st.stop()
elif st.session_state['upload_stage'] == 1:
    st.write(txt['Ongoing'])
    Conditions_in_sidebar("DB")

    st.write("---")

elif st.session_state['upload_stage'] == 2:
    st.write(txt['Updatable'])
    st.error("UNREACHABLE STATE")

elif st.session_state['upload_stage'] == 3:
    st.write(txt['Final'])

    st.write("---")
