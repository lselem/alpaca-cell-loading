# -*- Omeed BAHRAMI -*-
# -*-coding:utf-8 -*-

from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *
from streamlit_extras.switch_page_button import switch_page

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

# Initialize session state informations
if 'informations' not in st.session_state:
    st.session_state['informations'] = {
        'Name': '',
        'Temperature': '',
        'Humidity': '',
        'selected_object': None
    }

# Initialize session state stage
if 'pigtail_stage3' not in st.session_state.keys():
    st.session_state['pigtail_stage3'] = -1

if 'image_index' not in st.session_state.keys():
    st.session_state['image_index'] = 0

if 'checked_images' not in st.session_state.keys():
    st.session_state['checked_images'] = [False] * 9

if 'needs_rerun' not in st.session_state:
    st.session_state['needs_rerun'] = False

# Display the sidebar
display_sidebar()

st.title(txt['section10'])

#================================================================================
# Additional sidebar for page 10
def display_additional_sidebar():
    """
    Display the additional sidebar for the adjustment progress.
    This shows the progress of the adjustments as a series of checkboxes.
    """
    st.sidebar.title("Adjustement Progress")
    image_names = [
        'ZIF Locking',
        'Strain Relief',
        'ZIF Insertion V1',
        'ZRAY Clamp',
        'CLM Insertion',
        'CLM Un-mating',
        'Vacuum System',
        'ZIF Insertion V2',
        'Carrier Box'
    ]
    for idx, name in enumerate(image_names):
        st.sidebar.checkbox(name, value=st.session_state['checked_images'][idx], key=f'checkbox_{idx}', disabled=True)

def display_adjustements():
    """
    Display the adjustment images based on the user's selection.
    This allows the user to select a specific adjustment step from a dropdown menu.
    """
    selection = st.selectbox("Choice :", ["ZIF Locking", "Strain Relief", "ZIF Insertion V1",
                                        "ZRAY Clamp", "CLM Insertion", "CLM Unmating",
                                        "Vacuum System", "ZIF Insertion V2", "Carrier Box"])

    image_dict = {
        "ZIF Locking": "adjustements_zif_locking",
        "Strain Relief": "adjustements_strain_relief",
        "ZIF Insertion V1": "adjustements_zif_insertion_v1",
        "ZRAY Clamp": "adjustements_zray_clamp",
        "CLM Insertion": "adjustements_clm_insertion",
        "CLM Unmating": "adjustements_clm_unmating",
        "Vacuum System": "adjustements_vacuum_system",
        "ZIF Insertion V2": "adjustements_zif_insertion_v2",
        "Carrier Box": "adjustements_carrier_box",
    }

    if selection in image_dict:
        image_key = image_dict[selection]
        #image_data = get_image_from_db('adjustements_and_tools', selection.lower().replace(" ", "_"), image_key)
        #if image_data is None:
        #    st.error("Image data not found.")
        #else:
        #    with st.container(border=True):
        #        image = Image.open(io.BytesIO(image_data))
        #        st.image(image)
        # image = get_image_path('adjustements_and_tools', selection.lower().replace(" ", "_"), image_key)
        image = get_image_path('adjustements_and_tools', '', image_key)
        with st.container(border=True):
            st.image(image)


def display_adjustments_in_sequence(image_index):
    """
    Display the adjustment images in a sequence.
    This allows the user to navigate through the adjustment steps using Previous, Home, and Next buttons.
    """
    image_order = [
        'adjustements_zif_locking',
        'adjustements_strain_relief',
        'adjustements_zif_insertion_v1',
        'adjustements_zray_clamp',
        'adjustements_clm_insertion',
        'adjustements_clm_unmating',
        'adjustements_vacuum_system',
        'adjustements_zif_insertion_v2',
        'adjustements_carrier_box'
    ]

    image_names = [
        'adjustements_zif_locking',
        'adjustements_strain_relief',
        'adjustements_zif_insertion_v1',
        'adjustements_zray_clamp',
        'adjustements_clm_insertion',
        'adjustements_clm_unmating',
        'adjustements_vacuum_system',
        'adjustements_zif_insertion_v2',
        'adjustements_carrier_box'
    ]

    if 0 <= image_index < len(image_order):
        image_key = image_order[image_index]
        image = get_image_path('adjustements_and_tools', '', image_key)

        if 0 <= image_index < len(image_order):
            image_key = image_order[image_index]
            image = get_image_path('adjustements_and_tools', '', image_key)
            if True:
                with st.container(border=True):
                    title = image_names[image_index].replace("adjustements_", "").replace("_", " ").upper()
                    st.subheader(title, divider='rainbow')
                    st.image(image)
                    st.subheader('', divider='gray')
                    col1, col2, col3, col4 = st.columns(4)
                    with col1:
                        if st.button('Previous', disabled=(image_index == 0)):
                            st.session_state['image_index'] -= 1
                            st.session_state['needs_rerun'] = True
                    with col2:
                        if st.button('Home'):
                            st.session_state['image_index'] = 0
                            st.session_state['needs_rerun'] = True
                    with col3:
                        if st.button('Next', disabled=(image_index == len(image_order) - 1)):
                            st.session_state['image_index'] += 1
                            st.session_state['needs_rerun'] = True
                    with col4:
                        done_checked = st.checkbox('Done',
                            value=st.session_state['checked_images'][image_index],
                            key=f'done_checkbox_{image_index}',
                            on_change=update_checkmark,
                            args=(image_index,)
                            )



def update_checkmark(image_index):
    """
    Update the checkmark for the given image index.
    This is called when the 'Done' checkbox is clicked.
    """
    st.session_state['checked_images'][image_index] = not st.session_state['checked_images'][image_index]
    st.session_state['needs_rerun'] = True

#================================================================================

if st.session_state['pigtail_stage3'] == 1:
    col1 = st.columns(1)[0]
    with col1:
        st.markdown(txt['NoticeAdjustement'])

    with st.form(key='initial_form'):
        selected_tab = st.selectbox("Selection :", ["First adjustement for tools", "Tools already adjusted"])
        submit_button = st.form_submit_button(label='OK')

        if submit_button:
            if selected_tab == "First adjustement for tools":
                st.session_state['pigtail_stage3'] = 2
            elif selected_tab == "Tools already adjusted":
                st.session_state['pigtail_stage3'] = 3
            st.session_state['needs_rerun'] = True

elif st.session_state['pigtail_stage3'] == 2:
    display_additional_sidebar()
    display_adjustments_in_sequence(st.session_state['image_index'])

elif st.session_state['pigtail_stage3'] == 3:
    display_adjustements()

# Check if a rerun is needed
if st.session_state['needs_rerun']:
    st.session_state['needs_rerun'] = False
    st.experimental_rerun()

# Redirection note
st.info('Go to LAPP Menu for other processes.')
