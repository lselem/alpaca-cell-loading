# -*-coding:utf-8 -*-
from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *

st.set_page_config(
    page_title=txt['inspection'],
    page_icon=":mail:",
    layout="centered",
    initial_sidebar_state="expanded",
    #menu_items={
    #    'Get Help': 'mkDoc coming soon...',
    #    'Report a bug': "Contact luka.selem@lpsc.in2p3.fr",
    #    'About': "# Alpaca cell loading app. Using Streamlit !"
    #}
    )

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

def Inspection_Done(result=True):
    module=str(st.session_state.refModule)
    conn = st.session_state.conn

    #-- Create new conditions for this step
    cond_id = Insert_Conditions()
    if cond_id is None: #-- Insert_Conditions failed
        return

    pass_inspection="""
            UPDATE Modules 
               SET cond_inspection_fk = %s,
                   result_inspection = %s
             WHERE module_id = %s
             """
    try:
        execute_query(conn, pass_inspection, (
            cond_id,
            result, 
            module,
            ))
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return
    return

def Validation_Inspection( ):
    Inspection_Done(True)#--If no defect found, necessarily True

    st.session_state.allmsg.append( [3,txt["logNoDefect"]] )

    Module_Status(st.session_state.refModule)

    return

def Enregistrement_Inspection(photo, com ):

    module=str(st.session_state.refModule)
    conn = st.session_state.conn

    if not photo :
        st.session_state.allmsg.append( [0,txt['logNoImage']] )
        return

    #-- Load photo in the image server
    try:
        photo = save_img_and_get_dict(photo)
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    #-- Check if inspection has been passed already
    #--FIXME this should be equivalent to the page status being "UPDATABLE"
    find_module = """
        SELECT result_inspection
          FROM Modules
         WHERE module_id = %s
    """
    data = None
    try:
        data = execute_read_query(conn, find_module, (module,) )
        assert len(data) == 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #By now, one and only one module should be in the DB
        st.session_state.allmsg.append( [0,"Not exactly one object found with id "+id] )
        return
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    #-- If first defect registered, also validate the step in Modules table
    if data[0][0] is None: 
        Inspection_Done(True) #--FIXME: allow to make the defect fatal

    #-- Create conditions for the new visual defect
    cond_id = Insert_Conditions(com)
    if cond_id is None: #-- Insert_Conditions failed
        return

    #-- Load visual defect photo
    load_defect="""
        INSERT INTO VisualDefects (
                    photo,
                    module_fk,
                    cond_fk
                    )
             VALUES (%s,%s,%s)
        """
    try:
        execute_query(conn, load_defect, (
            photo,
            module,
            cond_id,
            ))
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    st.session_state.allmsg.append( [3,txt["logLoadImage"]] )
    st.session_state.defectCom = ''

    Module_Status(st.session_state.refModule)

    return

def Affichage_Inspection():

    module=str(st.session_state.refModule)
    conn = st.session_state.conn

    get_photos = """
            SELECT
                   photo,
                   Conditions.comment,
                   Conditions.operator,
                   Conditions.humidity,
                   Conditions.temperature,
                   Conditions.datetime
              FROM VisualDefects
        INNER JOIN Conditions 
                ON cond_fk = Conditions.cond_id
             WHERE module_fk = %s
    """
    photoDB=None
    try:
        photoDB = execute_read_query(conn, get_photos, (module,) )
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    if not photoDB:
        return []

    allPhotos = list()
    for defect in photoDB:
        defect = list(defect)
        try:
            jsonStr = str( defect[0] )[1:-1]
            jsonStr = jsonStr.replace("\'", "\"")
            #defect[0] = json.loads(jsonStr)["url"]
            defect[0] = json.loads(jsonStr)["path"]
        except Exception as err:
            st.session_state.allmsg.append( [0,str(err)] )
            pass
        allPhotos.append(defect)

    return allPhotos

def Display_Defects( allPhotos ):
    for i_ph, ph in enumerate(allPhotos):
        df = pd.DataFrame( (allPhotos[i_ph][2:],), columns = [
                        txt['operator'],   #Conditions.operator",
                        txt['humidity'],   #Conditions.humidity",
                        txt['temperature'],#Conditions.temperature",
                        txt['date'],       #Conditions.datetime",
                     ])
        if ph[1] is None:
            ph[1] = ''
        capt = str(i_ph +1)+" - "+ph[1]
        col1, col2 = st.columns(2)
        col1.image(ph[0],caption=capt)
        col2.dataframe(df,height=20)

#================================================================================
#-- Initialise session_state: 
#   * only module_stage{IS} really necessary, then page stop anyway
if 'module_stage2' not in st.session_state.keys():
    st.session_state['module_stage2'] = -1

#-- Page stage management: 
st.title(txt['section2'])

if st.session_state['module_stage2'] == -1:
    st.write(txt['Init'])
    st.stop()

elif st.session_state['module_stage2'] == 0:
    st.write(txt['Closed'] + st.session_state.nextStep)
    st.stop()

elif st.session_state['module_stage2'] == 1:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCarrier'],value=st.session_state.refCarrier)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Ongoing'])
    Conditions_in_sidebar("Insp")

    st.write("---")

    noDefect = st.toggle(txt['noDefect'])

    if noDefect:
        st.button(txt['validate'], on_click=Validation_Inspection)
        st.stop()

    st.subheader(txt['subsection2_show'])
    PhotoDefect = st.file_uploader(label=txt['photoDefect'], 
            type=['png','jpg','jpeg'],
            )
    Commentaires=st.text_area(label=txt['com'],height=20,
            value='',
            key="defectCom"
            )

    #-- Avoid accidentally loading empty comment
    if Commentaires == '':
        noComment = st.toggle(txt['noCom'],False)
    if (Commentaires != '') or noComment: 
        st.button(txt['register'], on_click=Enregistrement_Inspection ,
                kwargs = {
                          'photo' : PhotoDefect,
                          'com'   : Commentaires,
                         }
                 )

    Print_out_messages()

elif st.session_state['module_stage2'] == 2:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCarrier'],value=st.session_state.refCarrier)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Updatable'])
    Conditions_in_sidebar("Insp")

    st.write("---")
    st.subheader(txt['subsection1_show'])
    allPhotos = Affichage_Inspection()
    if allPhotos is None:#Something failed in Affichage
        Print_out_messages()
        st.stop()
    if len(allPhotos) == 0:
        st.info(txt['logModuleNoDefect'])
    else:
        st.info( txt['logAfficheImage']+str(len(allPhotos)) )
        if st.toggle(txt['showImage']):
            Display_Defects( allPhotos )

    st.subheader(txt['subsection2_show'])

    PhotoDefect = st.file_uploader(label=txt['photoDefect'], 
            type=['png','jpg','jpeg'],
            )
    Commentaires=st.text_area(label=txt['com'],height=20,
            value = '',
            key="defectCom"
            )
    #-- Avoid accidentally loading empty comment
    if Commentaires == '':
        noComment = st.toggle(txt['noCom'],False)
    if (Commentaires != '') or noComment: 
        st.button(txt['register'], on_click=Enregistrement_Inspection ,
                kwargs = {
                          'photo' : PhotoDefect,
                          'com'   : Commentaires,
                         }
                 )

    Print_out_messages()

elif st.session_state['module_stage2'] == 3:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCarrier'],value=st.session_state.refCarrier)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Final'])

    st.write("---")

    #TODO: Formatting of allPhotos
    allPhotos = Affichage_Inspection()
    if allPhotos is None:#Something failed in Affichage
        st.stop()
    if len(allPhotos) == 0:
        st.info(txt['logModuleNoDefect'])
    else:
        st.info( txt['logAfficheImage']+str(len(allPhotos)) )
        Display_Defects( allPhotos )

    Print_out_messages()
