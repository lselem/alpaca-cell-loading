# -*-coding:utf-8 -*-
from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *

st.set_page_config(
    page_title=txt['prepCell'],
    page_icon=":mail:",
    layout="centered",
    initial_sidebar_state="expanded",
    #menu_items={
    #    'Get Help': 'mkDoc coming soon...',
    #    'Report a bug': "Contact luka.selem@lpsc.in2p3.fr",
    #    'About': "# Alpaca cell loading app. Using Streamlit !"
    #}
    )

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

def Enregistrement_Cellule(photoFront, photoBack, massCell, com):

    cell=str(st.session_state.refCell)
    conn = st.session_state.conn

    if not (photoFront and photoBack) :
        st.session_state.allmsg.append( [0,txt['logNoImage']] )
        return

    try:
        photoFront = save_img_and_get_dict(photoFront,'CellFront')
        photoBack  = save_img_and_get_dict(photoBack, 'CellBack' )
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    if ( Cell_Status(st.session_state.refCell) != 1 ):
        st.session_state.allmsg.append( [0,"Wrong cell status or already received !"] )
        return

    #-- Create new conditions for this step
    cond_id = Insert_Conditions( com )
    if cond_id is None: #-- Insert_Conditions failed
        return

    load_cell="""
        INSERT INTO Cells (
                    cell_id,
                    photo_front,
                    photo_back,
                    mass,
                    cond_preparation_fk,
                    result_preparation
                    )
            VALUES (%s,%s,%s,%s,%s,%s)
            """
    try:
        execute_query(conn, load_cell, (
            cell,
            photoFront,
            photoBack,
            massCell,
            cond_id,
            True,#FIXME: no possibility to reject for now
            ))
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    st.session_state.allmsg.append( [3,txt["logAddCell"]+ str(cell)] )

    Cell_Status(st.session_state.refCell)

    return

def Affichage_Cellule():

    cell=str(st.session_state.refCell)
    conn = st.session_state.conn

    find_cell = """
            SELECT
                   photo_front,
                   photo_back,
                   mass,
                   Conditions.comment,
                   Conditions.operator,
                   Conditions.humidity,
                   Conditions.temperature,
                   Conditions.datetime
              FROM Cells
        INNER JOIN Conditions
                ON cond_preparation_fk = cond_id
             WHERE cell_id = %s
    """
    data = None
    try:
        data = execute_read_query(conn, find_cell, (cell,) )
        assert len(data) == 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #By now, one and only one cell should be in the DB
        st.session_state.allmsg.append( [0,"Not exactly one object found with id "+id] )
        return
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    cellState = list(data[0])
    try:
        jsonStr = str( data[0][0] )[1:-1]
        jsonStr = jsonStr.replace("\'", "\"")
        #cellState[0] = json.loads(jsonStr)["url"]
        cellState[0] = json.loads(jsonStr)["path"]
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        pass
    try:
        jsonStr = str( data[0][1] )[1:-1]
        jsonStr = jsonStr.replace("\'", "\"")
        #cellState[1] = json.loads(jsonStr)["url"]
        cellState[1] = json.loads(jsonStr)["path"]
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        pass

    return cellState

#================================================================================
#-- Initialise session_state: 
#   * only cell_stage{IS} really necessary, then page stop anyway
if 'cell_stage1' not in st.session_state.keys():
    st.session_state['cell_stage1'] = -1

#-- Page stage management: 
st.title(txt['section4'])

if st.session_state['cell_stage1'] == -1:
    st.write(txt['Init'])
    st.stop()
elif st.session_state['cell_stage1'] == 0:
    st.write(txt['Closed'] + st.session_state.nextStep)
    st.stop()
elif st.session_state['cell_stage1'] == 1:
    st.metric(label=txt['refCell'],value=st.session_state.refCell)
    st.write(txt['Ongoing'])
    Conditions_in_sidebar("Cell")

    st.write("---")

    #PhotoFront = st.camera_input(label=txt['photoFront'])
    #PhotoBack  = st.camera_input(label=txt['photoBack'])
    PhotoFront = st.file_uploader(label=txt['photoFront'], type=['png','jpg','jpeg'])
    PhotoBack  = st.file_uploader(label=txt['photoBack' ], type=['png','jpg','jpeg'])
    MassCell = st.number_input(label=txt['massCell'],
                               value=3.0, step=0.5, min_value=0., max_value=20.0)

    Commentaires=st.text_area(label=txt['com'], height=20, value='')

    #-- Avoid accidentally loading empty comment
    if Commentaires == '':
        noComment = st.toggle(txt['noCom'],False)
    if (Commentaires != '') or noComment: 
        st.button(txt['register'], on_click=Enregistrement_Cellule,
                  kwargs = {
                            'photoFront' : PhotoFront,
                            'photoBack'  : PhotoBack,
                            'massCell'   : MassCell,
                            'com'        : Commentaires,
                           }
                 )
    Print_out_messages()

elif st.session_state['cell_stage1'] == 2:
    st.write(txt['Updatable'])
    st.error("UNREACHABLE STATE")

elif st.session_state['cell_stage1'] == 3:
    st.metric(label=txt['refCell'],value=st.session_state.refCell)
    st.write(txt['Final'])

    st.write("---")

    cellState = Affichage_Cellule()
    if cellState is None:
        Print_out_messages()
        st.stop()

    df = pd.DataFrame( (cellState[2:],), columns = [
                        txt['massCell'],   #mass
                        txt['com'],        #Conditions.comment
                        txt['operator'],   #Conditions.operator",
                        txt['humidity'],   #Conditions.humidity",
                        txt['temperature'],#Conditions.temperature",
                        txt['date'],       #Conditions.datetime",
                     ])
    st.dataframe(df,height=20)

    col1, col2 = st.columns(2)
    col1.image(cellState[0], caption=txt['photoFront'])
    col2.image(cellState[1], caption=txt['photoBack'])

    Print_out_messages()
