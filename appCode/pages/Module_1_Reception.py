# -*-coding:utf-8 -*-
from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *

st.set_page_config(
    page_title=txt['reception'],
    page_icon=":mail:",
    layout="centered",
    initial_sidebar_state="expanded",
    #menu_items={
    #    'Get Help': 'mkDoc coming soon...',
    #    'Report a bug': "Contact luka.selem@lpsc.in2p3.fr",
    #    'About': "# Alpaca cell loading app. Using Streamlit !"
    #}
    )

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect
Menu_with_redirect()

#================================================================================
#--General methods of page 1
def Enregistrement_Reception(photo, carrier, isStock, com):

    #XXX Formating
    module=str(st.session_state.refModule)
    conn = st.session_state.conn

    if not photo :
        st.session_state.allmsg.append( [0,txt['logNoImage']] )
        return

    if carrier == '' :
        st.session_state.allmsg.append( [0,txt['logNoCarrier']] )
        return

    try:
        photo = save_img_and_get_dict(photo)
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    if ( Module_Status(st.session_state.refModule) != 1 ):
        st.session_state.allmsg.append( [0,"Wrong module status or already received !"] )
        return

    #-- Create new conditions for this step
    cond_id = Insert_Conditions( com )
    if cond_id is None: #-- Insert_Conditions failed
        return

    load_module="""
        INSERT INTO Modules (
                    module_id,
                    carrier_id,
                    photo_reception,
                    is_stored,
                    cond_reception_fk,
                    result_reception
                    )
             VALUES (%s,%s,%s,%s,%s,%s)
        """
    try:
        execute_query(conn, load_module, (
            module,
            carrier,
            photo,
            isStock,
            cond_id,
            True,#NOTE: no possibility to reject at reception
            ))
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    st.session_state.allmsg.append( [3,txt["logAdd"]+ str(module)] )

    Module_Status(st.session_state.refModule)

    return

def Affichage_Reception():

    module=str(st.session_state.refModule)
    conn = st.session_state.conn

    find_module = """
            SELECT
                   photo_reception,
                   carrier_id,
                   is_stored,
                   Conditions.comment,
                   Conditions.operator,
                   Conditions.humidity,
                   Conditions.temperature,
                   Conditions.datetime
              FROM Modules
        INNER JOIN Conditions 
                ON cond_reception_fk = cond_id
             WHERE module_id = %s
    """
    data = None
    try:
        data = execute_read_query(conn, find_module, (module,) )
        assert len(data) == 1 #ID should be unique: 0 or 1 object found
    except AssertionError as err:
        #By now, one and only one module should be in the DB
        st.session_state.allmsg.append( [0,"Not exactly one object found with id "+id] )
        return
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        return

    moduleState = list(data[0])
    try:
        jsonStr = str( data[0][0] )[1:-1]
        jsonStr = jsonStr.replace("\'", "\"")
        #moduleState[0] = json.loads(jsonStr)["url"]
        moduleState[0] = json.loads(jsonStr)["path"]
    except Exception as err:
        st.session_state.allmsg.append( [0,str(err)] )
        pass

    return moduleState

#================================================================================
#-- Initialise session_state: 
#   * only module_stage{IS} really necessary, then page stop anyway
if 'module_stage1' not in st.session_state.keys():
    st.session_state['module_stage1'] = -1

#-- Page stage management: 
st.title(txt['section1'])

if st.session_state['module_stage1'] == -1:
    st.write(txt['Init'])
    st.stop()

elif st.session_state['module_stage1'] == 0:
    st.write("__"+txt['refModule']+" :__ "+st.session_state.refModule)
    st.write(txt['Closed'] + st.session_state.nextStep)
    st.error("UNREACHABLE STATE")
    st.stop()

elif st.session_state['module_stage1'] == 1:

    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCarrier'],value=st.session_state.refCarrier)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)

    st.write(txt['Ongoing'])
    Conditions_in_sidebar("Recep")

    st.write("---")

    if st.session_state.currentObj != txt['carrier']: #Reference entered in home was not a carrier
        RefCarrier = st.text_input(label=txt['refCarrier'])
        st.session_state.refCarrier = RefCarrier.upper()
    #Photo = st.camera_input(label=txt['photoModule'])
    Photo = st.file_uploader(label=txt['photoModule'], type=['png','jpg','jpeg'])
    Stockage = st.checkbox(label=txt['stockLog'])

    Commentaires=st.text_area(label=txt['com'], height=20, value='')

    #-- Avoid accidentally loading empty comment
    if Commentaires == '':
        noComment = st.toggle(txt['noCom'],False)
    if (Commentaires != '') or noComment: 
        st.button(txt['register'], on_click=Enregistrement_Reception,
                  kwargs = {
                            'photo'   : Photo,
                            'carrier' : st.session_state.refCarrier,
                            'isStock' : Stockage,
                            'com'     : Commentaires,
                           }
                 )
    Print_out_messages()

elif st.session_state['module_stage1'] == 2:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCarrier'],value=st.session_state.refCarrier)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)

    st.write(txt['Updatable'])
    st.error("UNREACHABLE STATE")

elif st.session_state['module_stage1'] == 3:
    col1, col2 = st.columns(2)
    col1.metric(label=txt['refCarrier'],value=st.session_state.refCarrier)
    col2.metric(label=txt['refModule'],value=st.session_state.refModule)
    st.write(txt['Final'])

    st.write("---")

    moduleState = Affichage_Reception()
    df = pd.DataFrame( (moduleState[1:],), columns = [
                        txt['refCarrier'], #carrier_id
                        txt['stockLog'],   #is_stored
                        txt['com'],        #Conditions.comment
                        txt['operator'],   #Conditions.operator",
                        txt['humidity'],   #Conditions.humidity",
                        txt['temperature'],#Conditions.temperature",
                        txt['date'],       #Conditions.datetime",
                     ])
    st.dataframe(df,height=20)
    st.image(moduleState[0], caption=txt['photoModule'])

    Print_out_messages()
