# -*- Omeed BAHRAMI -*-
# -*-coding:utf-8 -*-

from generalAppConfig import *
from utils.app_utils import *
from utils.YalDBaccess import *
from streamlit_extras.switch_page_button import switch_page

# Redirect to appMain.py if no reference loaded otherwise show the navigation menu
from utils.menu import Menu_with_redirect 
Menu_with_redirect()

# Initialize session state information
if 'informations' not in st.session_state:
    st.session_state['informations'] = {
        'Name': '',
        'Temperature': '',
        'Humidity': '',
        'selected_object': None
    }

# Initialize session state if not already done
if 'pigtail_stage2' not in st.session_state.keys():
    st.session_state['pigtail_stage2'] = -1

if 'image_index' not in st.session_state.keys():
    st.session_state['image_index'] = 0

# Display the sidebar
display_sidebar()
#-- Page stage management:
st.title(txt['section9'])

#================================================================================
# Function to display tools based on the selected object
def display_tools(object, image_prefix, image_index):
    """
    Display the images of tools based on the selected object and image index.
    This function fetches the image data from the database and displays it.
    """
    try:
        key = f'{image_prefix}_{image_index + 1}'
        #image_data = get_image_from_db('position_of_tools', object, key)
        #if image_data is None:
        #    st.error("Image data not found.")
        #    return
        #image = Image.open(io.BytesIO(image_data))
        image = get_image_path('position_of_tools', object, key)
        st.image(image)
    except Exception as e:
        st.error(f"Error loading image: {e}")

#================================================================================

# Get the selected object from session state
selected_object = st.session_state['informations'].get('selected_object')

# Logic to determine the image prefix based on the selected object
if selected_object:
    if selected_object == "top":
        st.session_state['selected_object'] = 'top'
        st.session_state['image_prefix'] = 'position_top'
    elif selected_object == "bot":
        st.session_state['selected_object'] = 'bot'
        st.session_state['image_prefix'] = 'position_bot'
    elif selected_object == "front":
        st.session_state['selected_object'] = 'front'
        st.session_state['image_prefix'] = 'position_front'
    elif selected_object == "back":
        st.session_state['selected_object'] = 'back'
        st.session_state['image_prefix'] = 'position_back'
else:
    st.error("No Flavour selected. Please go back to the previous page and select a Flavour.")

# Page stages management
if st.session_state['pigtail_stage2'] == 1:
    with st.form(key='next_form'):
        col1 = st.columns(1)[0]
        with col1:
            #image_data_bb = get_image_from_db('position_of_tools', 'breadboard', 'position_breadboard')
            #if image_data_bb is None:
            #    st.error("Image data not found.")
            #image_bb = Image.open(io.BytesIO(image_data_bb))
            #st.image(image_bb)
            image_path = get_image_path('position_of_tools', 'breadboard', 'position_breadboard')
            st.subheader('Breadboard', divider='rainbow')
            st.image(image_path)
            st.subheader('', divider='gray')

        col2 = st.columns(1)[0]
        with col2:
            submit_button = st.form_submit_button(label='NEXT')
            if submit_button:
                st.session_state['pigtail_stage2'] = 2
                st.rerun()

elif st.session_state['pigtail_stage2'] == 2:
    with st.form(key='home_form'):
        col1 = st.columns(1)[0]
        with col1:
            st.subheader(selected_object.upper(), divider='rainbow') # For flavour title, can change it later
            display_tools(st.session_state['selected_object'], st.session_state['image_prefix'], st.session_state['image_index'])
            st.subheader('', divider='gray')
        col2, col3 = st.columns(2)
        with col2:
            home_button = st.form_submit_button(label='Breadboard')
        with col3:
            # Change the button label based on the image index
            next_button_label = 'Picture' if st.session_state['image_index'] == 0 else 'Schematic'
            next_image_button = st.form_submit_button(label=next_button_label)
        
        if home_button:
            st.session_state['pigtail_stage2'] = 1
            st.session_state['image_index'] = 0  # Reset the image index
            st.rerun()
        elif next_image_button:
            st.session_state['image_index'] = (st.session_state['image_index'] + 1) % 2  # Assuming there are two images per object
            st.rerun()

# Redirection note
st.info('Go to LAPP Menu for other processes.')