#================================================
# NEW SETUP
#================================================
#! /bin/bash

#-- Setup lab configuration
if [ -L .env ]
then
  echo "Using configuration from `realpath .env`"
else
  cd config/
  echo -e "No lab configuration specified yet. Following configurations are available:\n"
  ls *.env
  echo ""

  read -p "Enter config file name: " configFile

  while [ ! -f $configFile ]
  do
    echo "This file doesn't exist !"
    read -p "Enter config file name: " configFile
  done
  cd ..

  ln -s config/$configFile .env
  echo "Using configuration from `realpath .env`"
fi
